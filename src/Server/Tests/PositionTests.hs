module Tests.PositionTests where

import Jump.Position
import Test.HUnit

m_1Test = TestCase (assertEqual "for m_1 45 90" (round $ m_1 45 90) 0 )

m_2Test = TestCase (assertEqual "for m_2 25 170 0.15 1.2" (round $ 10000 * (m_2 25 170 0.15 1.2 )) 2103)
