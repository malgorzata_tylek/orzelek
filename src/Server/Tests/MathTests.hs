module Tests.MathTest where

import Test.HUnit
import Server.Math.AngleCalc
import Server.Math.PrimaryFunctions


squareFunTest = TestCase (assertEqual "for squareFun sqrt 2" (round $ squareFun sqrt 2) 2 )

factorialTest0 = TestCase (assertEqual "for factorial 7" (factorial 7) 5040 )
factorialTest1 = TestCase (assertEqual "for factorial 4" (factorial 4) 24 )

sqrtQuotientTest0 = TestCase (assertEqual "for quotients 1/4" (sqrtQuotient 1 4) 0.5 )
sqrtQuotientTest1 = TestCase (assertEqual "for quotients 8/2" (sqrtQuotient 8 2) 2 )

tests = TestList [
    TestLabel "squareFunTest" squareFunTest,
    TestLabel "factorialTest0" factorialTest0,
    TestLabel "factorialTest1" factorialTest1
  ]

sqrtQuotientTests = TestList [
    TestLabel "sqrtQuotientTest0" sqrtQuotientTest0,
    TestLabel "sqrtQuotientTest1" sqrtQuotientTest1
  ]

-- to run
-- runTestTT tests
