module Server.Jump.Aerodynamic.Force
where

-- opór powietrza - drag force
-- Fd = ro/2 * D
-- ro - gęstość powietrza
-- D - wspołczynnik oporu

dragForce :: Double -> Double -> Double
dragForce airDensity dragCoefficient =
  airDensity * dragCoefficient / 2

-- siła nośna - lift force
-- Fl = ro/2 * L
-- ro - gęstość powietrza
-- L - wspołczynnik oporu

liftForce :: Double -> Double -> Double
liftForce airDensity liftCoefficient =
  airDensity * liftCoefficient / 2

-- grawitacja
gravity :: Double
gravity = 9.80665 -- m/s^2

-- siła grawitacji
gravityForce :: Double -> Double
gravityForce mass =
  mass * gravity
