module Server.Jump.Dynamic where

import Server.Jump.Aerodynamic.Force
import Server.Math.AngleCalc

-- siły działające na skoczka:

-- w poziomie
-- Fx
-- jumperAngle - kąt między górną częścią tułowia skoczka a kierunkiem lotu
-- movementAngle - kąt między kierunkiem lotu skoczka a osią x

horizontal :: Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double
horizontal liftForce velocity longitudinalArea jumperAngle movementAngle dragForce frontalArea
  = liftForce * velocity^2 * longitudinalArea
  * (jumperAngleSine)^2 * jumperAngleCosine * movementAngleSine
  - (dragForce * velocity^2
  * (frontalArea + longitudinalArea * (jumperAngleSine)^2 * jumperAngleSine)
  * movementAngleCosine)
  where
    jumperAngleSine = sine jumperAngle
    jumperAngleCosine = cosine jumperAngle
    movementAngleSine = sine movementAngle
    movementAngleCosine = cosine movementAngle

-- w pionie
-- Fy

vertical :: Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double
vertical gravityForce liftForce velocity longitudinalArea jumperAngle movementAngle dragForce frontalArea
  = -gravityForce * liftForce * velocity^2 * longitudinalArea
  * (jumperAngleSine)^2 * jumperAngleCosine * movementAngleCosine
  + dragForce * velocity^2
  * (frontalArea + longitudinalArea * (jumperAngleSine)^2 * jumperAngleSine)
  * movementAngleSine
  where
    jumperAngleSine = sine jumperAngle
    jumperAngleCosine = cosine jumperAngle
    movementAngleSine = sine movementAngle
    movementAngleCosine = cosine movementAngle

  -- acceleration
acceleration :: Double -> Double -> Double
acceleration force mass =
  force / mass

-- -- velocity
velocity :: Double -> Double -> Double
velocity acceleration time =
  acceleration * time

-- -- position at any moment
-- position :: Double -> Double -> Double -> Double
-- position startPosition currVelocity time =
--   startPosition + (currVelocity * time)
