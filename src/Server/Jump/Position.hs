module Server.Jump.Position where

import Server.Math.AngleCalc
import Server.Math.PrimaryFunctions
import Server.Jump.Aerodynamic.Force
import Server.Simulation.Defaults

step :: Double
step = 0.01

-- obliczanie wspolrzednej y(t)
yBuffor :: Double
yBuffor = 1

k1 :: Double -> Double
k1 u = step * u

q1 :: Double -> Double -> Double
q1 u kConst =
  step * (-gravity + (kConst * square u))

k2 :: Double -> Double -> Double -> Double -> Double
k2 u kConst k1_ q1_ =
  step * (-gravity + kConst * (square k1_) + 0.5 * q1_)

q2 :: Double -> Double -> Double -> Double
q2 kConst u q1_ =
  step * (-gravity + kConst * square (u + 0.5 * q1_))

k3 :: Double -> Double -> Double -> Double
k3 kConst k1_ k2_ =
  step * (-gravity + kConst * square k1_ + 0.5 * k2_)

q3 :: Double -> Double -> Double -> Double
q3 kConst u q2_ =
  step * (-gravity + kConst * square (u + 0.5 * q2_))

k4 :: Double -> Double -> Double
k4 u q3_ =
  step * (u + q3_)

q4 :: Double -> Double -> Double -> Double
q4 kConst u q3_ =
  step * (-gravity + kConst * square (u + q3_))

yNumerical :: [(Double, Double)] -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> [(Double, Double)]
yNumerical results time mass currentY u jumperAngle movementAngle longArea frontArea liftForce_ dragForce_
  | currentY > yBuffor = yNumerical (results ++ [(currentY, time)]) (time + step) mass newY newU jumperAngle movementAngle longArea frontArea liftForce_ dragForce_
  | otherwise = results ++ [(currentY, time)]
  where
    k1_ = k1 u
    m1_ = m1 jumperAngle movementAngle longArea
    m2_ = m2 jumperAngle movementAngle longArea frontArea
    kConst = v_yConst m1_ m2_ liftForce_ dragForce_ mass
    q1_ = q1 u kConst
    k2_ = k2 u kConst k1_ q1_
    q2_ = q2 kConst u q1_
    k3_ = k3 kConst k1_ k2_
    q3_ = q3 kConst u q2_
    k4_ = k4 u q3_
    q4_ = q4 kConst u q3_
    newY = currentY + (1/6) * (k1_ + 2 * k2_ + 2 * k3_ + k4_)
    newU = uNumerical u q1_ q2_ q3_ q4_

testYNumerical = yNumerical [] 0 mass 65 2.5 jumperAngle movementAngle longitudinalArea frontalArea (liftForce 3 1) (dragForce 3 1)

uNumerical :: Double -> Double -> Double -> Double -> Double -> Double
uNumerical currentU q1_ q2_ q3_ q4_ =
  currentU + (1/6) * (q1_ + 2 * q2_ + 2 * q3_ + q4_)

m1 :: Double -> Double -> Double -> Double
m1 jumperAngle movementAngle longArea =
  longArea * squareFun sine jumperAngle * cosine jumperAngle * cosine movementAngle

m2 :: Double -> Double -> Double -> Double -> Double
m2 jumperAngle movementAngle longArea frontArea =
  (frontArea + (longArea * squareFun sine jumperAngle * sine jumperAngle)) * sine movementAngle

-- we wzorze: K
v_yConst :: Double -> Double -> Double -> Double -> Double -> Double
v_yConst m1 m2 liftForce dragForce mass =
  ((liftForce * m1) + (dragForce * m2) ) / mass

kTest =
  v_yConst m11 m22 liftForce_ dragForce_ 70
  where
    m11 = m1 35 35 1.2
    m22 = m2 35 35 1.2 0.15
    liftForce_ = liftForce 3 1
    dragForce_ = dragForce 3 1


-- obliczanie wspolrzednej x(t)
n1 :: Double -> Double -> Double -> Double -> Double
n1 liftForce longArea alpha beta =
  liftForce * longArea * squareFun sine alpha * cosine alpha * sine beta

n2 :: Double -> Double -> Double -> Double -> Double -> Double
n2 dragForce frontArea longArea alpha beta =
    dragForce * (frontArea + (longArea * squareFun sine alpha * sine alpha )) * cosine beta

pConst :: Double -> Double -> Double
pConst n1_ n2_ =
  n1_ - n2_

testP =
  n11 - n22
  where
    liftForce_ = liftForce 3 1
    dragForce_ = dragForce 3 1
    n11 = n1 liftForce_ 1.2 35 35
    n22 = n2 dragForce_ 0.15 1.2 35 35

k1_x :: Double -> Double
k1_x u =
  step * u

q1_x :: Double -> Double -> Double -> Double
q1_x pConst_ mass u =
  step * pConst_ / mass * square u

k2_x :: Double -> Double -> Double
k2_x u q1_x_ =
  step * (u + 0.5 * q1_x_)

q2_x :: Double -> Double -> Double -> Double -> Double
q2_x pConst_ mass u q1_x_ =
  step * pConst_ / mass * square (u + 0.5 * q1_x_)

k3_x :: Double -> Double -> Double
k3_x u q2_x_ =
  step * (u + 0.5 * q2_x_)

q3_x :: Double -> Double -> Double -> Double -> Double
q3_x pConst_ mass u q2_x_ =
  step * pConst_ / mass * square (u + 0.5 * q2_x_)

k4_x :: Double -> Double -> Double
k4_x u q3_x_ =
  step * (u + q3_x_)

q4_x :: Double -> Double -> Double -> Double -> Double
q4_x pConst_ mass u q3_x_ =
  step * pConst_ / mass * square (u + q3_x_)

xNumerical :: [Double] -> [Double] -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> [(Double)]
xNumerical results [time] currentX u mass jumperAngle movementAngle frontArea longArea liftForce_ dragForce_ =
  results ++ [newX]
  where
    k1_x_ = k1_x u
    n1_ = n1 liftForce_ longArea jumperAngle movementAngle
    n2_ = n2 dragForce_ frontArea longArea jumperAngle movementAngle
    pConst_ = pConst n1_ n2_
    q1_x_ = q1_x pConst_ mass u
    k2_x_ = k2_x u q1_x_
    q2_x_ = q2_x pConst_ mass u q1_x_
    k3_x_ = k3_x u q2_x_
    q3_x_ = q3_x pConst_ mass u q2_x_
    k4_x_ = k4_x u q3_x_
    q4_x_ = q4_x pConst_ mass u q3_x_
    newX = currentX + (1/6) * (k1_x_ + 2 * k2_x_ + 2 * k3_x_ + k4_x_)

xNumerical results (t:time) currentX u mass jumperAngle movementAngle frontArea longArea liftForce_ dragForce_ =
  xNumerical (results ++ [currentX]) time newX newU mass jumperAngle movementAngle frontArea longArea liftForce_ dragForce_
  where
    n1_ = n1 liftForce_ longArea jumperAngle movementAngle
    n2_ = n2 dragForce_ frontArea longArea jumperAngle movementAngle
    pConst_ = pConst n1_ n2_
    q1_x_ = q1_x pConst_ mass u
    q2_x_ = q2_x pConst_ mass u q1_x_
    q3_x_ = q3_x pConst_ mass u q2_x_
    q4_x_ = q4_x pConst_ mass u q3_x_
    k1_x_ = k1_x u
    k2_x_ = k2_x u q1_x_
    k3_x_ = k3_x u q2_x_
    k4_x_ = k4_x u q3_x_
    newU = uNumerical u q1_x_ q2_x_ q3_x_ q4_x_
    newX = currentX + (1/6) * (k1_x_ + 2 * k2_x_ + 2 * k3_x_ + k4_x_)
