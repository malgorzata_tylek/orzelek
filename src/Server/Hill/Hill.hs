module Server.Hill.Hill where

import Server.Math.AngleCalc

--hillsize - rozmiar skoczni - odległość od progu do punktu L
--slopeAngle - kąt nachylenia progu

hillEdgeHeight :: Double -> Double -> Double
hillEdgeHeight hillSize slopeAngle =
  hillSize * (\slope -> sine slope) slopeAngle

-- wyliczenie pozycji startowej - miejsca, z którego zaczyna się lot (próg skoczni)
-- na podstawie rozmiaru skoczni i kąta nachylenia progu

startYPosition :: Double -> Double -> Double
startYPosition = hillEdgeHeight

startXPosition :: Double
startXPosition = 0
