module Server.Math.AngleCalc
where

import Server.Math.PrimaryFunctions

radians :: Double -> Double
radians degrees =
  (degrees * pi) / 180

sine :: Double -> Double
sine degrees =
  sin $ radians degrees

cosine :: Double -> Double
cosine degrees =
    cos $ radians degrees

arctan :: Double -> Double
arctan degrees =
    atan $ radians degrees  
