module Server.Math.PrimaryFunctions where

square :: Double -> Double
square value = (\value -> value * value) value

squareFun :: (Double -> Double) -> Double -> Double
squareFun fun arg =
  (squareFunction fun) arg

squareFunction :: (Double -> Double) -> (Double -> Double)
squareFunction fun =
  square . fun

sqrtQuotient :: Double -> Double -> Double
sqrtQuotient numerator denominator =
  sqrt $ numerator / denominator

factorial :: Double -> Double
factorial 0 = 1
factorial n = n * factorial (n - 1)

roundWithPrecision :: Double -> Int -> Double
roundWithPrecision number precision =
  (fromInteger $ round $ number * (10^precision)) / (10.0^^precision)
