module Server.Simulation.Algorithm where

--import Data.Vector
import Server.Simulation.Defaults
import Server.Jump.Position
import Server.Hill.Hill
import Server.Math.PrimaryFunctions
import Server.Jump.Aerodynamic.Force
import Haste.Graphics.Canvas

-- cel: znależć pozycję skoczka (x, y) w dowolnym momencie
-- time step = 0.001s
-- koniec symulacji - y < 1
-- output : długość skoku; czas lotu
startTime :: Double
startTime = 0

precision :: Int
precision = 2

temporaryXPath :: Double -> [Double]
temporaryXPath n =
  [0..n]

jumpY :: Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> [(Double, Double)]
jumpY mass v0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  yNumerical [] startTime mass currentY v0 jumperAngle movementAngle longArea frontArea liftForce_ dragForce_
  where
    currentY = Server.Hill.Hill.startYPosition hillSize slopeAngle
    liftForce_ = liftForce airDensity liftCoefficient
    dragForce_ = dragForce airDensity dragCoefficient

jumpX :: [Double] -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> [Double]
jumpX time mass v0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  xNumerical [] time startXPosition v0 mass jumperAngle movementAngle frontArea longArea liftForce_ dragForce_
  where
    liftForce_ = liftForce airDensity liftCoefficient
    dragForce_ = dragForce airDensity dragCoefficient

jumpXTest = jumpX [0,0.01..5.21] mass velocity_x jumperAngle movementAngle frontalArea longitudinalArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle

getXYTime :: Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> (Double, Double, Double)
getXYTime mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  (maxX, maxY, maxTime)
  where
    ysAndTimes = jumpY mass vY0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
    maxY = fst $ last ysAndTimes
    times = snd $ unzip ysAndTimes
    maxTime = last times
    xs = jumpX times mass vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
    maxX = last xs


distanceAndTime ::  Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> (Double, Double)
distanceAndTime mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  (roundWithPrecision distance precision, roundWithPrecision time precision)
  where
    (x, y, time) = getXYTime mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
    distance = countDistance (Server.Hill.Hill.startYPosition hillSize slopeAngle) x

distanceAndTimeIO ::  String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> IO (Double, Double)
distanceAndTimeIO mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  return dAT
    where
      dAT = distanceAndTime (read mass) (read vY0) (read vX0) (read jumperAngle) (read movementAngle) (read frontArea)
        (read longArea) (read airDensity) (read dragCoefficient) (read liftCoefficient) (read hillSize) (read slopeAngle)


countDistance :: Double -> Double -> Double
countDistance startYPosition x =
  sqrt $ square startYPosition + square x

flightPath :: Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> Double -> [Point]
flightPath mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  zip xCoordinate yCoordinate
  where
    ysAndTimes = jumpY mass vY0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
    yCoordinate = getFirsts ysAndTimes
    times = snd $ unzip ysAndTimes
    xCoordinate = jumpX times mass vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle

flightPathIO :: String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> String -> IO [Point]
flightPathIO mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle =
  return path
  where
    path = flightPath (read mass) (read vY0) (read vX0) (read jumperAngle) (read movementAngle) (read frontArea)
      (read longArea) (read airDensity) (read dragCoefficient) (read liftCoefficient) (read hillSize) (read slopeAngle)

getFirsts tuples = fst $ unzip tuples

getSeconds tuples = snd $ unzip tuples

testPath = flightPath mass velocity_y velocity_x jumperAngle movementAngle frontalArea longitudinalArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle

testXY =
  getXYTime mass velocity_y velocity_x jumperAngle movementAngle frontalArea longitudinalArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
