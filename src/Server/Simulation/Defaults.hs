module Server.Simulation.Defaults where

airDensity :: Double
airDensity = 1

liftCoefficient :: Double
liftCoefficient = 1

dragCoefficient :: Double
dragCoefficient = 2

velocity_x :: Double
velocity_x = 27

velocity_y :: Double
velocity_y = 2.5

frontalArea :: Double
frontalArea = 0.15

longitudinalArea :: Double
longitudinalArea = 1.2

jumperAngle :: Double
jumperAngle = 35

movementAngle :: Double
movementAngle = 20

mass :: Double
mass = 60

startYPosition :: Double
startYPosition = 68

hillSize :: Double
hillSize = 120

slopeAngle :: Double
slopeAngle = 35
