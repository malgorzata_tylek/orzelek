module Server.Simulation.Results where

import Haste.Graphics.Canvas
--import Data.Vector.Primitive
import Server.Simulation.Algorithm

-- mock
getDistance :: Double
getDistance =
  127.5

-- y coordinate
yCoordinate :: [Double]
yCoordinate =
  [
    9.7, 10.3, 10.5, 10.8, 11.2, 11.2, 11, 10.6,
    10.2, 9.8, 9, 8, 7.2, 6, 5, 3.9, 2.9, 2, 1.6, 1  
  ]

-- x coordinate
xCoordinate :: [Double]
xCoordinate =
  [0..19]

-- mock
flightPath :: [Point]
flightPath =
  zip xCoordinate yCoordinate
--  [ (a,b) | a <- [1..30] , b <- [a+1] ]
