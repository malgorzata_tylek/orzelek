"use strict";
// This object will hold all exports.
var Haste = {};
if(typeof window === 'undefined') window = global;

/* Constructor functions for small ADTs. */
function T0(t){this._=t;}
function T1(t,a){this._=t;this.a=a;}
function T2(t,a,b){this._=t;this.a=a;this.b=b;}
function T3(t,a,b,c){this._=t;this.a=a;this.b=b;this.c=c;}
function T4(t,a,b,c,d){this._=t;this.a=a;this.b=b;this.c=c;this.d=d;}
function T5(t,a,b,c,d,e){this._=t;this.a=a;this.b=b;this.c=c;this.d=d;this.e=e;}
function T6(t,a,b,c,d,e,f){this._=t;this.a=a;this.b=b;this.c=c;this.d=d;this.e=e;this.f=f;}

/* Thunk
   Creates a thunk representing the given closure.
   If the non-updatable flag is undefined, the thunk is updatable.
*/
function T(f, nu) {
    this.f = f;
    if(nu === undefined) {
        this.x = __updatable;
    }
}

/* Hint to optimizer that an imported symbol is strict. */
function __strict(x) {return x}

// A tailcall.
function F(f) {
    this.f = f;
}

// A partially applied function. Invariant: members are never thunks.
function PAP(f, args) {
    this.f = f;
    this.args = args;
    this.arity = f.length - args.length;
}

// "Zero" object; used to avoid creating a whole bunch of new objects
// in the extremely common case of a nil-like data constructor.
var __Z = new T0(0);

// Special object used for blackholing.
var __blackhole = {};

// Used to indicate that an object is updatable.
var __updatable = {};

// Indicates that a closure-creating tail loop isn't done.
var __continue = {};

/* Generic apply.
   Applies a function *or* a partial application object to a list of arguments.
   See https://ghc.haskell.org/trac/ghc/wiki/Commentary/Rts/HaskellExecution/FunctionCalls
   for more information.
*/
function A(f, args) {
    while(true) {
        f = E(f);
        if(f instanceof Function) {
            if(args.length === f.length) {
                return f.apply(null, args);
            } else if(args.length < f.length) {
                return new PAP(f, args);
            } else {
                var f2 = f.apply(null, args.slice(0, f.length));
                args = args.slice(f.length);
                f = B(f2);
            }
        } else if(f instanceof PAP) {
            if(args.length === f.arity) {
                return f.f.apply(null, f.args.concat(args));
            } else if(args.length < f.arity) {
                return new PAP(f.f, f.args.concat(args));
            } else {
                var f2 = f.f.apply(null, f.args.concat(args.slice(0, f.arity)));
                args = args.slice(f.arity);
                f = B(f2);
            }
        } else {
            return f;
        }
    }
}

function A1(f, x) {
    f = E(f);
    if(f instanceof Function) {
        return f.length === 1 ? f(x) : new PAP(f, [x]);
    } else if(f instanceof PAP) {
        return f.arity === 1 ? f.f.apply(null, f.args.concat([x]))
                             : new PAP(f.f, f.args.concat([x]));
    } else {
        return f;
    }
}

function A2(f, x, y) {
    f = E(f);
    if(f instanceof Function) {
        switch(f.length) {
        case 2:  return f(x, y);
        case 1:  return A1(B(f(x)), y);
        default: return new PAP(f, [x,y]);
        }
    } else if(f instanceof PAP) {
        switch(f.arity) {
        case 2:  return f.f.apply(null, f.args.concat([x,y]));
        case 1:  return A1(B(f.f.apply(null, f.args.concat([x]))), y);
        default: return new PAP(f.f, f.args.concat([x,y]));
        }
    } else {
        return f;
    }
}

function A3(f, x, y, z) {
    f = E(f);
    if(f instanceof Function) {
        switch(f.length) {
        case 3:  return f(x, y, z);
        case 2:  return A1(B(f(x, y)), z);
        case 1:  return A2(B(f(x)), y, z);
        default: return new PAP(f, [x,y,z]);
        }
    } else if(f instanceof PAP) {
        switch(f.arity) {
        case 3:  return f.f.apply(null, f.args.concat([x,y,z]));
        case 2:  return A1(B(f.f.apply(null, f.args.concat([x,y]))), z);
        case 1:  return A2(B(f.f.apply(null, f.args.concat([x]))), y, z);
        default: return new PAP(f.f, f.args.concat([x,y,z]));
        }
    } else {
        return f;
    }
}

/* Eval
   Evaluate the given thunk t into head normal form.
   If the "thunk" we get isn't actually a thunk, just return it.
*/
function E(t) {
    if(t instanceof T) {
        if(t.f !== __blackhole) {
            if(t.x === __updatable) {
                var f = t.f;
                t.f = __blackhole;
                t.x = f();
            } else {
                return t.f();
            }
        }
        if(t.x === __updatable) {
            throw 'Infinite loop!';
        } else {
            return t.x;
        }
    } else {
        return t;
    }
}

/* Tail call chain counter. */
var C = 0, Cs = [];

/* Bounce
   Bounce on a trampoline for as long as we get a function back.
*/
function B(f) {
    Cs.push(C);
    while(f instanceof F) {
        var fun = f.f;
        f.f = __blackhole;
        C = 0;
        f = fun();
    }
    C = Cs.pop();
    return f;
}

// Export Haste, A, B and E. Haste because we need to preserve exports, A, B
// and E because they're handy for Haste.Foreign.
if(!window) {
    var window = {};
}
window['Haste'] = Haste;
window['A'] = A;
window['E'] = E;
window['B'] = B;


/* Throw an error.
   We need to be able to use throw as an exception so we wrap it in a function.
*/
function die(err) {
    throw E(err);
}

function quot(a, b) {
    return (a-a%b)/b;
}

function quotRemI(a, b) {
    return {_:0, a:(a-a%b)/b, b:a%b};
}

// 32 bit integer multiplication, with correct overflow behavior
// note that |0 or >>>0 needs to be applied to the result, for int and word
// respectively.
if(Math.imul) {
    var imul = Math.imul;
} else {
    var imul = function(a, b) {
        // ignore high a * high a as the result will always be truncated
        var lows = (a & 0xffff) * (b & 0xffff); // low a * low b
        var aB = (a & 0xffff) * (b & 0xffff0000); // low a * high b
        var bA = (a & 0xffff0000) * (b & 0xffff); // low b * high a
        return lows + aB + bA; // sum will not exceed 52 bits, so it's safe
    }
}

function addC(a, b) {
    var x = a+b;
    return {_:0, a:x & 0xffffffff, b:x > 0x7fffffff};
}

function subC(a, b) {
    var x = a-b;
    return {_:0, a:x & 0xffffffff, b:x < -2147483648};
}

function sinh (arg) {
    return (Math.exp(arg) - Math.exp(-arg)) / 2;
}

function tanh (arg) {
    return (Math.exp(arg) - Math.exp(-arg)) / (Math.exp(arg) + Math.exp(-arg));
}

function cosh (arg) {
    return (Math.exp(arg) + Math.exp(-arg)) / 2;
}

function isFloatFinite(x) {
    return isFinite(x);
}

function isDoubleFinite(x) {
    return isFinite(x);
}

function err(str) {
    die(toJSStr(str));
}

/* unpackCString#
   NOTE: update constructor tags if the code generator starts munging them.
*/
function unCStr(str) {return unAppCStr(str, __Z);}

function unFoldrCStr(str, f, z) {
    var acc = z;
    for(var i = str.length-1; i >= 0; --i) {
        acc = B(A(f, [str.charCodeAt(i), acc]));
    }
    return acc;
}

function unAppCStr(str, chrs) {
    var i = arguments[2] ? arguments[2] : 0;
    if(i >= str.length) {
        return E(chrs);
    } else {
        return {_:1,a:str.charCodeAt(i),b:new T(function() {
            return unAppCStr(str,chrs,i+1);
        })};
    }
}

function charCodeAt(str, i) {return str.charCodeAt(i);}

function fromJSStr(str) {
    return unCStr(E(str));
}

function toJSStr(hsstr) {
    var s = '';
    for(var str = E(hsstr); str._ == 1; str = E(str.b)) {
        s += String.fromCharCode(E(str.a));
    }
    return s;
}

// newMutVar
function nMV(val) {
    return ({x: val});
}

// readMutVar
function rMV(mv) {
    return mv.x;
}

// writeMutVar
function wMV(mv, val) {
    mv.x = val;
}

// atomicModifyMutVar
function mMV(mv, f) {
    var x = B(A(f, [mv.x]));
    mv.x = x.a;
    return x.b;
}

function localeEncoding() {
    var le = newByteArr(5);
    le['v']['i8'][0] = 'U'.charCodeAt(0);
    le['v']['i8'][1] = 'T'.charCodeAt(0);
    le['v']['i8'][2] = 'F'.charCodeAt(0);
    le['v']['i8'][3] = '-'.charCodeAt(0);
    le['v']['i8'][4] = '8'.charCodeAt(0);
    return le;
}

var isDoubleNaN = isNaN;
var isFloatNaN = isNaN;

function isDoubleInfinite(d) {
    return (d === Infinity);
}
var isFloatInfinite = isDoubleInfinite;

function isDoubleNegativeZero(x) {
    return (x===0 && (1/x)===-Infinity);
}
var isFloatNegativeZero = isDoubleNegativeZero;

function strEq(a, b) {
    return a == b;
}

function strOrd(a, b) {
    if(a < b) {
        return 0;
    } else if(a == b) {
        return 1;
    }
    return 2;
}

/* Convert a JS exception into a Haskell JSException */
function __hsException(e) {
  e = e.toString();
  var x = new Long(2904464383, 3929545892, true);
  var y = new Long(3027541338, 3270546716, true);
  var t = new T5(0, x, y
                  , new T5(0, x, y
                            , unCStr("haste-prim")
                            , unCStr("Haste.Prim.Foreign")
                            , unCStr("JSException")), __Z, __Z);
  var show = function(x) {return unCStr(E(x).a);}
  var dispEx = function(x) {return unCStr("JavaScript exception: " + E(x).a);}
  var showList = function(_, s) {return unAppCStr(e, s);}
  var showsPrec = function(_, _p, s) {return unAppCStr(e, s);}
  var showDict = new T3(0, showsPrec, show, showList);
  var self;
  var fromEx = function(_) {return new T1(1, self);}
  var dict = new T5(0, t, showDict, null /* toException */, fromEx, dispEx);
  self = new T2(0, dict, new T1(0, e));
  return self;
}

function jsCatch(act, handler) {
    try {
        return B(A(act,[0]));
    } catch(e) {
        if(typeof e._ === 'undefined') {
            e = __hsException(e);
        }
        return B(A(handler,[e, 0]));
    }
}

/* Haste represents constructors internally using 1 for the first constructor,
   2 for the second, etc.
   However, dataToTag should use 0, 1, 2, etc. Also, booleans might be unboxed.
 */
function dataToTag(x) {
    if(x instanceof Object) {
        return x._;
    } else {
        return x;
    }
}

function __word_encodeDouble(d, e) {
    return d * Math.pow(2,e);
}

var __word_encodeFloat = __word_encodeDouble;
var jsRound = Math.round, rintDouble = jsRound, rintFloat = jsRound;
var jsTrunc = Math.trunc ? Math.trunc : function(x) {
    return x < 0 ? Math.ceil(x) : Math.floor(x);
};
function jsRoundW(n) {
    return Math.abs(jsTrunc(n));
}
var realWorld = undefined;
if(typeof _ == 'undefined') {
    var _ = undefined;
}

function popCnt64(i) {
    return popCnt(i.low) + popCnt(i.high);
}

function popCnt(i) {
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

function __clz(bits, x) {
    x &= (Math.pow(2, bits)-1);
    if(x === 0) {
        return bits;
    } else {
        return bits - (1 + Math.floor(Math.log(x)/Math.LN2));
    }
}

// TODO: can probably be done much faster with arithmetic tricks like __clz
function __ctz(bits, x) {
    var y = 1;
    x &= (Math.pow(2, bits)-1);
    if(x === 0) {
        return bits;
    }
    for(var i = 0; i < bits; ++i) {
        if(y & x) {
            return i;
        } else {
            y <<= 1;
        }
    }
    return 0;
}

// Scratch space for byte arrays.
var rts_scratchBuf = new ArrayBuffer(8);
var rts_scratchW32 = new Uint32Array(rts_scratchBuf);
var rts_scratchFloat = new Float32Array(rts_scratchBuf);
var rts_scratchDouble = new Float64Array(rts_scratchBuf);

function decodeFloat(x) {
    if(x === 0) {
        return __decodedZeroF;
    }
    rts_scratchFloat[0] = x;
    var sign = x < 0 ? -1 : 1;
    var exp = ((rts_scratchW32[0] >> 23) & 0xff) - 150;
    var man = rts_scratchW32[0] & 0x7fffff;
    if(exp === 0) {
        ++exp;
    } else {
        man |= (1 << 23);
    }
    return {_:0, a:sign*man, b:exp};
}

var __decodedZero = {_:0,a:1,b:0,c:0,d:0};
var __decodedZeroF = {_:0,a:1,b:0};

function decodeDouble(x) {
    if(x === 0) {
        // GHC 7.10+ *really* doesn't like 0 to be represented as anything
        // but zeroes all the way.
        return __decodedZero;
    }
    rts_scratchDouble[0] = x;
    var sign = x < 0 ? -1 : 1;
    var manHigh = rts_scratchW32[1] & 0xfffff;
    var manLow = rts_scratchW32[0];
    var exp = ((rts_scratchW32[1] >> 20) & 0x7ff) - 1075;
    if(exp === 0) {
        ++exp;
    } else {
        manHigh |= (1 << 20);
    }
    return {_:0, a:sign, b:manHigh, c:manLow, d:exp};
}

function isNull(obj) {
    return obj === null;
}

function jsRead(str) {
    return Number(str);
}

function jsShowI(val) {return val.toString();}
function jsShow(val) {
    var ret = val.toString();
    return val == Math.round(val) ? ret + '.0' : ret;
}

window['jsGetMouseCoords'] = function jsGetMouseCoords(e) {
    var posx = 0;
    var posy = 0;
    if (!e) var e = window.event;
    if (e.pageX || e.pageY) 	{
	posx = e.pageX;
	posy = e.pageY;
    }
    else if (e.clientX || e.clientY) 	{
	posx = e.clientX + document.body.scrollLeft
	    + document.documentElement.scrollLeft;
	posy = e.clientY + document.body.scrollTop
	    + document.documentElement.scrollTop;
    }
    return [posx - (e.currentTarget.offsetLeft || 0),
	    posy - (e.currentTarget.offsetTop || 0)];
}

var jsRand = Math.random;

// Concatenate a Haskell list of JS strings
function jsCat(strs, sep) {
    var arr = [];
    strs = E(strs);
    while(strs._) {
        strs = E(strs);
        arr.push(E(strs.a));
        strs = E(strs.b);
    }
    return arr.join(sep);
}

// Parse a JSON message into a Haste.JSON.JSON value.
// As this pokes around inside Haskell values, it'll need to be updated if:
// * Haste.JSON.JSON changes;
// * E() starts to choke on non-thunks;
// * data constructor code generation changes; or
// * Just and Nothing change tags.
function jsParseJSON(str) {
    try {
        var js = JSON.parse(str);
        var hs = toHS(js);
    } catch(_) {
        return __Z;
    }
    return {_:1,a:hs};
}

function toHS(obj) {
    switch(typeof obj) {
    case 'number':
        return {_:0, a:jsRead(obj)};
    case 'string':
        return {_:1, a:obj};
    case 'boolean':
        return {_:2, a:obj}; // Booleans are special wrt constructor tags!
    case 'object':
        if(obj instanceof Array) {
            return {_:3, a:arr2lst_json(obj, 0)};
        } else if (obj == null) {
            return {_:5};
        } else {
            // Object type but not array - it's a dictionary.
            // The RFC doesn't say anything about the ordering of keys, but
            // considering that lots of people rely on keys being "in order" as
            // defined by "the same way someone put them in at the other end,"
            // it's probably a good idea to put some cycles into meeting their
            // misguided expectations.
            var ks = [];
            for(var k in obj) {
                ks.unshift(k);
            }
            var xs = [0];
            for(var i = 0; i < ks.length; i++) {
                xs = {_:1, a:{_:0, a:ks[i], b:toHS(obj[ks[i]])}, b:xs};
            }
            return {_:4, a:xs};
        }
    }
}

function arr2lst_json(arr, elem) {
    if(elem >= arr.length) {
        return __Z;
    }
    return {_:1, a:toHS(arr[elem]), b:new T(function() {return arr2lst_json(arr,elem+1);}),c:true}
}

/* gettimeofday(2) */
function gettimeofday(tv, _tz) {
    var t = new Date().getTime();
    writeOffAddr("i32", 4, tv, 0, (t/1000)|0);
    writeOffAddr("i32", 4, tv, 1, ((t%1000)*1000)|0);
    return 0;
}

// Create a little endian ArrayBuffer representation of something.
function toABHost(v, n, x) {
    var a = new ArrayBuffer(n);
    new window[v](a)[0] = x;
    return a;
}

function toABSwap(v, n, x) {
    var a = new ArrayBuffer(n);
    new window[v](a)[0] = x;
    var bs = new Uint8Array(a);
    for(var i = 0, j = n-1; i < j; ++i, --j) {
        var tmp = bs[i];
        bs[i] = bs[j];
        bs[j] = tmp;
    }
    return a;
}

window['toABle'] = toABHost;
window['toABbe'] = toABSwap;

// Swap byte order if host is not little endian.
var buffer = new ArrayBuffer(2);
new DataView(buffer).setInt16(0, 256, true);
if(new Int16Array(buffer)[0] !== 256) {
    window['toABle'] = toABSwap;
    window['toABbe'] = toABHost;
}

/* bn.js by Fedor Indutny, see doc/LICENSE.bn for license */
var __bn = {};
(function (module, exports) {
'use strict';

function BN(number, base, endian) {
  // May be `new BN(bn)` ?
  if (number !== null &&
      typeof number === 'object' &&
      Array.isArray(number.words)) {
    return number;
  }

  this.negative = 0;
  this.words = null;
  this.length = 0;

  if (base === 'le' || base === 'be') {
    endian = base;
    base = 10;
  }

  if (number !== null)
    this._init(number || 0, base || 10, endian || 'be');
}
if (typeof module === 'object')
  module.exports = BN;
else
  exports.BN = BN;

BN.BN = BN;
BN.wordSize = 26;

BN.max = function max(left, right) {
  if (left.cmp(right) > 0)
    return left;
  else
    return right;
};

BN.min = function min(left, right) {
  if (left.cmp(right) < 0)
    return left;
  else
    return right;
};

BN.prototype._init = function init(number, base, endian) {
  if (typeof number === 'number') {
    return this._initNumber(number, base, endian);
  } else if (typeof number === 'object') {
    return this._initArray(number, base, endian);
  }
  if (base === 'hex')
    base = 16;

  number = number.toString().replace(/\s+/g, '');
  var start = 0;
  if (number[0] === '-')
    start++;

  if (base === 16)
    this._parseHex(number, start);
  else
    this._parseBase(number, base, start);

  if (number[0] === '-')
    this.negative = 1;

  this.strip();

  if (endian !== 'le')
    return;

  this._initArray(this.toArray(), base, endian);
};

BN.prototype._initNumber = function _initNumber(number, base, endian) {
  if (number < 0) {
    this.negative = 1;
    number = -number;
  }
  if (number < 0x4000000) {
    this.words = [ number & 0x3ffffff ];
    this.length = 1;
  } else if (number < 0x10000000000000) {
    this.words = [
      number & 0x3ffffff,
      (number / 0x4000000) & 0x3ffffff
    ];
    this.length = 2;
  } else {
    this.words = [
      number & 0x3ffffff,
      (number / 0x4000000) & 0x3ffffff,
      1
    ];
    this.length = 3;
  }

  if (endian !== 'le')
    return;

  // Reverse the bytes
  this._initArray(this.toArray(), base, endian);
};

BN.prototype._initArray = function _initArray(number, base, endian) {
  if (number.length <= 0) {
    this.words = [ 0 ];
    this.length = 1;
    return this;
  }

  this.length = Math.ceil(number.length / 3);
  this.words = new Array(this.length);
  for (var i = 0; i < this.length; i++)
    this.words[i] = 0;

  var off = 0;
  if (endian === 'be') {
    for (var i = number.length - 1, j = 0; i >= 0; i -= 3) {
      var w = number[i] | (number[i - 1] << 8) | (number[i - 2] << 16);
      this.words[j] |= (w << off) & 0x3ffffff;
      this.words[j + 1] = (w >>> (26 - off)) & 0x3ffffff;
      off += 24;
      if (off >= 26) {
        off -= 26;
        j++;
      }
    }
  } else if (endian === 'le') {
    for (var i = 0, j = 0; i < number.length; i += 3) {
      var w = number[i] | (number[i + 1] << 8) | (number[i + 2] << 16);
      this.words[j] |= (w << off) & 0x3ffffff;
      this.words[j + 1] = (w >>> (26 - off)) & 0x3ffffff;
      off += 24;
      if (off >= 26) {
        off -= 26;
        j++;
      }
    }
  }
  return this.strip();
};

function parseHex(str, start, end) {
  var r = 0;
  var len = Math.min(str.length, end);
  for (var i = start; i < len; i++) {
    var c = str.charCodeAt(i) - 48;

    r <<= 4;

    // 'a' - 'f'
    if (c >= 49 && c <= 54)
      r |= c - 49 + 0xa;

    // 'A' - 'F'
    else if (c >= 17 && c <= 22)
      r |= c - 17 + 0xa;

    // '0' - '9'
    else
      r |= c & 0xf;
  }
  return r;
}

BN.prototype._parseHex = function _parseHex(number, start) {
  // Create possibly bigger array to ensure that it fits the number
  this.length = Math.ceil((number.length - start) / 6);
  this.words = new Array(this.length);
  for (var i = 0; i < this.length; i++)
    this.words[i] = 0;

  // Scan 24-bit chunks and add them to the number
  var off = 0;
  for (var i = number.length - 6, j = 0; i >= start; i -= 6) {
    var w = parseHex(number, i, i + 6);
    this.words[j] |= (w << off) & 0x3ffffff;
    this.words[j + 1] |= w >>> (26 - off) & 0x3fffff;
    off += 24;
    if (off >= 26) {
      off -= 26;
      j++;
    }
  }
  if (i + 6 !== start) {
    var w = parseHex(number, start, i + 6);
    this.words[j] |= (w << off) & 0x3ffffff;
    this.words[j + 1] |= w >>> (26 - off) & 0x3fffff;
  }
  this.strip();
};

function parseBase(str, start, end, mul) {
  var r = 0;
  var len = Math.min(str.length, end);
  for (var i = start; i < len; i++) {
    var c = str.charCodeAt(i) - 48;

    r *= mul;

    // 'a'
    if (c >= 49)
      r += c - 49 + 0xa;

    // 'A'
    else if (c >= 17)
      r += c - 17 + 0xa;

    // '0' - '9'
    else
      r += c;
  }
  return r;
}

BN.prototype._parseBase = function _parseBase(number, base, start) {
  // Initialize as zero
  this.words = [ 0 ];
  this.length = 1;

  // Find length of limb in base
  for (var limbLen = 0, limbPow = 1; limbPow <= 0x3ffffff; limbPow *= base)
    limbLen++;
  limbLen--;
  limbPow = (limbPow / base) | 0;

  var total = number.length - start;
  var mod = total % limbLen;
  var end = Math.min(total, total - mod) + start;

  var word = 0;
  for (var i = start; i < end; i += limbLen) {
    word = parseBase(number, i, i + limbLen, base);

    this.imuln(limbPow);
    if (this.words[0] + word < 0x4000000)
      this.words[0] += word;
    else
      this._iaddn(word);
  }

  if (mod !== 0) {
    var pow = 1;
    var word = parseBase(number, i, number.length, base);

    for (var i = 0; i < mod; i++)
      pow *= base;
    this.imuln(pow);
    if (this.words[0] + word < 0x4000000)
      this.words[0] += word;
    else
      this._iaddn(word);
  }
};

BN.prototype.copy = function copy(dest) {
  dest.words = new Array(this.length);
  for (var i = 0; i < this.length; i++)
    dest.words[i] = this.words[i];
  dest.length = this.length;
  dest.negative = this.negative;
};

BN.prototype.clone = function clone() {
  var r = new BN(null);
  this.copy(r);
  return r;
};

// Remove leading `0` from `this`
BN.prototype.strip = function strip() {
  while (this.length > 1 && this.words[this.length - 1] === 0)
    this.length--;
  return this._normSign();
};

BN.prototype._normSign = function _normSign() {
  // -0 = 0
  if (this.length === 1 && this.words[0] === 0)
    this.negative = 0;
  return this;
};

var zeros = [
  '',
  '0',
  '00',
  '000',
  '0000',
  '00000',
  '000000',
  '0000000',
  '00000000',
  '000000000',
  '0000000000',
  '00000000000',
  '000000000000',
  '0000000000000',
  '00000000000000',
  '000000000000000',
  '0000000000000000',
  '00000000000000000',
  '000000000000000000',
  '0000000000000000000',
  '00000000000000000000',
  '000000000000000000000',
  '0000000000000000000000',
  '00000000000000000000000',
  '000000000000000000000000',
  '0000000000000000000000000'
];

var groupSizes = [
  0, 0,
  25, 16, 12, 11, 10, 9, 8,
  8, 7, 7, 7, 7, 6, 6,
  6, 6, 6, 6, 6, 5, 5,
  5, 5, 5, 5, 5, 5, 5,
  5, 5, 5, 5, 5, 5, 5
];

var groupBases = [
  0, 0,
  33554432, 43046721, 16777216, 48828125, 60466176, 40353607, 16777216,
  43046721, 10000000, 19487171, 35831808, 62748517, 7529536, 11390625,
  16777216, 24137569, 34012224, 47045881, 64000000, 4084101, 5153632,
  6436343, 7962624, 9765625, 11881376, 14348907, 17210368, 20511149,
  24300000, 28629151, 33554432, 39135393, 45435424, 52521875, 60466176
];

BN.prototype.toString = function toString(base, padding) {
  base = base || 10;
  var padding = padding | 0 || 1;
  if (base === 16 || base === 'hex') {
    var out = '';
    var off = 0;
    var carry = 0;
    for (var i = 0; i < this.length; i++) {
      var w = this.words[i];
      var word = (((w << off) | carry) & 0xffffff).toString(16);
      carry = (w >>> (24 - off)) & 0xffffff;
      if (carry !== 0 || i !== this.length - 1)
        out = zeros[6 - word.length] + word + out;
      else
        out = word + out;
      off += 2;
      if (off >= 26) {
        off -= 26;
        i--;
      }
    }
    if (carry !== 0)
      out = carry.toString(16) + out;
    while (out.length % padding !== 0)
      out = '0' + out;
    if (this.negative !== 0)
      out = '-' + out;
    return out;
  } else if (base === (base | 0) && base >= 2 && base <= 36) {
    var groupSize = groupSizes[base];
    var groupBase = groupBases[base];
    var out = '';
    var c = this.clone();
    c.negative = 0;
    while (c.cmpn(0) !== 0) {
      var r = c.modn(groupBase).toString(base);
      c = c.idivn(groupBase);

      if (c.cmpn(0) !== 0)
        out = zeros[groupSize - r.length] + r + out;
      else
        out = r + out;
    }
    if (this.cmpn(0) === 0)
      out = '0' + out;
    while (out.length % padding !== 0)
      out = '0' + out;
    if (this.negative !== 0)
      out = '-' + out;
    return out;
  } else {
    throw 'Base should be between 2 and 36';
  }
};

BN.prototype.toJSON = function toJSON() {
  return this.toString(16);
};

BN.prototype.toArray = function toArray(endian, length) {
  this.strip();
  var littleEndian = endian === 'le';
  var res = new Array(this.byteLength());
  res[0] = 0;

  var q = this.clone();
  if (!littleEndian) {
    // Assume big-endian
    for (var i = 0; q.cmpn(0) !== 0; i++) {
      var b = q.andln(0xff);
      q.iushrn(8);

      res[res.length - i - 1] = b;
    }
  } else {
    for (var i = 0; q.cmpn(0) !== 0; i++) {
      var b = q.andln(0xff);
      q.iushrn(8);

      res[i] = b;
    }
  }

  if (length) {
    while (res.length < length) {
      if (littleEndian)
        res.push(0);
      else
        res.unshift(0);
    }
  }

  return res;
};

if (Math.clz32) {
  BN.prototype._countBits = function _countBits(w) {
    return 32 - Math.clz32(w);
  };
} else {
  BN.prototype._countBits = function _countBits(w) {
    var t = w;
    var r = 0;
    if (t >= 0x1000) {
      r += 13;
      t >>>= 13;
    }
    if (t >= 0x40) {
      r += 7;
      t >>>= 7;
    }
    if (t >= 0x8) {
      r += 4;
      t >>>= 4;
    }
    if (t >= 0x02) {
      r += 2;
      t >>>= 2;
    }
    return r + t;
  };
}

// Return number of used bits in a BN
BN.prototype.bitLength = function bitLength() {
  var hi = 0;
  var w = this.words[this.length - 1];
  var hi = this._countBits(w);
  return (this.length - 1) * 26 + hi;
};

BN.prototype.byteLength = function byteLength() {
  return Math.ceil(this.bitLength() / 8);
};

// Return negative clone of `this`
BN.prototype.neg = function neg() {
  if (this.cmpn(0) === 0)
    return this.clone();

  var r = this.clone();
  r.negative = this.negative ^ 1;
  return r;
};

BN.prototype.ineg = function ineg() {
  this.negative ^= 1;
  return this;
};

// Or `num` with `this` in-place
BN.prototype.iuor = function iuor(num) {
  while (this.length < num.length)
    this.words[this.length++] = 0;

  for (var i = 0; i < num.length; i++)
    this.words[i] = this.words[i] | num.words[i];

  return this.strip();
};

BN.prototype.ior = function ior(num) {
  //assert((this.negative | num.negative) === 0);
  return this.iuor(num);
};


// Or `num` with `this`
BN.prototype.or = function or(num) {
  if (this.length > num.length)
    return this.clone().ior(num);
  else
    return num.clone().ior(this);
};

BN.prototype.uor = function uor(num) {
  if (this.length > num.length)
    return this.clone().iuor(num);
  else
    return num.clone().iuor(this);
};


// And `num` with `this` in-place
BN.prototype.iuand = function iuand(num) {
  // b = min-length(num, this)
  var b;
  if (this.length > num.length)
    b = num;
  else
    b = this;

  for (var i = 0; i < b.length; i++)
    this.words[i] = this.words[i] & num.words[i];

  this.length = b.length;

  return this.strip();
};

BN.prototype.iand = function iand(num) {
  //assert((this.negative | num.negative) === 0);
  return this.iuand(num);
};


// And `num` with `this`
BN.prototype.and = function and(num) {
  if (this.length > num.length)
    return this.clone().iand(num);
  else
    return num.clone().iand(this);
};

BN.prototype.uand = function uand(num) {
  if (this.length > num.length)
    return this.clone().iuand(num);
  else
    return num.clone().iuand(this);
};


// Xor `num` with `this` in-place
BN.prototype.iuxor = function iuxor(num) {
  // a.length > b.length
  var a;
  var b;
  if (this.length > num.length) {
    a = this;
    b = num;
  } else {
    a = num;
    b = this;
  }

  for (var i = 0; i < b.length; i++)
    this.words[i] = a.words[i] ^ b.words[i];

  if (this !== a)
    for (; i < a.length; i++)
      this.words[i] = a.words[i];

  this.length = a.length;

  return this.strip();
};

BN.prototype.ixor = function ixor(num) {
  //assert((this.negative | num.negative) === 0);
  return this.iuxor(num);
};


// Xor `num` with `this`
BN.prototype.xor = function xor(num) {
  if (this.length > num.length)
    return this.clone().ixor(num);
  else
    return num.clone().ixor(this);
};

BN.prototype.uxor = function uxor(num) {
  if (this.length > num.length)
    return this.clone().iuxor(num);
  else
    return num.clone().iuxor(this);
};


// Add `num` to `this` in-place
BN.prototype.iadd = function iadd(num) {
  // negative + positive
  if (this.negative !== 0 && num.negative === 0) {
    this.negative = 0;
    var r = this.isub(num);
    this.negative ^= 1;
    return this._normSign();

  // positive + negative
  } else if (this.negative === 0 && num.negative !== 0) {
    num.negative = 0;
    var r = this.isub(num);
    num.negative = 1;
    return r._normSign();
  }

  // a.length > b.length
  var a;
  var b;
  if (this.length > num.length) {
    a = this;
    b = num;
  } else {
    a = num;
    b = this;
  }

  var carry = 0;
  for (var i = 0; i < b.length; i++) {
    var r = (a.words[i] | 0) + (b.words[i] | 0) + carry;
    this.words[i] = r & 0x3ffffff;
    carry = r >>> 26;
  }
  for (; carry !== 0 && i < a.length; i++) {
    var r = (a.words[i] | 0) + carry;
    this.words[i] = r & 0x3ffffff;
    carry = r >>> 26;
  }

  this.length = a.length;
  if (carry !== 0) {
    this.words[this.length] = carry;
    this.length++;
  // Copy the rest of the words
  } else if (a !== this) {
    for (; i < a.length; i++)
      this.words[i] = a.words[i];
  }

  return this;
};

// Add `num` to `this`
BN.prototype.add = function add(num) {
  if (num.negative !== 0 && this.negative === 0) {
    num.negative = 0;
    var res = this.sub(num);
    num.negative ^= 1;
    return res;
  } else if (num.negative === 0 && this.negative !== 0) {
    this.negative = 0;
    var res = num.sub(this);
    this.negative = 1;
    return res;
  }

  if (this.length > num.length)
    return this.clone().iadd(num);
  else
    return num.clone().iadd(this);
};

// Subtract `num` from `this` in-place
BN.prototype.isub = function isub(num) {
  // this - (-num) = this + num
  if (num.negative !== 0) {
    num.negative = 0;
    var r = this.iadd(num);
    num.negative = 1;
    return r._normSign();

  // -this - num = -(this + num)
  } else if (this.negative !== 0) {
    this.negative = 0;
    this.iadd(num);
    this.negative = 1;
    return this._normSign();
  }

  // At this point both numbers are positive
  var cmp = this.cmp(num);

  // Optimization - zeroify
  if (cmp === 0) {
    this.negative = 0;
    this.length = 1;
    this.words[0] = 0;
    return this;
  }

  // a > b
  var a;
  var b;
  if (cmp > 0) {
    a = this;
    b = num;
  } else {
    a = num;
    b = this;
  }

  var carry = 0;
  for (var i = 0; i < b.length; i++) {
    var r = (a.words[i] | 0) - (b.words[i] | 0) + carry;
    carry = r >> 26;
    this.words[i] = r & 0x3ffffff;
  }
  for (; carry !== 0 && i < a.length; i++) {
    var r = (a.words[i] | 0) + carry;
    carry = r >> 26;
    this.words[i] = r & 0x3ffffff;
  }

  // Copy rest of the words
  if (carry === 0 && i < a.length && a !== this)
    for (; i < a.length; i++)
      this.words[i] = a.words[i];
  this.length = Math.max(this.length, i);

  if (a !== this)
    this.negative = 1;

  return this.strip();
};

// Subtract `num` from `this`
BN.prototype.sub = function sub(num) {
  return this.clone().isub(num);
};

function smallMulTo(self, num, out) {
  out.negative = num.negative ^ self.negative;
  var len = (self.length + num.length) | 0;
  out.length = len;
  len = (len - 1) | 0;

  // Peel one iteration (compiler can't do it, because of code complexity)
  var a = self.words[0] | 0;
  var b = num.words[0] | 0;
  var r = a * b;

  var lo = r & 0x3ffffff;
  var carry = (r / 0x4000000) | 0;
  out.words[0] = lo;

  for (var k = 1; k < len; k++) {
    // Sum all words with the same `i + j = k` and accumulate `ncarry`,
    // note that ncarry could be >= 0x3ffffff
    var ncarry = carry >>> 26;
    var rword = carry & 0x3ffffff;
    var maxJ = Math.min(k, num.length - 1);
    for (var j = Math.max(0, k - self.length + 1); j <= maxJ; j++) {
      var i = (k - j) | 0;
      var a = self.words[i] | 0;
      var b = num.words[j] | 0;
      var r = a * b;

      var lo = r & 0x3ffffff;
      ncarry = (ncarry + ((r / 0x4000000) | 0)) | 0;
      lo = (lo + rword) | 0;
      rword = lo & 0x3ffffff;
      ncarry = (ncarry + (lo >>> 26)) | 0;
    }
    out.words[k] = rword | 0;
    carry = ncarry | 0;
  }
  if (carry !== 0) {
    out.words[k] = carry | 0;
  } else {
    out.length--;
  }

  return out.strip();
}

function bigMulTo(self, num, out) {
  out.negative = num.negative ^ self.negative;
  out.length = self.length + num.length;

  var carry = 0;
  var hncarry = 0;
  for (var k = 0; k < out.length - 1; k++) {
    // Sum all words with the same `i + j = k` and accumulate `ncarry`,
    // note that ncarry could be >= 0x3ffffff
    var ncarry = hncarry;
    hncarry = 0;
    var rword = carry & 0x3ffffff;
    var maxJ = Math.min(k, num.length - 1);
    for (var j = Math.max(0, k - self.length + 1); j <= maxJ; j++) {
      var i = k - j;
      var a = self.words[i] | 0;
      var b = num.words[j] | 0;
      var r = a * b;

      var lo = r & 0x3ffffff;
      ncarry = (ncarry + ((r / 0x4000000) | 0)) | 0;
      lo = (lo + rword) | 0;
      rword = lo & 0x3ffffff;
      ncarry = (ncarry + (lo >>> 26)) | 0;

      hncarry += ncarry >>> 26;
      ncarry &= 0x3ffffff;
    }
    out.words[k] = rword;
    carry = ncarry;
    ncarry = hncarry;
  }
  if (carry !== 0) {
    out.words[k] = carry;
  } else {
    out.length--;
  }

  return out.strip();
}

BN.prototype.mulTo = function mulTo(num, out) {
  var res;
  if (this.length + num.length < 63)
    res = smallMulTo(this, num, out);
  else
    res = bigMulTo(this, num, out);
  return res;
};

// Multiply `this` by `num`
BN.prototype.mul = function mul(num) {
  var out = new BN(null);
  out.words = new Array(this.length + num.length);
  return this.mulTo(num, out);
};

// In-place Multiplication
BN.prototype.imul = function imul(num) {
  if (this.cmpn(0) === 0 || num.cmpn(0) === 0) {
    this.words[0] = 0;
    this.length = 1;
    return this;
  }

  var tlen = this.length;
  var nlen = num.length;

  this.negative = num.negative ^ this.negative;
  this.length = this.length + num.length;
  this.words[this.length - 1] = 0;

  for (var k = this.length - 2; k >= 0; k--) {
    // Sum all words with the same `i + j = k` and accumulate `carry`,
    // note that carry could be >= 0x3ffffff
    var carry = 0;
    var rword = 0;
    var maxJ = Math.min(k, nlen - 1);
    for (var j = Math.max(0, k - tlen + 1); j <= maxJ; j++) {
      var i = k - j;
      var a = this.words[i] | 0;
      var b = num.words[j] | 0;
      var r = a * b;

      var lo = r & 0x3ffffff;
      carry += (r / 0x4000000) | 0;
      lo += rword;
      rword = lo & 0x3ffffff;
      carry += lo >>> 26;
    }
    this.words[k] = rword;
    this.words[k + 1] += carry;
    carry = 0;
  }

  // Propagate overflows
  var carry = 0;
  for (var i = 1; i < this.length; i++) {
    var w = (this.words[i] | 0) + carry;
    this.words[i] = w & 0x3ffffff;
    carry = w >>> 26;
  }

  return this.strip();
};

BN.prototype.imuln = function imuln(num) {
  // Carry
  var carry = 0;
  for (var i = 0; i < this.length; i++) {
    var w = (this.words[i] | 0) * num;
    var lo = (w & 0x3ffffff) + (carry & 0x3ffffff);
    carry >>= 26;
    carry += (w / 0x4000000) | 0;
    // NOTE: lo is 27bit maximum
    carry += lo >>> 26;
    this.words[i] = lo & 0x3ffffff;
  }

  if (carry !== 0) {
    this.words[i] = carry;
    this.length++;
  }

  return this;
};

BN.prototype.muln = function muln(num) {
  return this.clone().imuln(num);
};

// `this` * `this`
BN.prototype.sqr = function sqr() {
  return this.mul(this);
};

// `this` * `this` in-place
BN.prototype.isqr = function isqr() {
  return this.mul(this);
};

// Shift-left in-place
BN.prototype.iushln = function iushln(bits) {
  var r = bits % 26;
  var s = (bits - r) / 26;
  var carryMask = (0x3ffffff >>> (26 - r)) << (26 - r);

  if (r !== 0) {
    var carry = 0;
    for (var i = 0; i < this.length; i++) {
      var newCarry = this.words[i] & carryMask;
      var c = ((this.words[i] | 0) - newCarry) << r;
      this.words[i] = c | carry;
      carry = newCarry >>> (26 - r);
    }
    if (carry) {
      this.words[i] = carry;
      this.length++;
    }
  }

  if (s !== 0) {
    for (var i = this.length - 1; i >= 0; i--)
      this.words[i + s] = this.words[i];
    for (var i = 0; i < s; i++)
      this.words[i] = 0;
    this.length += s;
  }

  return this.strip();
};

BN.prototype.ishln = function ishln(bits) {
  return this.iushln(bits);
};

// Shift-right in-place
BN.prototype.iushrn = function iushrn(bits, hint, extended) {
  var h;
  if (hint)
    h = (hint - (hint % 26)) / 26;
  else
    h = 0;

  var r = bits % 26;
  var s = Math.min((bits - r) / 26, this.length);
  var mask = 0x3ffffff ^ ((0x3ffffff >>> r) << r);
  var maskedWords = extended;

  h -= s;
  h = Math.max(0, h);

  // Extended mode, copy masked part
  if (maskedWords) {
    for (var i = 0; i < s; i++)
      maskedWords.words[i] = this.words[i];
    maskedWords.length = s;
  }

  if (s === 0) {
    // No-op, we should not move anything at all
  } else if (this.length > s) {
    this.length -= s;
    for (var i = 0; i < this.length; i++)
      this.words[i] = this.words[i + s];
  } else {
    this.words[0] = 0;
    this.length = 1;
  }

  var carry = 0;
  for (var i = this.length - 1; i >= 0 && (carry !== 0 || i >= h); i--) {
    var word = this.words[i] | 0;
    this.words[i] = (carry << (26 - r)) | (word >>> r);
    carry = word & mask;
  }

  // Push carried bits as a mask
  if (maskedWords && carry !== 0)
    maskedWords.words[maskedWords.length++] = carry;

  if (this.length === 0) {
    this.words[0] = 0;
    this.length = 1;
  }

  this.strip();

  return this;
};

BN.prototype.ishrn = function ishrn(bits, hint, extended) {
  return this.iushrn(bits, hint, extended);
};

// Shift-left
BN.prototype.shln = function shln(bits) {
  var x = this.clone();
  var neg = x.negative;
  x.negative = false;
  x.ishln(bits);
  x.negative = neg;
  return x;
};

BN.prototype.ushln = function ushln(bits) {
  return this.clone().iushln(bits);
};

// Shift-right
BN.prototype.shrn = function shrn(bits) {
  var x = this.clone();
  if(x.negative) {
      x.negative = false;
      x.ishrn(bits);
      x.negative = true;
      return x.isubn(1);
  } else {
      return x.ishrn(bits);
  }
};

BN.prototype.ushrn = function ushrn(bits) {
  return this.clone().iushrn(bits);
};

// Test if n bit is set
BN.prototype.testn = function testn(bit) {
  var r = bit % 26;
  var s = (bit - r) / 26;
  var q = 1 << r;

  // Fast case: bit is much higher than all existing words
  if (this.length <= s) {
    return false;
  }

  // Check bit and return
  var w = this.words[s];

  return !!(w & q);
};

// Add plain number `num` to `this`
BN.prototype.iaddn = function iaddn(num) {
  if (num < 0)
    return this.isubn(-num);

  // Possible sign change
  if (this.negative !== 0) {
    if (this.length === 1 && (this.words[0] | 0) < num) {
      this.words[0] = num - (this.words[0] | 0);
      this.negative = 0;
      return this;
    }

    this.negative = 0;
    this.isubn(num);
    this.negative = 1;
    return this;
  }

  // Add without checks
  return this._iaddn(num);
};

BN.prototype._iaddn = function _iaddn(num) {
  this.words[0] += num;

  // Carry
  for (var i = 0; i < this.length && this.words[i] >= 0x4000000; i++) {
    this.words[i] -= 0x4000000;
    if (i === this.length - 1)
      this.words[i + 1] = 1;
    else
      this.words[i + 1]++;
  }
  this.length = Math.max(this.length, i + 1);

  return this;
};

// Subtract plain number `num` from `this`
BN.prototype.isubn = function isubn(num) {
  if (num < 0)
    return this.iaddn(-num);

  if (this.negative !== 0) {
    this.negative = 0;
    this.iaddn(num);
    this.negative = 1;
    return this;
  }

  this.words[0] -= num;

  // Carry
  for (var i = 0; i < this.length && this.words[i] < 0; i++) {
    this.words[i] += 0x4000000;
    this.words[i + 1] -= 1;
  }

  return this.strip();
};

BN.prototype.addn = function addn(num) {
  return this.clone().iaddn(num);
};

BN.prototype.subn = function subn(num) {
  return this.clone().isubn(num);
};

BN.prototype.iabs = function iabs() {
  this.negative = 0;

  return this;
};

BN.prototype.abs = function abs() {
  return this.clone().iabs();
};

BN.prototype._ishlnsubmul = function _ishlnsubmul(num, mul, shift) {
  // Bigger storage is needed
  var len = num.length + shift;
  var i;
  if (this.words.length < len) {
    var t = new Array(len);
    for (var i = 0; i < this.length; i++)
      t[i] = this.words[i];
    this.words = t;
  } else {
    i = this.length;
  }

  // Zeroify rest
  this.length = Math.max(this.length, len);
  for (; i < this.length; i++)
    this.words[i] = 0;

  var carry = 0;
  for (var i = 0; i < num.length; i++) {
    var w = (this.words[i + shift] | 0) + carry;
    var right = (num.words[i] | 0) * mul;
    w -= right & 0x3ffffff;
    carry = (w >> 26) - ((right / 0x4000000) | 0);
    this.words[i + shift] = w & 0x3ffffff;
  }
  for (; i < this.length - shift; i++) {
    var w = (this.words[i + shift] | 0) + carry;
    carry = w >> 26;
    this.words[i + shift] = w & 0x3ffffff;
  }

  if (carry === 0)
    return this.strip();

  carry = 0;
  for (var i = 0; i < this.length; i++) {
    var w = -(this.words[i] | 0) + carry;
    carry = w >> 26;
    this.words[i] = w & 0x3ffffff;
  }
  this.negative = 1;

  return this.strip();
};

BN.prototype._wordDiv = function _wordDiv(num, mode) {
  var shift = this.length - num.length;

  var a = this.clone();
  var b = num;

  // Normalize
  var bhi = b.words[b.length - 1] | 0;
  var bhiBits = this._countBits(bhi);
  shift = 26 - bhiBits;
  if (shift !== 0) {
    b = b.ushln(shift);
    a.iushln(shift);
    bhi = b.words[b.length - 1] | 0;
  }

  // Initialize quotient
  var m = a.length - b.length;
  var q;

  if (mode !== 'mod') {
    q = new BN(null);
    q.length = m + 1;
    q.words = new Array(q.length);
    for (var i = 0; i < q.length; i++)
      q.words[i] = 0;
  }

  var diff = a.clone()._ishlnsubmul(b, 1, m);
  if (diff.negative === 0) {
    a = diff;
    if (q)
      q.words[m] = 1;
  }

  for (var j = m - 1; j >= 0; j--) {
    var qj = (a.words[b.length + j] | 0) * 0x4000000 +
             (a.words[b.length + j - 1] | 0);

    // NOTE: (qj / bhi) is (0x3ffffff * 0x4000000 + 0x3ffffff) / 0x2000000 max
    // (0x7ffffff)
    qj = Math.min((qj / bhi) | 0, 0x3ffffff);

    a._ishlnsubmul(b, qj, j);
    while (a.negative !== 0) {
      qj--;
      a.negative = 0;
      a._ishlnsubmul(b, 1, j);
      if (a.cmpn(0) !== 0)
        a.negative ^= 1;
    }
    if (q)
      q.words[j] = qj;
  }
  if (q)
    q.strip();
  a.strip();

  // Denormalize
  if (mode !== 'div' && shift !== 0)
    a.iushrn(shift);
  return { div: q ? q : null, mod: a };
};

BN.prototype.divmod = function divmod(num, mode, positive) {
  if (this.negative !== 0 && num.negative === 0) {
    var res = this.neg().divmod(num, mode);
    var div;
    var mod;
    if (mode !== 'mod')
      div = res.div.neg();
    if (mode !== 'div') {
      mod = res.mod.neg();
      if (positive && mod.neg)
        mod = mod.add(num);
    }
    return {
      div: div,
      mod: mod
    };
  } else if (this.negative === 0 && num.negative !== 0) {
    var res = this.divmod(num.neg(), mode);
    var div;
    if (mode !== 'mod')
      div = res.div.neg();
    return { div: div, mod: res.mod };
  } else if ((this.negative & num.negative) !== 0) {
    var res = this.neg().divmod(num.neg(), mode);
    var mod;
    if (mode !== 'div') {
      mod = res.mod.neg();
      if (positive && mod.neg)
        mod = mod.isub(num);
    }
    return {
      div: res.div,
      mod: mod
    };
  }

  // Both numbers are positive at this point

  // Strip both numbers to approximate shift value
  if (num.length > this.length || this.cmp(num) < 0)
    return { div: new BN(0), mod: this };

  // Very short reduction
  if (num.length === 1) {
    if (mode === 'div')
      return { div: this.divn(num.words[0]), mod: null };
    else if (mode === 'mod')
      return { div: null, mod: new BN(this.modn(num.words[0])) };
    return {
      div: this.divn(num.words[0]),
      mod: new BN(this.modn(num.words[0]))
    };
  }

  return this._wordDiv(num, mode);
};

// Find `this` / `num`
BN.prototype.div = function div(num) {
  return this.divmod(num, 'div', false).div;
};

// Find `this` % `num`
BN.prototype.mod = function mod(num) {
  return this.divmod(num, 'mod', false).mod;
};

BN.prototype.umod = function umod(num) {
  return this.divmod(num, 'mod', true).mod;
};

// Find Round(`this` / `num`)
BN.prototype.divRound = function divRound(num) {
  var dm = this.divmod(num);

  // Fast case - exact division
  if (dm.mod.cmpn(0) === 0)
    return dm.div;

  var mod = dm.div.negative !== 0 ? dm.mod.isub(num) : dm.mod;

  var half = num.ushrn(1);
  var r2 = num.andln(1);
  var cmp = mod.cmp(half);

  // Round down
  if (cmp < 0 || r2 === 1 && cmp === 0)
    return dm.div;

  // Round up
  return dm.div.negative !== 0 ? dm.div.isubn(1) : dm.div.iaddn(1);
};

BN.prototype.modn = function modn(num) {
  var p = (1 << 26) % num;

  var acc = 0;
  for (var i = this.length - 1; i >= 0; i--)
    acc = (p * acc + (this.words[i] | 0)) % num;

  return acc;
};

// In-place division by number
BN.prototype.idivn = function idivn(num) {
  var carry = 0;
  for (var i = this.length - 1; i >= 0; i--) {
    var w = (this.words[i] | 0) + carry * 0x4000000;
    this.words[i] = (w / num) | 0;
    carry = w % num;
  }

  return this.strip();
};

BN.prototype.divn = function divn(num) {
  return this.clone().idivn(num);
};

BN.prototype.isEven = function isEven() {
  return (this.words[0] & 1) === 0;
};

BN.prototype.isOdd = function isOdd() {
  return (this.words[0] & 1) === 1;
};

// And first word and num
BN.prototype.andln = function andln(num) {
  return this.words[0] & num;
};

BN.prototype.cmpn = function cmpn(num) {
  var negative = num < 0;
  if (negative)
    num = -num;

  if (this.negative !== 0 && !negative)
    return -1;
  else if (this.negative === 0 && negative)
    return 1;

  num &= 0x3ffffff;
  this.strip();

  var res;
  if (this.length > 1) {
    res = 1;
  } else {
    var w = this.words[0] | 0;
    res = w === num ? 0 : w < num ? -1 : 1;
  }
  if (this.negative !== 0)
    res = -res;
  return res;
};

// Compare two numbers and return:
// 1 - if `this` > `num`
// 0 - if `this` == `num`
// -1 - if `this` < `num`
BN.prototype.cmp = function cmp(num) {
  if (this.negative !== 0 && num.negative === 0)
    return -1;
  else if (this.negative === 0 && num.negative !== 0)
    return 1;

  var res = this.ucmp(num);
  if (this.negative !== 0)
    return -res;
  else
    return res;
};

// Unsigned comparison
BN.prototype.ucmp = function ucmp(num) {
  // At this point both numbers have the same sign
  if (this.length > num.length)
    return 1;
  else if (this.length < num.length)
    return -1;

  var res = 0;
  for (var i = this.length - 1; i >= 0; i--) {
    var a = this.words[i] | 0;
    var b = num.words[i] | 0;

    if (a === b)
      continue;
    if (a < b)
      res = -1;
    else if (a > b)
      res = 1;
    break;
  }
  return res;
};
})(undefined, __bn);

// MVar implementation.
// Since Haste isn't concurrent, takeMVar and putMVar don't block on empty
// and full MVars respectively, but terminate the program since they would
// otherwise be blocking forever.

function newMVar() {
    return ({empty: true});
}

function tryTakeMVar(mv) {
    if(mv.empty) {
        return {_:0, a:0, b:undefined};
    } else {
        var val = mv.x;
        mv.empty = true;
        mv.x = null;
        return {_:0, a:1, b:val};
    }
}

function takeMVar(mv) {
    if(mv.empty) {
        // TODO: real BlockedOnDeadMVar exception, perhaps?
        err("Attempted to take empty MVar!");
    }
    var val = mv.x;
    mv.empty = true;
    mv.x = null;
    return val;
}

function putMVar(mv, val) {
    if(!mv.empty) {
        // TODO: real BlockedOnDeadMVar exception, perhaps?
        err("Attempted to put full MVar!");
    }
    mv.empty = false;
    mv.x = val;
}

function tryPutMVar(mv, val) {
    if(!mv.empty) {
        return 0;
    } else {
        mv.empty = false;
        mv.x = val;
        return 1;
    }
}

function sameMVar(a, b) {
    return (a == b);
}

function isEmptyMVar(mv) {
    return mv.empty ? 1 : 0;
}

// Implementation of stable names.
// Unlike native GHC, the garbage collector isn't going to move data around
// in a way that we can detect, so each object could serve as its own stable
// name if it weren't for the fact we can't turn a JS reference into an
// integer.
// So instead, each object has a unique integer attached to it, which serves
// as its stable name.

var __next_stable_name = 1;
var __stable_table;

function makeStableName(x) {
    if(x instanceof Object) {
        if(!x.stableName) {
            x.stableName = __next_stable_name;
            __next_stable_name += 1;
        }
        return {type: 'obj', name: x.stableName};
    } else {
        return {type: 'prim', name: Number(x)};
    }
}

function eqStableName(x, y) {
    return (x.type == y.type && x.name == y.name) ? 1 : 0;
}

// TODO: inefficient compared to real fromInt?
__bn.Z = new __bn.BN(0);
__bn.ONE = new __bn.BN(1);
__bn.MOD32 = new __bn.BN(0x100000000); // 2^32
var I_fromNumber = function(x) {return new __bn.BN(x);}
var I_fromInt = I_fromNumber;
var I_fromBits = function(lo,hi) {
    var x = new __bn.BN(lo >>> 0);
    var y = new __bn.BN(hi >>> 0);
    y.ishln(32);
    x.iadd(y);
    return x;
}
var I_fromString = function(s) {return new __bn.BN(s);}
var I_toInt = function(x) {return I_toNumber(x.mod(__bn.MOD32));}
var I_toWord = function(x) {return I_toInt(x) >>> 0;};
// TODO: inefficient!
var I_toNumber = function(x) {return Number(x.toString());}
var I_equals = function(a,b) {return a.cmp(b) === 0;}
var I_compare = function(a,b) {return a.cmp(b);}
var I_compareInt = function(x,i) {return x.cmp(new __bn.BN(i));}
var I_negate = function(x) {return x.neg();}
var I_add = function(a,b) {return a.add(b);}
var I_sub = function(a,b) {return a.sub(b);}
var I_mul = function(a,b) {return a.mul(b);}
var I_mod = function(a,b) {return I_rem(I_add(b, I_rem(a, b)), b);}
var I_quotRem = function(a,b) {
    var qr = a.divmod(b);
    return {_:0, a:qr.div, b:qr.mod};
}
var I_div = function(a,b) {
    if((a.cmp(__bn.Z)>=0) != (a.cmp(__bn.Z)>=0)) {
        if(a.cmp(a.rem(b), __bn.Z) !== 0) {
            return a.div(b).sub(__bn.ONE);
        }
    }
    return a.div(b);
}
var I_divMod = function(a,b) {
    return {_:0, a:I_div(a,b), b:a.mod(b)};
}
var I_quot = function(a,b) {return a.div(b);}
var I_rem = function(a,b) {return a.mod(b);}
var I_and = function(a,b) {return a.and(b);}
var I_or = function(a,b) {return a.or(b);}
var I_xor = function(a,b) {return a.xor(b);}
var I_shiftLeft = function(a,b) {return a.shln(b);}
var I_shiftRight = function(a,b) {return a.shrn(b);}
var I_signum = function(x) {return x.cmp(new __bn.BN(0));}
var I_abs = function(x) {return x.abs();}
var I_decodeDouble = function(x) {
    var dec = decodeDouble(x);
    var mantissa = I_fromBits(dec.c, dec.b);
    if(dec.a < 0) {
        mantissa = I_negate(mantissa);
    }
    return {_:0, a:dec.d, b:mantissa};
}
var I_toString = function(x) {return x.toString();}
var I_fromRat = function(a, b) {
    return I_toNumber(a) / I_toNumber(b);
}

function I_fromInt64(x) {
    if(x.isNegative()) {
        return I_negate(I_fromInt64(x.negate()));
    } else {
        return I_fromBits(x.low, x.high);
    }
}

function I_toInt64(x) {
    if(x.negative) {
        return I_toInt64(I_negate(x)).negate();
    } else {
        return new Long(I_toInt(x), I_toInt(I_shiftRight(x,32)));
    }
}

function I_fromWord64(x) {
    return I_fromBits(x.toInt(), x.shru(32).toInt());
}

function I_toWord64(x) {
    var w = I_toInt64(x);
    w.unsigned = true;
    return w;
}

/**
 * @license long.js (c) 2013 Daniel Wirtz <dcode@dcode.io>
 * Released under the Apache License, Version 2.0
 * see: https://github.com/dcodeIO/long.js for details
 */
function Long(low, high, unsigned) {
    this.low = low | 0;
    this.high = high | 0;
    this.unsigned = !!unsigned;
}

var INT_CACHE = {};
var UINT_CACHE = {};
function cacheable(x, u) {
    return u ? 0 <= (x >>>= 0) && x < 256 : -128 <= (x |= 0) && x < 128;
}

function __fromInt(value, unsigned) {
    var obj, cachedObj, cache;
    if (unsigned) {
        if (cache = cacheable(value >>>= 0, true)) {
            cachedObj = UINT_CACHE[value];
            if (cachedObj)
                return cachedObj;
        }
        obj = new Long(value, (value | 0) < 0 ? -1 : 0, true);
        if (cache)
            UINT_CACHE[value] = obj;
        return obj;
    } else {
        if (cache = cacheable(value |= 0, false)) {
            cachedObj = INT_CACHE[value];
            if (cachedObj)
                return cachedObj;
        }
        obj = new Long(value, value < 0 ? -1 : 0, false);
        if (cache)
            INT_CACHE[value] = obj;
        return obj;
    }
}

function __fromNumber(value, unsigned) {
    if (isNaN(value) || !isFinite(value))
        return unsigned ? UZERO : ZERO;
    if (unsigned) {
        if (value < 0)
            return UZERO;
        if (value >= TWO_PWR_64_DBL)
            return MAX_UNSIGNED_VALUE;
    } else {
        if (value <= -TWO_PWR_63_DBL)
            return MIN_VALUE;
        if (value + 1 >= TWO_PWR_63_DBL)
            return MAX_VALUE;
    }
    if (value < 0)
        return __fromNumber(-value, unsigned).neg();
    return new Long((value % TWO_PWR_32_DBL) | 0, (value / TWO_PWR_32_DBL) | 0, unsigned);
}
var pow_dbl = Math.pow;
var TWO_PWR_16_DBL = 1 << 16;
var TWO_PWR_24_DBL = 1 << 24;
var TWO_PWR_32_DBL = TWO_PWR_16_DBL * TWO_PWR_16_DBL;
var TWO_PWR_64_DBL = TWO_PWR_32_DBL * TWO_PWR_32_DBL;
var TWO_PWR_63_DBL = TWO_PWR_64_DBL / 2;
var TWO_PWR_24 = __fromInt(TWO_PWR_24_DBL);
var ZERO = __fromInt(0);
Long.ZERO = ZERO;
var UZERO = __fromInt(0, true);
Long.UZERO = UZERO;
var ONE = __fromInt(1);
Long.ONE = ONE;
var UONE = __fromInt(1, true);
Long.UONE = UONE;
var NEG_ONE = __fromInt(-1);
Long.NEG_ONE = NEG_ONE;
var MAX_VALUE = new Long(0xFFFFFFFF|0, 0x7FFFFFFF|0, false);
Long.MAX_VALUE = MAX_VALUE;
var MAX_UNSIGNED_VALUE = new Long(0xFFFFFFFF|0, 0xFFFFFFFF|0, true);
Long.MAX_UNSIGNED_VALUE = MAX_UNSIGNED_VALUE;
var MIN_VALUE = new Long(0, 0x80000000|0, false);
Long.MIN_VALUE = MIN_VALUE;
var __lp = Long.prototype;
__lp.toInt = function() {return this.unsigned ? this.low >>> 0 : this.low;};
__lp.toNumber = function() {
    if (this.unsigned)
        return ((this.high >>> 0) * TWO_PWR_32_DBL) + (this.low >>> 0);
    return this.high * TWO_PWR_32_DBL + (this.low >>> 0);
};
__lp.isZero = function() {return this.high === 0 && this.low === 0;};
__lp.isNegative = function() {return !this.unsigned && this.high < 0;};
__lp.isOdd = function() {return (this.low & 1) === 1;};
__lp.eq = function(other) {
    if (this.unsigned !== other.unsigned && (this.high >>> 31) === 1 && (other.high >>> 31) === 1)
        return false;
    return this.high === other.high && this.low === other.low;
};
__lp.neq = function(other) {return !this.eq(other);};
__lp.lt = function(other) {return this.comp(other) < 0;};
__lp.lte = function(other) {return this.comp(other) <= 0;};
__lp.gt = function(other) {return this.comp(other) > 0;};
__lp.gte = function(other) {return this.comp(other) >= 0;};
__lp.compare = function(other) {
    if (this.eq(other))
        return 0;
    var thisNeg = this.isNegative(),
        otherNeg = other.isNegative();
    if (thisNeg && !otherNeg)
        return -1;
    if (!thisNeg && otherNeg)
        return 1;
    if (!this.unsigned)
        return this.sub(other).isNegative() ? -1 : 1;
    return (other.high >>> 0) > (this.high >>> 0) || (other.high === this.high && (other.low >>> 0) > (this.low >>> 0)) ? -1 : 1;
};
__lp.comp = __lp.compare;
__lp.negate = function() {
    if (!this.unsigned && this.eq(MIN_VALUE))
        return MIN_VALUE;
    return this.not().add(ONE);
};
__lp.neg = __lp.negate;
__lp.add = function(addend) {
    var a48 = this.high >>> 16;
    var a32 = this.high & 0xFFFF;
    var a16 = this.low >>> 16;
    var a00 = this.low & 0xFFFF;

    var b48 = addend.high >>> 16;
    var b32 = addend.high & 0xFFFF;
    var b16 = addend.low >>> 16;
    var b00 = addend.low & 0xFFFF;

    var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
    c00 += a00 + b00;
    c16 += c00 >>> 16;
    c00 &= 0xFFFF;
    c16 += a16 + b16;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c32 += a32 + b32;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c48 += a48 + b48;
    c48 &= 0xFFFF;
    return new Long((c16 << 16) | c00, (c48 << 16) | c32, this.unsigned);
};
__lp.subtract = function(subtrahend) {return this.add(subtrahend.neg());};
__lp.sub = __lp.subtract;
__lp.multiply = function(multiplier) {
    if (this.isZero())
        return ZERO;
    if (multiplier.isZero())
        return ZERO;
    if (this.eq(MIN_VALUE))
        return multiplier.isOdd() ? MIN_VALUE : ZERO;
    if (multiplier.eq(MIN_VALUE))
        return this.isOdd() ? MIN_VALUE : ZERO;

    if (this.isNegative()) {
        if (multiplier.isNegative())
            return this.neg().mul(multiplier.neg());
        else
            return this.neg().mul(multiplier).neg();
    } else if (multiplier.isNegative())
        return this.mul(multiplier.neg()).neg();

    if (this.lt(TWO_PWR_24) && multiplier.lt(TWO_PWR_24))
        return __fromNumber(this.toNumber() * multiplier.toNumber(), this.unsigned);

    var a48 = this.high >>> 16;
    var a32 = this.high & 0xFFFF;
    var a16 = this.low >>> 16;
    var a00 = this.low & 0xFFFF;

    var b48 = multiplier.high >>> 16;
    var b32 = multiplier.high & 0xFFFF;
    var b16 = multiplier.low >>> 16;
    var b00 = multiplier.low & 0xFFFF;

    var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
    c00 += a00 * b00;
    c16 += c00 >>> 16;
    c00 &= 0xFFFF;
    c16 += a16 * b00;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c16 += a00 * b16;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c32 += a32 * b00;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c32 += a16 * b16;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c32 += a00 * b32;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c48 += a48 * b00 + a32 * b16 + a16 * b32 + a00 * b48;
    c48 &= 0xFFFF;
    return new Long((c16 << 16) | c00, (c48 << 16) | c32, this.unsigned);
};
__lp.mul = __lp.multiply;
__lp.divide = function(divisor) {
    if (divisor.isZero())
        throw Error('division by zero');
    if (this.isZero())
        return this.unsigned ? UZERO : ZERO;
    var approx, rem, res;
    if (this.eq(MIN_VALUE)) {
        if (divisor.eq(ONE) || divisor.eq(NEG_ONE))
            return MIN_VALUE;
        else if (divisor.eq(MIN_VALUE))
            return ONE;
        else {
            var halfThis = this.shr(1);
            approx = halfThis.div(divisor).shl(1);
            if (approx.eq(ZERO)) {
                return divisor.isNegative() ? ONE : NEG_ONE;
            } else {
                rem = this.sub(divisor.mul(approx));
                res = approx.add(rem.div(divisor));
                return res;
            }
        }
    } else if (divisor.eq(MIN_VALUE))
        return this.unsigned ? UZERO : ZERO;
    if (this.isNegative()) {
        if (divisor.isNegative())
            return this.neg().div(divisor.neg());
        return this.neg().div(divisor).neg();
    } else if (divisor.isNegative())
        return this.div(divisor.neg()).neg();

    res = ZERO;
    rem = this;
    while (rem.gte(divisor)) {
        approx = Math.max(1, Math.floor(rem.toNumber() / divisor.toNumber()));
        var log2 = Math.ceil(Math.log(approx) / Math.LN2),
            delta = (log2 <= 48) ? 1 : pow_dbl(2, log2 - 48),
            approxRes = __fromNumber(approx),
            approxRem = approxRes.mul(divisor);
        while (approxRem.isNegative() || approxRem.gt(rem)) {
            approx -= delta;
            approxRes = __fromNumber(approx, this.unsigned);
            approxRem = approxRes.mul(divisor);
        }
        if (approxRes.isZero())
            approxRes = ONE;

        res = res.add(approxRes);
        rem = rem.sub(approxRem);
    }
    return res;
};
__lp.div = __lp.divide;
__lp.modulo = function(divisor) {return this.sub(this.div(divisor).mul(divisor));};
__lp.mod = __lp.modulo;
__lp.not = function not() {return new Long(~this.low, ~this.high, this.unsigned);};
__lp.and = function(other) {return new Long(this.low & other.low, this.high & other.high, this.unsigned);};
__lp.or = function(other) {return new Long(this.low | other.low, this.high | other.high, this.unsigned);};
__lp.xor = function(other) {return new Long(this.low ^ other.low, this.high ^ other.high, this.unsigned);};

__lp.shl = function(numBits) {
    if ((numBits &= 63) === 0)
        return this;
    else if (numBits < 32)
        return new Long(this.low << numBits, (this.high << numBits) | (this.low >>> (32 - numBits)), this.unsigned);
    else
        return new Long(0, this.low << (numBits - 32), this.unsigned);
};

__lp.shr = function(numBits) {
    if ((numBits &= 63) === 0)
        return this;
    else if (numBits < 32)
        return new Long((this.low >>> numBits) | (this.high << (32 - numBits)), this.high >> numBits, this.unsigned);
    else
        return new Long(this.high >> (numBits - 32), this.high >= 0 ? 0 : -1, this.unsigned);
};

__lp.shru = function(numBits) {
    numBits &= 63;
    if (numBits === 0)
        return this;
    else {
        var high = this.high;
        if (numBits < 32) {
            var low = this.low;
            return new Long((low >>> numBits) | (high << (32 - numBits)), high >>> numBits, this.unsigned);
        } else if (numBits === 32)
            return new Long(high, 0, this.unsigned);
        else
            return new Long(high >>> (numBits - 32), 0, this.unsigned);
    }
};

__lp.toSigned = function() {return this.unsigned ? new Long(this.low, this.high, false) : this;};
__lp.toUnsigned = function() {return this.unsigned ? this : new Long(this.low, this.high, true);};

// Int64
function hs_eqInt64(x, y) {return x.eq(y);}
function hs_neInt64(x, y) {return x.neq(y);}
function hs_ltInt64(x, y) {return x.lt(y);}
function hs_leInt64(x, y) {return x.lte(y);}
function hs_gtInt64(x, y) {return x.gt(y);}
function hs_geInt64(x, y) {return x.gte(y);}
function hs_quotInt64(x, y) {return x.div(y);}
function hs_remInt64(x, y) {return x.modulo(y);}
function hs_plusInt64(x, y) {return x.add(y);}
function hs_minusInt64(x, y) {return x.subtract(y);}
function hs_timesInt64(x, y) {return x.multiply(y);}
function hs_negateInt64(x) {return x.negate();}
function hs_uncheckedIShiftL64(x, bits) {return x.shl(bits);}
function hs_uncheckedIShiftRA64(x, bits) {return x.shr(bits);}
function hs_uncheckedIShiftRL64(x, bits) {return x.shru(bits);}
function hs_int64ToInt(x) {return x.toInt();}
var hs_intToInt64 = __fromInt;

// Word64
function hs_wordToWord64(x) {return __fromInt(x, true);}
function hs_word64ToWord(x) {return x.toInt(x);}
function hs_mkWord64(low, high) {return new Long(low,high,true);}
function hs_and64(a,b) {return a.and(b);};
function hs_or64(a,b) {return a.or(b);};
function hs_xor64(a,b) {return a.xor(b);};
function hs_not64(x) {return x.not();}
var hs_eqWord64 = hs_eqInt64;
var hs_neWord64 = hs_neInt64;
var hs_ltWord64 = hs_ltInt64;
var hs_leWord64 = hs_leInt64;
var hs_gtWord64 = hs_gtInt64;
var hs_geWord64 = hs_geInt64;
var hs_quotWord64 = hs_quotInt64;
var hs_remWord64 = hs_remInt64;
var hs_uncheckedShiftL64 = hs_uncheckedIShiftL64;
var hs_uncheckedShiftRL64 = hs_uncheckedIShiftRL64;
function hs_int64ToWord64(x) {return x.toUnsigned();}
function hs_word64ToInt64(x) {return x.toSigned();}

// Joseph Myers' MD5 implementation, ported to work on typed arrays.
// Used under the BSD license.
function md5cycle(x, k) {
    var a = x[0], b = x[1], c = x[2], d = x[3];

    a = ff(a, b, c, d, k[0], 7, -680876936);
    d = ff(d, a, b, c, k[1], 12, -389564586);
    c = ff(c, d, a, b, k[2], 17,  606105819);
    b = ff(b, c, d, a, k[3], 22, -1044525330);
    a = ff(a, b, c, d, k[4], 7, -176418897);
    d = ff(d, a, b, c, k[5], 12,  1200080426);
    c = ff(c, d, a, b, k[6], 17, -1473231341);
    b = ff(b, c, d, a, k[7], 22, -45705983);
    a = ff(a, b, c, d, k[8], 7,  1770035416);
    d = ff(d, a, b, c, k[9], 12, -1958414417);
    c = ff(c, d, a, b, k[10], 17, -42063);
    b = ff(b, c, d, a, k[11], 22, -1990404162);
    a = ff(a, b, c, d, k[12], 7,  1804603682);
    d = ff(d, a, b, c, k[13], 12, -40341101);
    c = ff(c, d, a, b, k[14], 17, -1502002290);
    b = ff(b, c, d, a, k[15], 22,  1236535329);

    a = gg(a, b, c, d, k[1], 5, -165796510);
    d = gg(d, a, b, c, k[6], 9, -1069501632);
    c = gg(c, d, a, b, k[11], 14,  643717713);
    b = gg(b, c, d, a, k[0], 20, -373897302);
    a = gg(a, b, c, d, k[5], 5, -701558691);
    d = gg(d, a, b, c, k[10], 9,  38016083);
    c = gg(c, d, a, b, k[15], 14, -660478335);
    b = gg(b, c, d, a, k[4], 20, -405537848);
    a = gg(a, b, c, d, k[9], 5,  568446438);
    d = gg(d, a, b, c, k[14], 9, -1019803690);
    c = gg(c, d, a, b, k[3], 14, -187363961);
    b = gg(b, c, d, a, k[8], 20,  1163531501);
    a = gg(a, b, c, d, k[13], 5, -1444681467);
    d = gg(d, a, b, c, k[2], 9, -51403784);
    c = gg(c, d, a, b, k[7], 14,  1735328473);
    b = gg(b, c, d, a, k[12], 20, -1926607734);

    a = hh(a, b, c, d, k[5], 4, -378558);
    d = hh(d, a, b, c, k[8], 11, -2022574463);
    c = hh(c, d, a, b, k[11], 16,  1839030562);
    b = hh(b, c, d, a, k[14], 23, -35309556);
    a = hh(a, b, c, d, k[1], 4, -1530992060);
    d = hh(d, a, b, c, k[4], 11,  1272893353);
    c = hh(c, d, a, b, k[7], 16, -155497632);
    b = hh(b, c, d, a, k[10], 23, -1094730640);
    a = hh(a, b, c, d, k[13], 4,  681279174);
    d = hh(d, a, b, c, k[0], 11, -358537222);
    c = hh(c, d, a, b, k[3], 16, -722521979);
    b = hh(b, c, d, a, k[6], 23,  76029189);
    a = hh(a, b, c, d, k[9], 4, -640364487);
    d = hh(d, a, b, c, k[12], 11, -421815835);
    c = hh(c, d, a, b, k[15], 16,  530742520);
    b = hh(b, c, d, a, k[2], 23, -995338651);

    a = ii(a, b, c, d, k[0], 6, -198630844);
    d = ii(d, a, b, c, k[7], 10,  1126891415);
    c = ii(c, d, a, b, k[14], 15, -1416354905);
    b = ii(b, c, d, a, k[5], 21, -57434055);
    a = ii(a, b, c, d, k[12], 6,  1700485571);
    d = ii(d, a, b, c, k[3], 10, -1894986606);
    c = ii(c, d, a, b, k[10], 15, -1051523);
    b = ii(b, c, d, a, k[1], 21, -2054922799);
    a = ii(a, b, c, d, k[8], 6,  1873313359);
    d = ii(d, a, b, c, k[15], 10, -30611744);
    c = ii(c, d, a, b, k[6], 15, -1560198380);
    b = ii(b, c, d, a, k[13], 21,  1309151649);
    a = ii(a, b, c, d, k[4], 6, -145523070);
    d = ii(d, a, b, c, k[11], 10, -1120210379);
    c = ii(c, d, a, b, k[2], 15,  718787259);
    b = ii(b, c, d, a, k[9], 21, -343485551);

    x[0] = add32(a, x[0]);
    x[1] = add32(b, x[1]);
    x[2] = add32(c, x[2]);
    x[3] = add32(d, x[3]);

}

function cmn(q, a, b, x, s, t) {
    a = add32(add32(a, q), add32(x, t));
    return add32((a << s) | (a >>> (32 - s)), b);
}

function ff(a, b, c, d, x, s, t) {
    return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}

function gg(a, b, c, d, x, s, t) {
    return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}

function hh(a, b, c, d, x, s, t) {
    return cmn(b ^ c ^ d, a, b, x, s, t);
}

function ii(a, b, c, d, x, s, t) {
    return cmn(c ^ (b | (~d)), a, b, x, s, t);
}

function md51(s, n) {
    var a = s['v']['w8'];
    var orig_n = n,
        state = [1732584193, -271733879, -1732584194, 271733878], i;
    for (i=64; i<=n; i+=64) {
        md5cycle(state, md5blk(a.subarray(i-64, i)));
    }
    a = a.subarray(i-64);
    n = n < (i-64) ? 0 : n-(i-64);
    var tail = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];
    for (i=0; i<n; i++)
        tail[i>>2] |= a[i] << ((i%4) << 3);
    tail[i>>2] |= 0x80 << ((i%4) << 3);
    if (i > 55) {
        md5cycle(state, tail);
        for (i=0; i<16; i++) tail[i] = 0;
    }
    tail[14] = orig_n*8;
    md5cycle(state, tail);
    return state;
}
window['md51'] = md51;

function md5blk(s) {
    var md5blks = [], i;
    for (i=0; i<64; i+=4) {
        md5blks[i>>2] = s[i]
            + (s[i+1] << 8)
            + (s[i+2] << 16)
            + (s[i+3] << 24);
    }
    return md5blks;
}

var hex_chr = '0123456789abcdef'.split('');

function rhex(n)
{
    var s='', j=0;
    for(; j<4; j++)
        s += hex_chr[(n >> (j * 8 + 4)) & 0x0F]
        + hex_chr[(n >> (j * 8)) & 0x0F];
    return s;
}

function hex(x) {
    for (var i=0; i<x.length; i++)
        x[i] = rhex(x[i]);
    return x.join('');
}

function md5(s, n) {
    return hex(md51(s, n));
}

window['md5'] = md5;

function add32(a, b) {
    return (a + b) & 0xFFFFFFFF;
}

function __hsbase_MD5Init(ctx) {}
// Note that this is a one time "update", since that's all that's used by
// GHC.Fingerprint.
function __hsbase_MD5Update(ctx, s, n) {
    ctx.md5 = md51(s, n);
}
function __hsbase_MD5Final(out, ctx) {
    var a = out['v']['i32'];
    a[0] = ctx.md5[0];
    a[1] = ctx.md5[1];
    a[2] = ctx.md5[2];
    a[3] = ctx.md5[3];
}

// Functions for dealing with arrays.

function newArr(n, x) {
    var arr = new Array(n);
    for(var i = 0; i < n; ++i) {
        arr[i] = x;
    }
    return arr;
}

// Create all views at once; perhaps it's wasteful, but it's better than having
// to check for the right view at each read or write.
function newByteArr(n) {
    // Pad the thing to multiples of 8.
    var padding = 8 - n % 8;
    if(padding < 8) {
        n += padding;
    }
    return new ByteArray(new ArrayBuffer(n));
}

// Wrap a JS ArrayBuffer into a ByteArray. Truncates the array length to the
// closest multiple of 8 bytes.
function wrapByteArr(buffer) {
    var diff = buffer.byteLength % 8;
    if(diff != 0) {
        var buffer = buffer.slice(0, buffer.byteLength-diff);
    }
    return new ByteArray(buffer);
}

function ByteArray(buffer) {
    var views =
        { 'i8' : new Int8Array(buffer)
        , 'i16': new Int16Array(buffer)
        , 'i32': new Int32Array(buffer)
        , 'w8' : new Uint8Array(buffer)
        , 'w16': new Uint16Array(buffer)
        , 'w32': new Uint32Array(buffer)
        , 'f32': new Float32Array(buffer)
        , 'f64': new Float64Array(buffer)
        };
    this['b'] = buffer;
    this['v'] = views;
    this['off'] = 0;
}
window['newArr'] = newArr;
window['newByteArr'] = newByteArr;
window['wrapByteArr'] = wrapByteArr;
window['ByteArray'] = ByteArray;

// An attempt at emulating pointers enough for ByteString and Text to be
// usable without patching the hell out of them.
// The general idea is that Addr# is a byte array with an associated offset.

function plusAddr(addr, off) {
    var newaddr = {};
    newaddr['off'] = addr['off'] + off;
    newaddr['b']   = addr['b'];
    newaddr['v']   = addr['v'];
    return newaddr;
}

function writeOffAddr(type, elemsize, addr, off, x) {
    addr['v'][type][addr.off/elemsize + off] = x;
}

function writeOffAddr64(addr, off, x) {
    addr['v']['w32'][addr.off/8 + off*2] = x.low;
    addr['v']['w32'][addr.off/8 + off*2 + 1] = x.high;
}

function readOffAddr(type, elemsize, addr, off) {
    return addr['v'][type][addr.off/elemsize + off];
}

function readOffAddr64(signed, addr, off) {
    var w64 = hs_mkWord64( addr['v']['w32'][addr.off/8 + off*2]
                         , addr['v']['w32'][addr.off/8 + off*2 + 1]);
    return signed ? hs_word64ToInt64(w64) : w64;
}

// Two addresses are equal if they point to the same buffer and have the same
// offset. For other comparisons, just use the offsets - nobody in their right
// mind would check if one pointer is less than another, completely unrelated,
// pointer and then act on that information anyway.
function addrEq(a, b) {
    if(a == b) {
        return true;
    }
    return a && b && a['b'] == b['b'] && a['off'] == b['off'];
}

function addrLT(a, b) {
    if(a) {
        return b && a['off'] < b['off'];
    } else {
        return (b != 0); 
    }
}

function addrGT(a, b) {
    if(b) {
        return a && a['off'] > b['off'];
    } else {
        return (a != 0);
    }
}

function withChar(f, charCode) {
    return f(String.fromCharCode(charCode)).charCodeAt(0);
}

function u_towlower(charCode) {
    return withChar(function(c) {return c.toLowerCase()}, charCode);
}

function u_towupper(charCode) {
    return withChar(function(c) {return c.toUpperCase()}, charCode);
}

var u_towtitle = u_towupper;

function u_iswupper(charCode) {
    var c = String.fromCharCode(charCode);
    return c == c.toUpperCase() && c != c.toLowerCase();
}

function u_iswlower(charCode) {
    var c = String.fromCharCode(charCode);
    return  c == c.toLowerCase() && c != c.toUpperCase();
}

function u_iswdigit(charCode) {
    return charCode >= 48 && charCode <= 57;
}

function u_iswcntrl(charCode) {
    return charCode <= 0x1f || charCode == 0x7f;
}

function u_iswspace(charCode) {
    var c = String.fromCharCode(charCode);
    return c.replace(/\s/g,'') != c;
}

function u_iswalpha(charCode) {
    var c = String.fromCharCode(charCode);
    return c.replace(__hs_alphare, '') != c;
}

function u_iswalnum(charCode) {
    return u_iswdigit(charCode) || u_iswalpha(charCode);
}

function u_iswprint(charCode) {
    return !u_iswcntrl(charCode);
}

function u_gencat(c) {
    throw 'u_gencat is only supported with --full-unicode.';
}

// Regex that matches any alphabetic character in any language. Horrible thing.
var __hs_alphare = /[\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC]/g;

// Simulate handles.
// When implementing new handles, remember that passed strings may be thunks,
// and so need to be evaluated before use.

function jsNewHandle(init, read, write, flush, close, seek, tell) {
    var h = {
        read: read || function() {},
        write: write || function() {},
        seek: seek || function() {},
        tell: tell || function() {},
        close: close || function() {},
        flush: flush || function() {}
    };
    init.call(h);
    return h;
}

function jsReadHandle(h, len) {return h.read(len);}
function jsWriteHandle(h, str) {return h.write(str);}
function jsFlushHandle(h) {return h.flush();}
function jsCloseHandle(h) {return h.close();}

function jsMkConWriter(op) {
    return function(str) {
        str = E(str);
        var lines = (this.buf + str).split('\n');
        for(var i = 0; i < lines.length-1; ++i) {
            op.call(console, lines[i]);
        }
        this.buf = lines[lines.length-1];
    }
}

function jsMkStdout() {
    return jsNewHandle(
        function() {this.buf = '';},
        function(_) {return '';},
        jsMkConWriter(console.log),
        function() {console.log(this.buf); this.buf = '';}
    );
}

function jsMkStderr() {
    return jsNewHandle(
        function() {this.buf = '';},
        function(_) {return '';},
        jsMkConWriter(console.warn),
        function() {console.warn(this.buf); this.buf = '';}
    );
}

function jsMkStdin() {
    return jsNewHandle(
        function() {this.buf = '';},
        function(len) {
            while(this.buf.length < len) {
                this.buf += prompt('[stdin]') + '\n';
            }
            var ret = this.buf.substr(0, len);
            this.buf = this.buf.substr(len);
            return ret;
        }
    );
}

// "Weak Pointers". Mostly useless implementation since
// JS does its own GC.

function mkWeak(key, val, fin) {
    fin = !fin? function() {}: fin;
    return {key: key, val: val, fin: fin};
}

function derefWeak(w) {
    return {_:0, a:1, b:E(w).val};
}

function finalizeWeak(w) {
    return {_:0, a:B(A1(E(w).fin, __Z))};
}

/* For foreign import ccall "wrapper" */
function createAdjustor(args, f, a, b) {
    return function(){
        var x = f.apply(null, arguments);
        while(x instanceof F) {x = x.f();}
        return x;
    };
}

var __apply = function(f,as) {
    var arr = [];
    for(; as._ === 1; as = as.b) {
        arr.push(as.a);
    }
    arr.reverse();
    return f.apply(null, arr);
}
var __app0 = function(f) {return f();}
var __app1 = function(f,a) {return f(a);}
var __app2 = function(f,a,b) {return f(a,b);}
var __app3 = function(f,a,b,c) {return f(a,b,c);}
var __app4 = function(f,a,b,c,d) {return f(a,b,c,d);}
var __app5 = function(f,a,b,c,d,e) {return f(a,b,c,d,e);}
var __jsNull = function() {return null;}
var __eq = function(a,b) {return a===b;}
var __createJSFunc = function(arity, f){
    if(f instanceof Function && arity === f.length) {
        return (function() {
            var x = f.apply(null,arguments);
            if(x instanceof T) {
                if(x.f !== __blackhole) {
                    var ff = x.f;
                    x.f = __blackhole;
                    return x.x = ff();
                }
                return x.x;
            } else {
                while(x instanceof F) {
                    x = x.f();
                }
                return E(x);
            }
        });
    } else {
        return (function(){
            var as = Array.prototype.slice.call(arguments);
            as.push(0);
            return E(B(A(f,as)));
        });
    }
}


function __arr2lst(elem,arr) {
    if(elem >= arr.length) {
        return __Z;
    }
    return {_:1,
            a:arr[elem],
            b:new T(function(){return __arr2lst(elem+1,arr);})};
}

function __lst2arr(xs) {
    var arr = [];
    xs = E(xs);
    for(;xs._ === 1; xs = E(xs.b)) {
        arr.push(E(xs.a));
    }
    return arr;
}

var __new = function() {return ({});}
var __set = function(o,k,v) {o[k]=v;}
var __get = function(o,k) {return o[k];}
var __has = function(o,k) {return o[k]!==undefined;}

var _0="deltaZ",_1="deltaY",_2="deltaX",_3=function(_4,_5){var _6=E(_4);return (_6._==0)?E(_5):new T2(1,_6.a,new T(function(){return B(_3(_6.b,_5));}));},_7=function(_8,_9){var _a=jsShowI(_8);return new F(function(){return _3(fromJSStr(_a),_9);});},_b=41,_c=40,_d=function(_e,_f,_g){if(_f>=0){return new F(function(){return _7(_f,_g);});}else{if(_e<=6){return new F(function(){return _7(_f,_g);});}else{return new T2(1,_c,new T(function(){var _h=jsShowI(_f);return B(_3(fromJSStr(_h),new T2(1,_b,_g)));}));}}},_i=new T(function(){return B(unCStr(")"));}),_j=new T(function(){return B(_d(0,2,_i));}),_k=new T(function(){return B(unAppCStr(") is outside of enumeration\'s range (0,",_j));}),_l=function(_m){return new F(function(){return err(B(unAppCStr("toEnum{MouseButton}: tag (",new T(function(){return B(_d(0,_m,_k));}))));});},_n=function(_o,_){return new T(function(){var _p=Number(E(_o)),_q=jsTrunc(_p);if(_q<0){return B(_l(_q));}else{if(_q>2){return B(_l(_q));}else{return _q;}}});},_r=0,_s=new T3(0,_r,_r,_r),_t="button",_u=new T(function(){return eval("jsGetMouseCoords");}),_v=__Z,_w=function(_x,_){var _y=E(_x);if(!_y._){return _v;}else{var _z=B(_w(_y.b,_));return new T2(1,new T(function(){var _A=Number(E(_y.a));return jsTrunc(_A);}),_z);}},_B=function(_C,_){var _D=__arr2lst(0,_C);return new F(function(){return _w(_D,_);});},_E=function(_F,_){return new F(function(){return _B(E(_F),_);});},_G=function(_H,_){return new T(function(){var _I=Number(E(_H));return jsTrunc(_I);});},_J=new T2(0,_G,_E),_K=function(_L,_){var _M=E(_L);if(!_M._){return _v;}else{var _N=B(_K(_M.b,_));return new T2(1,_M.a,_N);}},_O=new T(function(){return B(unCStr("base"));}),_P=new T(function(){return B(unCStr("GHC.IO.Exception"));}),_Q=new T(function(){return B(unCStr("IOException"));}),_R=new T5(0,new Long(4053623282,1685460941,true),new Long(3693590983,2507416641,true),_O,_P,_Q),_S=new T5(0,new Long(4053623282,1685460941,true),new Long(3693590983,2507416641,true),_R,_v,_v),_T=function(_U){return E(_S);},_V=function(_W){return E(E(_W).a);},_X=function(_Y,_Z,_10){var _11=B(A1(_Y,_)),_12=B(A1(_Z,_)),_13=hs_eqWord64(_11.a,_12.a);if(!_13){return __Z;}else{var _14=hs_eqWord64(_11.b,_12.b);return (!_14)?__Z:new T1(1,_10);}},_15=function(_16){var _17=E(_16);return new F(function(){return _X(B(_V(_17.a)),_T,_17.b);});},_18=new T(function(){return B(unCStr(": "));}),_19=new T(function(){return B(unCStr(")"));}),_1a=new T(function(){return B(unCStr(" ("));}),_1b=new T(function(){return B(unCStr("interrupted"));}),_1c=new T(function(){return B(unCStr("system error"));}),_1d=new T(function(){return B(unCStr("unsatisified constraints"));}),_1e=new T(function(){return B(unCStr("user error"));}),_1f=new T(function(){return B(unCStr("permission denied"));}),_1g=new T(function(){return B(unCStr("illegal operation"));}),_1h=new T(function(){return B(unCStr("end of file"));}),_1i=new T(function(){return B(unCStr("resource exhausted"));}),_1j=new T(function(){return B(unCStr("resource busy"));}),_1k=new T(function(){return B(unCStr("does not exist"));}),_1l=new T(function(){return B(unCStr("already exists"));}),_1m=new T(function(){return B(unCStr("resource vanished"));}),_1n=new T(function(){return B(unCStr("timeout"));}),_1o=new T(function(){return B(unCStr("unsupported operation"));}),_1p=new T(function(){return B(unCStr("hardware fault"));}),_1q=new T(function(){return B(unCStr("inappropriate type"));}),_1r=new T(function(){return B(unCStr("invalid argument"));}),_1s=new T(function(){return B(unCStr("failed"));}),_1t=new T(function(){return B(unCStr("protocol error"));}),_1u=function(_1v,_1w){switch(E(_1v)){case 0:return new F(function(){return _3(_1l,_1w);});break;case 1:return new F(function(){return _3(_1k,_1w);});break;case 2:return new F(function(){return _3(_1j,_1w);});break;case 3:return new F(function(){return _3(_1i,_1w);});break;case 4:return new F(function(){return _3(_1h,_1w);});break;case 5:return new F(function(){return _3(_1g,_1w);});break;case 6:return new F(function(){return _3(_1f,_1w);});break;case 7:return new F(function(){return _3(_1e,_1w);});break;case 8:return new F(function(){return _3(_1d,_1w);});break;case 9:return new F(function(){return _3(_1c,_1w);});break;case 10:return new F(function(){return _3(_1t,_1w);});break;case 11:return new F(function(){return _3(_1s,_1w);});break;case 12:return new F(function(){return _3(_1r,_1w);});break;case 13:return new F(function(){return _3(_1q,_1w);});break;case 14:return new F(function(){return _3(_1p,_1w);});break;case 15:return new F(function(){return _3(_1o,_1w);});break;case 16:return new F(function(){return _3(_1n,_1w);});break;case 17:return new F(function(){return _3(_1m,_1w);});break;default:return new F(function(){return _3(_1b,_1w);});}},_1x=new T(function(){return B(unCStr("}"));}),_1y=new T(function(){return B(unCStr("{handle: "));}),_1z=function(_1A,_1B,_1C,_1D,_1E,_1F){var _1G=new T(function(){var _1H=new T(function(){var _1I=new T(function(){var _1J=E(_1D);if(!_1J._){return E(_1F);}else{var _1K=new T(function(){return B(_3(_1J,new T(function(){return B(_3(_19,_1F));},1)));},1);return B(_3(_1a,_1K));}},1);return B(_1u(_1B,_1I));}),_1L=E(_1C);if(!_1L._){return E(_1H);}else{return B(_3(_1L,new T(function(){return B(_3(_18,_1H));},1)));}}),_1M=E(_1E);if(!_1M._){var _1N=E(_1A);if(!_1N._){return E(_1G);}else{var _1O=E(_1N.a);if(!_1O._){var _1P=new T(function(){var _1Q=new T(function(){return B(_3(_1x,new T(function(){return B(_3(_18,_1G));},1)));},1);return B(_3(_1O.a,_1Q));},1);return new F(function(){return _3(_1y,_1P);});}else{var _1R=new T(function(){var _1S=new T(function(){return B(_3(_1x,new T(function(){return B(_3(_18,_1G));},1)));},1);return B(_3(_1O.a,_1S));},1);return new F(function(){return _3(_1y,_1R);});}}}else{return new F(function(){return _3(_1M.a,new T(function(){return B(_3(_18,_1G));},1));});}},_1T=function(_1U){var _1V=E(_1U);return new F(function(){return _1z(_1V.a,_1V.b,_1V.c,_1V.d,_1V.f,_v);});},_1W=function(_1X,_1Y,_1Z){var _20=E(_1Y);return new F(function(){return _1z(_20.a,_20.b,_20.c,_20.d,_20.f,_1Z);});},_21=function(_22,_23){var _24=E(_22);return new F(function(){return _1z(_24.a,_24.b,_24.c,_24.d,_24.f,_23);});},_25=44,_26=93,_27=91,_28=function(_29,_2a,_2b){var _2c=E(_2a);if(!_2c._){return new F(function(){return unAppCStr("[]",_2b);});}else{var _2d=new T(function(){var _2e=new T(function(){var _2f=function(_2g){var _2h=E(_2g);if(!_2h._){return E(new T2(1,_26,_2b));}else{var _2i=new T(function(){return B(A2(_29,_2h.a,new T(function(){return B(_2f(_2h.b));})));});return new T2(1,_25,_2i);}};return B(_2f(_2c.b));});return B(A2(_29,_2c.a,_2e));});return new T2(1,_27,_2d);}},_2j=function(_2k,_2l){return new F(function(){return _28(_21,_2k,_2l);});},_2m=new T3(0,_1W,_1T,_2j),_2n=new T(function(){return new T5(0,_T,_2m,_2o,_15,_1T);}),_2o=function(_2p){return new T2(0,_2n,_2p);},_2q=__Z,_2r=7,_2s=new T(function(){return B(unCStr("Pattern match failure in do expression at src/Haste/Prim/Any.hs:272:5-9"));}),_2t=new T6(0,_2q,_2r,_v,_2s,_2q,_2q),_2u=new T(function(){return B(_2o(_2t));}),_2v=function(_){return new F(function(){return die(_2u);});},_2w=function(_2x){return E(E(_2x).a);},_2y=function(_2z,_2A,_2B,_){var _2C=__arr2lst(0,_2B),_2D=B(_K(_2C,_)),_2E=E(_2D);if(!_2E._){return new F(function(){return _2v(_);});}else{var _2F=E(_2E.b);if(!_2F._){return new F(function(){return _2v(_);});}else{if(!E(_2F.b)._){var _2G=B(A3(_2w,_2z,_2E.a,_)),_2H=B(A3(_2w,_2A,_2F.a,_));return new T2(0,_2G,_2H);}else{return new F(function(){return _2v(_);});}}}},_2I=function(_){return new F(function(){return __jsNull();});},_2J=function(_2K){var _2L=B(A1(_2K,_));return E(_2L);},_2M=new T(function(){return B(_2J(_2I));}),_2N=new T(function(){return E(_2M);}),_2O=function(_2P,_2Q,_){if(E(_2P)==7){var _2R=__app1(E(_u),_2Q),_2S=B(_2y(_J,_J,_2R,_)),_2T=__get(_2Q,E(_2)),_2U=__get(_2Q,E(_1)),_2V=__get(_2Q,E(_0));return new T(function(){return new T3(0,E(_2S),E(_2q),E(new T3(0,_2T,_2U,_2V)));});}else{var _2W=__app1(E(_u),_2Q),_2X=B(_2y(_J,_J,_2W,_)),_2Y=__get(_2Q,E(_t)),_2Z=__eq(_2Y,E(_2N));if(!E(_2Z)){var _30=B(_n(_2Y,_));return new T(function(){return new T3(0,E(_2X),E(new T1(1,_30)),E(_s));});}else{return new T(function(){return new T3(0,E(_2X),E(_2q),E(_s));});}}},_31=function(_32,_33,_){return new F(function(){return _2O(_32,E(_33),_);});},_34="mouseout",_35="mouseover",_36="mousemove",_37="mouseup",_38="mousedown",_39="dblclick",_3a="click",_3b="wheel",_3c=function(_3d){switch(E(_3d)){case 0:return E(_3a);case 1:return E(_39);case 2:return E(_38);case 3:return E(_37);case 4:return E(_36);case 5:return E(_35);case 6:return E(_34);default:return E(_3b);}},_3e=new T2(0,_3c,_31),_3f=0,_3g=function(_){return _3f;},_3h=function(_3i,_){return new T1(1,_3i);},_3j=function(_3k){return E(_3k);},_3l=new T2(0,_3j,_3h),_3m=function(_3n,_3o,_){var _3p=B(A1(_3n,_)),_3q=B(A1(_3o,_));return _3p;},_3r=function(_3s,_3t,_){var _3u=B(A1(_3s,_)),_3v=B(A1(_3t,_));return new T(function(){return B(A1(_3u,_3v));});},_3w=function(_3x,_3y,_){var _3z=B(A1(_3y,_));return _3x;},_3A=function(_3B,_3C,_){var _3D=B(A1(_3C,_));return new T(function(){return B(A1(_3B,_3D));});},_3E=new T2(0,_3A,_3w),_3F=function(_3G,_){return _3G;},_3H=function(_3I,_3J,_){var _3K=B(A1(_3I,_));return new F(function(){return A1(_3J,_);});},_3L=new T5(0,_3E,_3F,_3r,_3H,_3m),_3M=new T(function(){return E(_2n);}),_3N=function(_3O){return E(E(_3O).c);},_3P=function(_3Q){return new T6(0,_2q,_2r,_v,_3Q,_2q,_2q);},_3R=function(_3S,_){var _3T=new T(function(){return B(A2(_3N,_3M,new T(function(){return B(A1(_3P,_3S));})));});return new F(function(){return die(_3T);});},_3U=function(_3V,_){return new F(function(){return _3R(_3V,_);});},_3W=function(_3X){return new F(function(){return A1(_3U,_3X);});},_3Y=function(_3Z,_40,_){var _41=B(A1(_3Z,_));return new F(function(){return A2(_40,_41,_);});},_42=new T5(0,_3L,_3Y,_3H,_3F,_3W),_43=new T2(0,_42,_3j),_44=new T2(0,_43,_3F),_45=function(_46,_47,_48){return new F(function(){return A1(_46,new T2(1,_25,new T(function(){return B(A1(_47,_48));})));});},_49=0,_4a=new T(function(){return eval("(function(c,p){p.appendChild(c);})");}),_4b=function(_4c,_4d,_){var _4e=E(_4d);if(!_4e._){return _3f;}else{var _4f=E(_4c),_4g=E(_4a),_4h=__app2(_4g,E(_4e.a),_4f),_4i=function(_4j,_){while(1){var _4k=E(_4j);if(!_4k._){return _3f;}else{var _4l=__app2(_4g,E(_4k.a),_4f);_4j=_4k.b;continue;}}};return new F(function(){return _4i(_4e.b,_);});}},_4m=new T(function(){return eval("(function(t){return document.createElement(t);})");}),_4n=function(_){return new F(function(){return __app1(E(_4m),"button");});},_4o=new T(function(){return new T2(0,E(new T1(1,"height")),"100px");}),_4p=new T(function(){return new T2(0,E(new T1(1,"margin")),"auto");}),_4q=new T(function(){return new T2(0,E(new T1(1,"font-size")),"24px");}),_4r=new T(function(){return new T2(0,E(new T1(1,"font-weight")),"lighter");}),_4s=new T(function(){return new T2(0,E(new T1(1,"font-family")),"Palatino Linotyp, sans-serif");}),_4t=new T(function(){return B(unCStr("#8E121A"));}),_4u=new T(function(){return new T2(0,E(new T1(1,"background")),toJSStr(E(_4t)));}),_4v=new T2(1,_4u,_v),_4w=new T(function(){return B(unCStr("#EFE8E0"));}),_4x=new T(function(){return new T2(0,E(new T1(1,"color")),toJSStr(E(_4w)));}),_4y=new T2(1,_4x,_4v),_4z=new T2(1,_4s,_4y),_4A=new T2(1,_4r,_4z),_4B=new T2(1,_4q,_4A),_4C=new T2(1,_4p,_4B),_4D=new T2(1,_4o,_4C),_4E=new T(function(){return new T2(0,E(new T1(1,"width")),"350px");}),_4F=new T2(1,_4E,_4D),_4G=new T(function(){return B(unCStr("Oblicz"));}),_4H=new T(function(){return B(unCStr("Wyczy\u015b\u0107"));}),_4I=new T(function(){var _4J=jsShow(35);return fromJSStr(_4J);}),_4K=new T(function(){var _4L=jsShow(120);return fromJSStr(_4L);}),_4M=function(_4N,_4O){return new T2(0,E(_4N),toJSStr(E(_4O)));},_4P=new T(function(){return new T1(0,"value");}),_4Q=function(_4R){return E(E(_4R).a);},_4S=function(_4T){return E(E(_4T).b);},_4U=function(_4V){return new F(function(){return fromJSStr(E(_4V));});},_4W=function(_4X){return E(E(_4X).a);},_4Y=new T(function(){return eval("(function(e,p){var x = e[p];return typeof x === \'undefined\' ? \'\' : x.toString();})");}),_4Z=function(_50){return E(E(_50).b);},_51=function(_52,_53,_54,_55){var _56=new T(function(){var _57=function(_){var _58=__app2(E(_4Y),B(A2(_4W,_52,_54)),E(_55));return new T(function(){return String(_58);});};return E(_57);});return new F(function(){return A2(_4Z,_53,_56);});},_59=function(_5a){return E(E(_5a).d);},_5b=function(_5c,_5d,_5e,_5f){var _5g=B(_4Q(_5d)),_5h=new T(function(){return B(_59(_5g));}),_5i=function(_5j){return new F(function(){return A1(_5h,new T(function(){return B(_4U(_5j));}));});},_5k=new T(function(){return B(_51(_5c,_5d,_5e,new T(function(){return toJSStr(E(_5f));},1)));});return new F(function(){return A3(_4S,_5g,_5k,_5i);});},_5l=new T(function(){return B(unCStr("value"));}),_5m=new T(function(){return eval("(function(e,p,v){e.setAttribute(p, v);})");}),_5n=new T(function(){return eval("(function(e,p,v){e.style[p] = v;})");}),_5o=new T(function(){return eval("(function(e,p,v){e[p] = v;})");}),_5p=function(_5q,_5r,_5s,_5t){var _5u=new T(function(){return B(A2(_4W,_5q,_5s));}),_5v=function(_5w,_){var _5x=E(_5w);if(!_5x._){return _3f;}else{var _5y=E(_5u),_5z=E(_4a),_5A=__app2(_5z,E(_5x.a),_5y),_5B=function(_5C,_){while(1){var _5D=E(_5C);if(!_5D._){return _3f;}else{var _5E=__app2(_5z,E(_5D.a),_5y);_5C=_5D.b;continue;}}};return new F(function(){return _5B(_5x.b,_);});}},_5F=function(_5G,_){while(1){var _5H=B((function(_5I,_){var _5J=E(_5I);if(!_5J._){return _3f;}else{var _5K=_5J.b,_5L=E(_5J.a);if(!_5L._){var _5M=_5L.b,_5N=E(_5L.a);switch(_5N._){case 0:var _5O=E(_5u),_5P=E(_5o),_5Q=__app3(_5P,_5O,_5N.a,_5M),_5R=function(_5S,_){while(1){var _5T=E(_5S);if(!_5T._){return _3f;}else{var _5U=_5T.b,_5V=E(_5T.a);if(!_5V._){var _5W=_5V.b,_5X=E(_5V.a);switch(_5X._){case 0:var _5Y=__app3(_5P,_5O,_5X.a,_5W);_5S=_5U;continue;case 1:var _5Z=__app3(E(_5n),_5O,_5X.a,_5W);_5S=_5U;continue;default:var _60=__app3(E(_5m),_5O,_5X.a,_5W);_5S=_5U;continue;}}else{var _61=B(_5v(_5V.a,_));_5S=_5U;continue;}}}};return new F(function(){return _5R(_5K,_);});break;case 1:var _62=E(_5u),_63=E(_5n),_64=__app3(_63,_62,_5N.a,_5M),_65=function(_66,_){while(1){var _67=E(_66);if(!_67._){return _3f;}else{var _68=_67.b,_69=E(_67.a);if(!_69._){var _6a=_69.b,_6b=E(_69.a);switch(_6b._){case 0:var _6c=__app3(E(_5o),_62,_6b.a,_6a);_66=_68;continue;case 1:var _6d=__app3(_63,_62,_6b.a,_6a);_66=_68;continue;default:var _6e=__app3(E(_5m),_62,_6b.a,_6a);_66=_68;continue;}}else{var _6f=B(_5v(_69.a,_));_66=_68;continue;}}}};return new F(function(){return _65(_5K,_);});break;default:var _6g=E(_5u),_6h=E(_5m),_6i=__app3(_6h,_6g,_5N.a,_5M),_6j=function(_6k,_){while(1){var _6l=E(_6k);if(!_6l._){return _3f;}else{var _6m=_6l.b,_6n=E(_6l.a);if(!_6n._){var _6o=_6n.b,_6p=E(_6n.a);switch(_6p._){case 0:var _6q=__app3(E(_5o),_6g,_6p.a,_6o);_6k=_6m;continue;case 1:var _6r=__app3(E(_5n),_6g,_6p.a,_6o);_6k=_6m;continue;default:var _6s=__app3(_6h,_6g,_6p.a,_6o);_6k=_6m;continue;}}else{var _6t=B(_5v(_6n.a,_));_6k=_6m;continue;}}}};return new F(function(){return _6j(_5K,_);});}}else{var _6u=B(_5v(_5L.a,_));_5G=_5K;return __continue;}}})(_5G,_));if(_5H!=__continue){return _5H;}}};return new F(function(){return A2(_4Z,_5r,function(_){return new F(function(){return _5F(_5t,_);});});});},_6v=function(_6w,_6x,_){var _6y=B(A(_5b,[_3l,_43,_6w,_5l,_])),_6z=E(_6y);if(!_6z._){return new F(function(){return A(_5p,[_3l,_43,_6w,new T2(1,new T(function(){return B(_4M(_4P,_6x));}),_v),_]);});}else{return new F(function(){return A(_5p,[_3l,_43,_6w,new T2(1,new T(function(){return new T2(0,E(_4P),toJSStr(new T2(1,_6z.a,_6z.b)));}),_v),_]);});}},_6A=new T(function(){var _6B=jsShow(0.15);return fromJSStr(_6B);}),_6C=new T(function(){var _6D=jsShow(1.2);return fromJSStr(_6D);}),_6E=new T(function(){var _6F=jsShow(20);return fromJSStr(_6F);}),_6G=new T(function(){var _6H=jsShow(2.5);return fromJSStr(_6H);}),_6I=new T(function(){var _6J=jsShow(27);return fromJSStr(_6J);}),_6K=new T(function(){var _6L=jsShow(60);return fromJSStr(_6L);}),_6M=new T(function(){var _6N=jsShow(2);return fromJSStr(_6N);}),_6O=new T(function(){var _6P=jsShow(1);return fromJSStr(_6P);}),_6Q=function(_6R,_6S,_6T,_6U,_6V,_6W,_6X,_6Y,_6Z,_70,_71,_72,_){var _73=B(_6v(_6R,_4K,_)),_74=B(_6v(_6S,_4I,_)),_75=B(_6v(_6T,_6O,_)),_76=B(_6v(_6U,_6M,_)),_77=B(_6v(_6V,_6O,_)),_78=B(_6v(_6W,_6K,_)),_79=B(_6v(_6X,_6I,_)),_7a=B(_6v(_6Y,_6G,_)),_7b=B(_6v(_6Z,_4I,_)),_7c=B(_6v(_70,_6E,_)),_7d=B(_6v(_71,_6C,_));return new F(function(){return _6v(_72,_6A,_);});},_7e=new T(function(){return B(unCStr("Uzupe\u0142nij warto\u015bci"));}),_7f=new T2(1,_4p,_v),_7g=new T(function(){return B(unCStr("height"));}),_7h=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"100px");}),_7i=new T2(1,_7h,_7f),_7j=new T(function(){return B(unCStr("width"));}),_7k=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"1050px");}),_7l=new T2(1,_7k,_7i),_7m=function(_){return new F(function(){return __app1(E(_4m),"tr");});},_7n=function(_7o){return E(E(_7o).c);},_7p=function(_7q,_7r,_7s,_7t){var _7u=B(_4Q(_7r)),_7v=function(_7w){return new F(function(){return A3(_7n,_7u,new T(function(){return B(_5p(_7q,_7r,_7w,_7t));}),new T(function(){return B(A2(_59,_7u,_7w));}));});};return new F(function(){return A3(_4S,_7u,_7s,_7v);});},_7x=new T(function(){return B(_7p(_3l,_43,_7m,_7l));}),_7y=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"350px");}),_7z=new T2(1,_7y,_7i),_7A=function(_){return new F(function(){return __app1(E(_4m),"td");});},_7B=new T(function(){return B(_7p(_3l,_43,_7A,_7z));}),_7C=new T(function(){return new T2(0,E(new T1(0,"value")),toJSStr(_v));}),_7D=new T2(1,_7C,_v),_7E=function(_7F){return E(E(_7F).a);},_7G=function(_7H){return E(E(_7H).b);},_7I=function(_7J){return E(E(_7J).a);},_7K=function(_){return new F(function(){return nMV(_2q);});},_7L=new T(function(){return B(_2J(_7K));}),_7M=new T(function(){return eval("(function(e,name,f){e.addEventListener(name,f,false);return [f];})");}),_7N=function(_7O){return E(E(_7O).b);},_7P=function(_7Q,_7R,_7S,_7T,_7U,_7V){var _7W=B(_7E(_7Q)),_7X=B(_4Q(_7W)),_7Y=new T(function(){return B(_4Z(_7W));}),_7Z=new T(function(){return B(_59(_7X));}),_80=new T(function(){return B(A2(_4W,_7R,_7T));}),_81=new T(function(){return B(A2(_7I,_7S,_7U));}),_82=function(_83){return new F(function(){return A1(_7Z,new T3(0,_81,_80,_83));});},_84=function(_85){var _86=new T(function(){var _87=new T(function(){var _88=__createJSFunc(2,function(_89,_){var _8a=B(A2(E(_85),_89,_));return _2N;}),_8b=_88;return function(_){return new F(function(){return __app3(E(_7M),E(_80),E(_81),_8b);});};});return B(A1(_7Y,_87));});return new F(function(){return A3(_4S,_7X,_86,_82);});},_8c=new T(function(){var _8d=new T(function(){return B(_4Z(_7W));}),_8e=function(_8f){var _8g=new T(function(){return B(A1(_8d,function(_){var _=wMV(E(_7L),new T1(1,_8f));return new F(function(){return A(_7G,[_7S,_7U,_8f,_]);});}));});return new F(function(){return A3(_4S,_7X,_8g,_7V);});};return B(A2(_7N,_7Q,_8e));});return new F(function(){return A3(_4S,_7X,_8c,_84);});},_8h=function(_){return new F(function(){return __app1(E(_4m),"table");});},_8i=new T(function(){return eval("(function(s){return document.createTextNode(s);})");}),_8j=function(_8k,_8l,_){var _8m=__app1(E(_8i),toJSStr(E(_8l))),_8n=__app2(E(_4a),_8m,E(_8k));return new F(function(){return _3g(_);});},_8o=function(_8p,_8q,_8r,_8s,_8t,_8u,_8v,_8w,_8x,_8y,_8z,_8A,_){var _8B=B(_8h(_)),_8C=B(A1(_7x,_)),_8D=B(A1(_7B,_)),_8E=B(A(_7p,[_3l,_43,_4n,_4F,_])),_8F=B(_8j(_8E,_4G,_)),_8G=E(_8E),_8H=E(_8D),_8I=E(_4a),_8J=__app2(_8I,_8G,_8H),_8K=B(A1(_7B,_)),_8L=B(A(_7p,[_3l,_43,_4n,_4F,_])),_8M=B(_8j(_8L,_4H,_)),_8N=E(_8L),_8O=E(_8K),_8P=__app2(_8I,_8N,_8O),_8Q=B(A1(_7B,_)),_8R=B(A(_7p,[_3l,_43,_4n,_4F,_])),_8S=B(_8j(_8R,_7e,_)),_8T=E(_8R),_8U=E(_8Q),_8V=__app2(_8I,_8T,_8U),_8W=B(A(_7P,[_44,_3l,_3e,_8N,_49,function(_8X,_){var _8Y=B(A(_5p,[_3l,_43,_8p,_7D,_])),_8Z=B(A(_5p,[_3l,_43,_8q,_7D,_])),_90=B(A(_5p,[_3l,_43,_8r,_7D,_])),_91=B(A(_5p,[_3l,_43,_8s,_7D,_])),_92=B(A(_5p,[_3l,_43,_8t,_7D,_])),_93=B(A(_5p,[_3l,_43,_8u,_7D,_])),_94=B(A(_5p,[_3l,_43,_8v,_7D,_])),_95=B(A(_5p,[_3l,_43,_8w,_7D,_])),_96=B(A(_5p,[_3l,_43,_8x,_7D,_])),_97=B(A(_5p,[_3l,_43,_8y,_7D,_])),_98=B(A(_5p,[_3l,_43,_8z,_7D,_]));return new F(function(){return A(_5p,[_3l,_43,_8A,_7D,_]);});},_])),_99=B(A(_7P,[_44,_3l,_3e,_8T,_49,function(_9a,_){return new F(function(){return _6Q(_8p,_8q,_8r,_8s,_8t,_8u,_8v,_8w,_8x,_8y,_8z,_8A,_);});},_])),_9b=B(A(_7P,[_44,_3l,_3e,_8G,_49,function(_9c,_){return new F(function(){return _6Q(_8p,_8q,_8r,_8s,_8t,_8u,_8v,_8w,_8x,_8y,_8z,_8A,_);});},_])),_9d=B(_4b(_8C,new T2(1,_8H,new T2(1,_8O,new T2(1,_8U,_v))),_)),_9e=E(_8B),_9f=__app2(_8I,E(_8C),_9e);return new T2(0,_9e,_8G);},_9g=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"1500px");}),_9h=new T(function(){return new T2(0,E(new T1(1,"padding-left")),"20px");}),_9i=new T(function(){return new T2(0,E(new T1(1,"padding-right")),"20px");}),_9j=new T(function(){return B(unCStr("#143057"));}),_9k=new T(function(){return new T2(0,E(new T1(1,"color")),toJSStr(E(_9j)));}),_9l=new T(function(){return new T2(0,E(new T1(1,"font-size")),"28px");}),_9m=new T2(1,_9l,_v),_9n=new T(function(){return new T2(0,E(new T1(1,"text-align")),"center");}),_9o=new T2(1,_9n,_9m),_9p=new T2(1,_9k,_9o),_9q=new T2(1,_4p,_9p),_9r=new T2(1,_9i,_9q),_9s=new T2(1,_9h,_9r),_9t=new T2(1,_7h,_9s),_9u=new T2(1,_9g,_9t),_9v=new T(function(){return B(_7p(_3l,_43,_7A,_9u));}),_9w=function(_){var _9x=B(_8h(_)),_9y=B(_7m(_)),_9z=B(_7m(_)),_9A=B(A1(_9v,_)),_9B=B(A1(_9v,_)),_9C=B(_4b(_9y,new T2(1,_9A,_v),_)),_9D=B(_4b(_9z,new T2(1,_9B,_v),_)),_9E=B(_4b(_9x,new T2(1,_9y,new T2(1,_9z,_v)),_));return new T3(0,_9x,_9A,_9B);},_9F=function(_){return new F(function(){return __app1(E(_4m),"div");});},_9G=new T(function(){return eval("document.body");}),_9H=function(_){var _9I=E(_9G),_9J=E(_5n),_9K=__app3(_9J,_9I,"background-image","url(\"static/wallpapers/wallpaper3.jpg\")"),_9L=__app3(_9J,_9I,"margin","auto"),_9M=__app3(_9J,_9I,toJSStr(E(_7j)),"100%");return new F(function(){return _3g(_);});},_9N=new T(function(){return B(unCStr(": empty list"));}),_9O=new T(function(){return B(unCStr("Prelude."));}),_9P=function(_9Q){return new F(function(){return err(B(_3(_9O,new T(function(){return B(_3(_9Q,_9N));},1))));});},_9R=new T(function(){return B(unCStr("head"));}),_9S=new T(function(){return B(_9P(_9R));}),_9T=new T2(1,_4s,_v),_9U=new T2(1,_4r,_9T),_9V=new T(function(){return new T2(0,E(new T1(1,"margin-bottom")),"80px");}),_9W=new T2(1,_9V,_9U),_9X=new T2(1,_4p,_9W),_9Y=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"1000px");}),_9Z=new T2(1,_9Y,_9X),_a0=new T2(1,_9V,_v),_a1=new T2(1,_4p,_a0),_a2=new T2(1,_7h,_a1),_a3=new T2(1,_7k,_a2),_a4=new T(function(){return new T2(0,E(new T1(1,"margin-bottom")),"100px");}),_a5=new T2(1,_a4,_v),_a6=new T(function(){return new T2(0,E(new T1(1,"margin-top")),"80px");}),_a7=new T2(1,_a6,_a5),_a8=new T2(1,_4p,_a7),_a9=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"160px");}),_aa=new T2(1,_a9,_a8),_ab=new T2(1,_9g,_aa),_ac=new T(function(){return eval("(function(e){while(e.firstChild){e.removeChild(e.firstChild);}})");}),_ad=function(_){return new F(function(){return __app1(E(_4m),"input");});},_ae=new T(function(){return B(unCStr("G\u0119sto\u015b\u0107 powietrza [kg/m3]"));}),_af=new T(function(){return B(unCStr("Wsp\u00f3\u0142czynnik oporu"));}),_ag=new T(function(){return B(unCStr("Wsp\u00f3\u0142czynnik si\u0142y no\u015bnej"));}),_ah=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"200px");}),_ai=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"50px");}),_aj=new T(function(){return new T2(0,E(new T1(1,"padding-right")),"10px");}),_ak=new T(function(){return B(unCStr("background"));}),_al=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_ak)))),toJSStr(E(_9j)));}),_am=new T2(1,_al,_v),_an=new T2(1,_4x,_am),_ao=new T2(1,_4s,_an),_ap=new T2(1,_4r,_ao),_aq=new T2(1,_4q,_ap),_ar=new T2(1,_aj,_aq),_as=new T2(1,_ai,_ar),_at=new T2(1,_ah,_as),_au=new T(function(){return new T2(0,E(new T1(1,"float")),"right");}),_av=new T2(1,_au,_at),_aw=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"520px");}),_ax=new T2(1,_aw,_7i),_ay=new T(function(){return new T2(0,E(new T1(1,"text-align")),"left");}),_az=new T2(1,_ay,_9m),_aA=new T2(1,_9k,_az),_aB=new T2(1,_4p,_aA),_aC=new T2(1,_9i,_aB),_aD=new T2(1,_9h,_aC),_aE=new T2(1,_7h,_aD),_aF=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"430px");}),_aG=new T2(1,_aF,_aE),_aH=new T(function(){return B(_7p(_3l,_43,_7A,_aG));}),_aI=function(_){var _aJ=B(_8h(_)),_aK=B(A(_7p,[_3l,_43,_7m,_ax,_])),_aL=B(A1(_aH,_)),_aM=B(_8j(_aL,_ae,_)),_aN=B(A1(_aH,_)),_aO=B(A(_7p,[_3l,_43,_ad,_av,_])),_aP=E(_aO),_aQ=E(_aN),_aR=E(_4a),_aS=__app2(_aR,_aP,_aQ),_aT=B(_4b(_aK,new T2(1,_aL,new T2(1,_aQ,_v)),_)),_aU=B(A(_7p,[_3l,_43,_7m,_ax,_])),_aV=B(A1(_aH,_)),_aW=B(_8j(_aV,_af,_)),_aX=B(A1(_aH,_)),_aY=B(A(_7p,[_3l,_43,_ad,_av,_])),_aZ=E(_aY),_b0=E(_aX),_b1=__app2(_aR,_aZ,_b0),_b2=B(_4b(_aU,new T2(1,_aV,new T2(1,_b0,_v)),_)),_b3=B(A(_7p,[_3l,_43,_7m,_ax,_])),_b4=B(A1(_aH,_)),_b5=B(_8j(_b4,_ag,_)),_b6=B(A1(_aH,_)),_b7=B(A(_7p,[_3l,_43,_ad,_av,_])),_b8=E(_b7),_b9=E(_b6),_ba=__app2(_aR,_b8,_b9),_bb=B(_4b(_b3,new T2(1,_b4,new T2(1,_b9,_v)),_)),_bc=B(_4b(_aJ,new T2(1,_aK,new T2(1,_aU,new T2(1,_b3,_v))),_));return new T4(0,_aJ,_aP,_aZ,_b8);},_bd=function(_){return new F(function(){return __app1(E(_4m),"input");});},_be=new T(function(){return B(unCStr("Powierzchnia przekroju poprzecznego skoczka [m2]"));}),_bf=new T(function(){return B(unCStr("K\u0105t skierowania skoczka [stopnie]"));}),_bg=new T(function(){return B(unCStr("Powierzchnia przekroju pod\u0142u\u017cnego skoczka [m2]"));}),_bh=new T(function(){return B(unCStr("Masa skoczka [kg]"));}),_bi=new T(function(){return B(unCStr("K\u0105t natarcia [stopnie]"));}),_bj=new T(function(){return B(unCStr("Pr\u0119dko\u015b\u0107 na progu w poziomie [m/s]"));}),_bk=new T(function(){return B(unCStr("Pr\u0119dko\u015b\u0107 na progu w pionie [m/s]"));}),_bl=function(_){var _bm=B(_8h(_)),_bn=B(A(_7p,[_3l,_43,_7m,_ax,_])),_bo=B(A1(_aH,_)),_bp=B(_8j(_bo,_bh,_)),_bq=B(A1(_aH,_)),_br=B(A(_7p,[_3l,_43,_bd,_av,_])),_bs=E(_br),_bt=E(_bq),_bu=E(_4a),_bv=__app2(_bu,_bs,_bt),_bw=B(_4b(_bn,new T2(1,_bo,new T2(1,_bt,_v)),_)),_bx=B(A(_7p,[_3l,_43,_7m,_ax,_])),_by=B(A1(_aH,_)),_bz=B(_8j(_by,_bj,_)),_bA=B(A1(_aH,_)),_bB=B(A(_7p,[_3l,_43,_bd,_av,_])),_bC=E(_bB),_bD=E(_bA),_bE=__app2(_bu,_bC,_bD),_bF=B(_4b(_bx,new T2(1,_by,new T2(1,_bD,_v)),_)),_bG=B(A(_7p,[_3l,_43,_7m,_ax,_])),_bH=B(A1(_aH,_)),_bI=B(_8j(_bH,_bk,_)),_bJ=B(A1(_aH,_)),_bK=B(A(_7p,[_3l,_43,_bd,_av,_])),_bL=E(_bK),_bM=E(_bJ),_bN=__app2(_bu,_bL,_bM),_bO=B(_4b(_bG,new T2(1,_bH,new T2(1,_bM,_v)),_)),_bP=B(A(_7p,[_3l,_43,_7m,_ax,_])),_bQ=B(A1(_aH,_)),_bR=B(_8j(_bQ,_bf,_)),_bS=B(A1(_aH,_)),_bT=B(A(_7p,[_3l,_43,_bd,_av,_])),_bU=E(_bT),_bV=E(_bS),_bW=__app2(_bu,_bU,_bV),_bX=B(_4b(_bP,new T2(1,_bQ,new T2(1,_bV,_v)),_)),_bY=B(A(_7p,[_3l,_43,_7m,_ax,_])),_bZ=B(A1(_aH,_)),_c0=B(_8j(_bZ,_bi,_)),_c1=B(A1(_aH,_)),_c2=B(A(_7p,[_3l,_43,_bd,_av,_])),_c3=E(_c2),_c4=E(_c1),_c5=__app2(_bu,_c3,_c4),_c6=B(_4b(_bY,new T2(1,_bZ,new T2(1,_c4,_v)),_)),_c7=B(A(_7p,[_3l,_43,_7m,_ax,_])),_c8=B(A1(_aH,_)),_c9=B(_8j(_c8,_bg,_)),_ca=B(A1(_aH,_)),_cb=B(A(_7p,[_3l,_43,_bd,_av,_])),_cc=E(_cb),_cd=E(_ca),_ce=__app2(_bu,_cc,_cd),_cf=B(_4b(_c7,new T2(1,_c8,new T2(1,_cd,_v)),_)),_cg=B(A(_7p,[_3l,_43,_7m,_ax,_])),_ch=B(A1(_aH,_)),_ci=B(_8j(_ch,_be,_)),_cj=B(A1(_aH,_)),_ck=B(A(_7p,[_3l,_43,_bd,_av,_])),_cl=E(_ck),_cm=E(_cj),_cn=__app2(_bu,_cl,_cm),_co=B(_4b(_cg,new T2(1,_ch,new T2(1,_cm,_v)),_)),_cp=B(_4b(_bm,new T2(1,_bn,new T2(1,_bx,new T2(1,_bG,new T2(1,_bP,new T2(1,_bY,new T2(1,_c7,new T2(1,_cg,_v))))))),_));return {_:0,a:_bm,b:_bs,c:_bC,d:_bL,e:_bU,f:_c3,g:_cc,h:_cl};},_cq=new T(function(){return B(unCStr("static/bischofshofen.jpg"));}),_cr=new T(function(){return B(unCStr("static/engelberg.jpg"));}),_cs=new T(function(){return new T2(0,E(new T1(1,"padding")),"0px");}),_ct=new T2(1,_cs,_v),_cu=new T(function(){return B(unCStr("margin"));}),_cv=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_cu)))),"auto");}),_cw=new T2(1,_cv,_ct),_cx=new T2(1,_a9,_cw),_cy=new T2(1,_ah,_cx),_cz=function(_cA,_cB){while(1){var _cC=E(_cA);if(!_cC._){return (E(_cB)._==0)?true:false;}else{var _cD=E(_cB);if(!_cD._){return false;}else{if(E(_cC.a)!=E(_cD.a)){return false;}else{_cA=_cC.b;_cB=_cD.b;continue;}}}}},_cE=new T(function(){return B(unCStr("base"));}),_cF=new T(function(){return B(unCStr("Control.Exception.Base"));}),_cG=new T(function(){return B(unCStr("PatternMatchFail"));}),_cH=new T5(0,new Long(18445595,3739165398,true),new Long(52003073,3246954884,true),_cE,_cF,_cG),_cI=new T5(0,new Long(18445595,3739165398,true),new Long(52003073,3246954884,true),_cH,_v,_v),_cJ=function(_cK){return E(_cI);},_cL=function(_cM){var _cN=E(_cM);return new F(function(){return _X(B(_V(_cN.a)),_cJ,_cN.b);});},_cO=function(_cP){return E(E(_cP).a);},_cQ=function(_cR){return new T2(0,_cS,_cR);},_cT=function(_cU,_cV){return new F(function(){return _3(E(_cU).a,_cV);});},_cW=function(_cX,_cY){return new F(function(){return _28(_cT,_cX,_cY);});},_cZ=function(_d0,_d1,_d2){return new F(function(){return _3(E(_d1).a,_d2);});},_d3=new T3(0,_cZ,_cO,_cW),_cS=new T(function(){return new T5(0,_cJ,_d3,_cQ,_cL,_cO);}),_d4=new T(function(){return B(unCStr("Non-exhaustive patterns in"));}),_d5=function(_d6,_d7){return new F(function(){return die(new T(function(){return B(A2(_3N,_d7,_d6));}));});},_d8=function(_d9,_da){return new F(function(){return _d5(_d9,_da);});},_db=function(_dc,_dd){var _de=E(_dd);if(!_de._){return new T2(0,_v,_v);}else{var _df=_de.a;if(!B(A1(_dc,_df))){return new T2(0,_v,_de);}else{var _dg=new T(function(){var _dh=B(_db(_dc,_de.b));return new T2(0,_dh.a,_dh.b);});return new T2(0,new T2(1,_df,new T(function(){return E(E(_dg).a);})),new T(function(){return E(E(_dg).b);}));}}},_di=32,_dj=new T(function(){return B(unCStr("\n"));}),_dk=function(_dl){return (E(_dl)==124)?false:true;},_dm=function(_dn,_do){var _dp=B(_db(_dk,B(unCStr(_dn)))),_dq=_dp.a,_dr=function(_ds,_dt){var _du=new T(function(){var _dv=new T(function(){return B(_3(_do,new T(function(){return B(_3(_dt,_dj));},1)));});return B(unAppCStr(": ",_dv));},1);return new F(function(){return _3(_ds,_du);});},_dw=E(_dp.b);if(!_dw._){return new F(function(){return _dr(_dq,_v);});}else{if(E(_dw.a)==124){return new F(function(){return _dr(_dq,new T2(1,_di,_dw.b));});}else{return new F(function(){return _dr(_dq,_v);});}}},_dx=function(_dy){return new F(function(){return _d8(new T1(0,new T(function(){return B(_dm(_dy,_d4));})),_cS);});},_dz=new T(function(){return B(_dx("Client/Events/HillClickedEvents.hs:(10,1)-(40,59)|function hillClicked"));}),_dA=new T(function(){var _dB=jsShow(134);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dB)));}),_dC=new T2(1,_dA,_v),_dD=new T(function(){var _dE=jsShow(33.2);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dE)));}),_dF=new T2(1,_dD,_v),_dG=new T(function(){return B(unCStr("oslo"));}),_dH=new T(function(){var _dI=jsShow(142);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dI)));}),_dJ=new T2(1,_dH,_v),_dK=new T(function(){var _dL=jsShow(36.9);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dL)));}),_dM=new T2(1,_dK,_v),_dN=new T(function(){return B(unCStr("kuusamo"));}),_dO=new T(function(){var _dP=jsShow(130);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dP)));}),_dQ=new T2(1,_dO,_v),_dR=new T(function(){var _dS=jsShow(34.5);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dS)));}),_dT=new T2(1,_dR,_v),_dU=new T(function(){return B(unCStr("innsbruck"));}),_dV=new T(function(){var _dW=jsShow(34);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_dW)));}),_dX=new T2(1,_dV,_v),_dY=new T(function(){return B(unCStr("engelberg"));}),_dZ=new T(function(){var _e0=jsShow(35.5);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_e0)));}),_e1=new T2(1,_dZ,_v),_e2=new T(function(){var _e3=jsShow(140);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_e3)));}),_e4=new T2(1,_e2,_v),_e5=new T(function(){var _e6=jsShow(35);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_e6)));}),_e7=new T2(1,_e5,_v),_e8=new T(function(){return B(unCStr("bischofshofen"));}),_e9=new T(function(){return B(unCStr("zakopane"));}),_ea=new T(function(){var _eb=jsShow(98);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_eb)));}),_ec=new T2(1,_ea,_v),_ed=new T(function(){var _ee=jsShow(36);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_ee)));}),_ef=new T2(1,_ed,_v),_eg=new T(function(){return B(unCStr("ramsau"));}),_eh=new T(function(){var _ei=jsShow(225);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_ei)));}),_ej=new T2(1,_eh,_v),_ek=new T(function(){var _el=jsShow(33);return new T2(0,E(new T1(0,"value")),toJSStr(fromJSStr(_el)));}),_em=new T2(1,_ek,_v),_en=new T(function(){return B(unCStr("planica"));}),_eo=function(_ep,_eq,_er,_){if(!B(_cz(_ep,_e8))){if(!B(_cz(_ep,_dY))){if(!B(_cz(_ep,_dU))){if(!B(_cz(_ep,_dN))){if(!B(_cz(_ep,_dG))){if(!B(_cz(_ep,_en))){if(!B(_cz(_ep,_eg))){if(!B(_cz(_ep,_e9))){return E(_dz);}else{var _es=B(A(_5p,[_3l,_43,_eq,_dC,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_e1,_]);});}}else{var _et=B(A(_5p,[_3l,_43,_eq,_ec,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_ef,_]);});}}else{var _eu=B(A(_5p,[_3l,_43,_eq,_ej,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_em,_]);});}}else{var _ev=B(A(_5p,[_3l,_43,_eq,_dC,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_dF,_]);});}}else{var _ew=B(A(_5p,[_3l,_43,_eq,_dJ,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_dM,_]);});}}else{var _ex=B(A(_5p,[_3l,_43,_eq,_dQ,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_dT,_]);});}}else{var _ey=B(A(_5p,[_3l,_43,_eq,_dJ,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_dX,_]);});}}else{var _ez=B(A(_5p,[_3l,_43,_eq,_e4,_]));return new F(function(){return A(_5p,[_3l,_43,_er,_e7,_]);});}},_eA=new T2(1,_a9,_v),_eB=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"230px");}),_eC=new T2(1,_eB,_eA),_eD=new T2(1,_9l,_9U),_eE=new T2(1,_9n,_eD),_eF=new T2(1,_9k,_eE),_eG=new T2(1,_9i,_eF),_eH=new T2(1,_a9,_eG),_eI=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"280px");}),_eJ=new T2(1,_eI,_eH),_eK=new T(function(){return B(_7p(_3l,_43,_7A,_eJ));}),_eL=new T2(1,_cv,_v),_eM=new T2(1,_a9,_eL),_eN=new T2(1,_ah,_eM),_eO=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"180px");}),_eP=new T2(1,_eO,_v),_eQ=new T2(1,_9g,_eP),_eR=new T(function(){return B(unCStr("HS [m]"));}),_eS=new T(function(){return B(unCStr("src"));}),_eT=function(_){return new F(function(){return __app1(E(_4m),"img");});},_eU=function(_eV,_eW,_){var _eX=B(A(_7p,[_3l,_43,_eT,_eW,_])),_eY=E(_eX),_eZ=__app3(E(_5o),_eY,toJSStr(E(_eS)),toJSStr(E(_eV)));return _eY;},_f0=function(_f1,_f2,_f3,_){var _f4=B(A(_7p,[_3l,_43,_4n,_f2,_])),_f5=B(_eU(_f1,_f3,_)),_f6=E(_f4),_f7=__app2(E(_4a),E(_f5),_f6);return _f6;},_f8=new T(function(){return B(unCStr("static/innsbruck.jpg"));}),_f9=function(_){return new F(function(){return __app1(E(_4m),"input");});},_fa=new T(function(){return B(unCStr("static/kuusamo.jpg"));}),_fb=new T(function(){return B(unCStr("zakopane"));}),_fc=new T(function(){return B(unCStr("engelberg"));}),_fd=new T(function(){return B(unCStr("ramsau"));}),_fe=new T(function(){return B(unCStr("kuusamo"));}),_ff=new T(function(){return B(unCStr("innsbruck"));}),_fg=new T(function(){return B(unCStr("bischofshofen"));}),_fh=new T(function(){return B(unCStr("oslo"));}),_fi=new T(function(){return B(unCStr("planica"));}),_fj=new T(function(){return B(unCStr("static/oslo.jpg"));}),_fk=new T(function(){return B(unCStr("static/planica.jpg"));}),_fl=new T(function(){return B(unCStr("static/ramsau.jpg"));}),_fm=new T(function(){return B(unCStr("K\u0105t nachylenia stoku [stopnie]"));}),_fn=new T(function(){return B(unCStr("static/zakopane.jpg"));}),_fo=function(_){var _fp=B(_8h(_)),_fq=B(A(_7p,[_3l,_43,_7m,_eQ,_])),_fr=B(A(_7p,[_3l,_43,_7m,_eQ,_])),_fs=B(A(_7p,[_3l,_43,_7A,_eC,_])),_ft=B(_f0(_fn,_cy,_eN,_)),_fu=E(_ft),_fv=E(_fs),_fw=E(_4a),_fx=__app2(_fw,_fu,_fv),_fy=B(A(_7p,[_3l,_43,_7A,_eC,_])),_fz=B(_f0(_cr,_cy,_eN,_)),_fA=E(_fz),_fB=E(_fy),_fC=__app2(_fw,_fA,_fB),_fD=B(A(_7p,[_3l,_43,_7A,_eC,_])),_fE=B(_f0(_fl,_cy,_eN,_)),_fF=E(_fE),_fG=E(_fD),_fH=__app2(_fw,_fF,_fG),_fI=B(A(_7p,[_3l,_43,_7A,_eC,_])),_fJ=B(_f0(_fa,_cy,_eN,_)),_fK=E(_fJ),_fL=E(_fI),_fM=__app2(_fw,_fK,_fL),_fN=B(A1(_eK,_)),_fO=B(_8j(_fN,_eR,_)),_fP=B(A1(_eK,_)),_fQ=B(A(_7p,[_3l,_43,_f9,_av,_])),_fR=E(_fQ),_fS=E(_fP),_fT=__app2(_fw,_fR,_fS),_fU=B(_4b(_fq,new T2(1,_fv,new T2(1,_fB,new T2(1,_fG,new T2(1,_fL,new T2(1,_fN,new T2(1,_fS,_v)))))),_)),_fV=B(A(_7p,[_3l,_43,_7A,_eC,_])),_fW=B(_f0(_f8,_cy,_eN,_)),_fX=E(_fW),_fY=E(_fV),_fZ=__app2(_fw,_fX,_fY),_g0=B(A(_7p,[_3l,_43,_7A,_eC,_])),_g1=B(_f0(_cq,_cy,_eN,_)),_g2=E(_g1),_g3=E(_g0),_g4=__app2(_fw,_g2,_g3),_g5=B(A(_7p,[_3l,_43,_7A,_eC,_])),_g6=B(_f0(_fj,_cy,_eN,_)),_g7=E(_g6),_g8=E(_g5),_g9=__app2(_fw,_g7,_g8),_ga=B(A(_7p,[_3l,_43,_7A,_eC,_])),_gb=B(_f0(_fk,_cy,_eN,_)),_gc=E(_gb),_gd=E(_ga),_ge=__app2(_fw,_gc,_gd),_gf=B(A1(_eK,_)),_gg=B(_8j(_gf,_fm,_)),_gh=B(A1(_eK,_)),_gi=B(A(_7p,[_3l,_43,_f9,_av,_])),_gj=E(_gi),_gk=E(_gh),_gl=__app2(_fw,_gj,_gk),_gm=B(_4b(_fr,new T2(1,_fY,new T2(1,_g3,new T2(1,_g8,new T2(1,_gd,new T2(1,_gf,new T2(1,_gk,_v)))))),_)),_gn=B(_4b(_fp,new T2(1,_fq,new T2(1,_fr,_v)),_)),_go=B(A(_7P,[_44,_3l,_3e,_fu,_49,function(_gp,_){return new F(function(){return _eo(_fb,_fR,_gj,_);});},_])),_gq=B(A(_7P,[_44,_3l,_3e,_fA,_49,function(_gr,_){return new F(function(){return _eo(_fc,_fR,_gj,_);});},_])),_gs=B(A(_7P,[_44,_3l,_3e,_fF,_49,function(_gt,_){return new F(function(){return _eo(_fd,_fR,_gj,_);});},_])),_gu=B(A(_7P,[_44,_3l,_3e,_fK,_49,function(_gv,_){return new F(function(){return _eo(_fe,_fR,_gj,_);});},_])),_gw=B(A(_7P,[_44,_3l,_3e,_fX,_49,function(_gx,_){return new F(function(){return _eo(_ff,_fR,_gj,_);});},_])),_gy=B(A(_7P,[_44,_3l,_3e,_g2,_49,function(_gz,_){return new F(function(){return _eo(_fg,_fR,_gj,_);});},_])),_gA=B(A(_7P,[_44,_3l,_3e,_g7,_49,function(_gB,_){return new F(function(){return _eo(_fh,_fR,_gj,_);});},_])),_gC=B(A(_7P,[_44,_3l,_3e,_gc,_49,function(_gD,_){return new F(function(){return _eo(_fi,_fR,_gj,_);});},_]));return new T3(0,_fp,_fR,_gj);},_gE=function(_gF,_){var _gG=E(_gF);if(!_gG._){return _v;}else{var _gH=B(A1(_gG.a,_)),_gI=B(_gE(_gG.b,_));return new T2(1,_gH,_gI);}},_gJ=function(_gK,_){var _gL=B(_9F(_)),_gM=E(_gL),_gN=__app2(E(_4a),E(_gK),_gM);return _gM;},_gO=function(_gP){var _gQ=E(_gP);return (_gQ._==0)?__Z:new T2(1,function(_){return new F(function(){return _gJ(_gQ.a,_);});},new T(function(){return B(_gO(_gQ.b));}));},_gR=function(_gS){var _gT=E(_gS);if(!_gT._){return new T2(0,_v,_v);}else{var _gU=E(_gT.a),_gV=new T(function(){var _gW=B(_gR(_gT.b));return new T2(0,_gW.a,_gW.b);});return new T2(0,new T2(1,_gU.a,new T(function(){return E(E(_gV).a);})),new T2(1,_gU.b,new T(function(){return E(E(_gV).b);})));}},_gX=function(_gY,_gZ,_h0,_h1,_h2,_h3,_h4,_h5,_h6,_h7,_h8){while(1){var _h9=B((function(_ha,_hb,_hc,_hd,_he,_hf,_hg,_hh,_hi,_hj,_hk){if(_hd<=1){return new F(function(){return _3(_ha,new T2(1,new T2(0,_hd,_hb),_v));});}else{var _hl=Math.sin(_hf*3.141592653589793/180),_hm=1.0e-2*_he,_hn=(_hj*_hh*_hl*_hl*Math.cos(_hf*3.141592653589793/180)*Math.cos(_hg*3.141592653589793/180)+_hk*(_hi+_hh*_hl*_hl*_hl)*Math.sin(_hg*3.141592653589793/180))/_hc,_ho=1.0e-2*(-9.80665+_hn*_he*_he),_hp=1.0e-2*(-9.80665+_hn*_hm*_hm+0.5*_ho),_hq=_he+0.5*_ho,_hr=1.0e-2*(-9.80665+_hn*_hq*_hq),_hs=_he+0.5*_hr,_ht=1.0e-2*(-9.80665+_hn*_hs*_hs),_hu=_he+_ht,_hv=B(_3(_ha,new T2(1,new T2(0,_hd,_hb),_v))),_hw=_hc,_hx=_hd+0.16666666666666666*(_hm+_hp+_hp+1.0e-2*(-9.80665+_hn*_hm*_hm+0.5*_hp)+1.0e-2*(-9.80665+_hn*_hm*_hm+0.5*_hp)+1.0e-2*(_he+_ht)),_hy=_he+0.16666666666666666*(_ho+_hr+_hr+_ht+_ht+1.0e-2*(-9.80665+_hn*_hu*_hu)),_hz=_hf,_hA=_hg,_hB=_hh,_hC=_hi,_hD=_hj,_hE=_hk;_gY=_hv;_gZ=new T(function(){return E(_hb)+1.0e-2;});_h0=_hw;_h1=_hx;_h2=_hy;_h3=_hz;_h4=_hA;_h5=_hB;_h6=_hC;_h7=_hD;_h8=_hE;return __continue;}})(_gY,_gZ,_h0,_h1,_h2,_h3,_h4,_h5,_h6,_h7,_h8));if(_h9!=__continue){return _h9;}}},_hF=function(_hG,_hH,_hI,_hJ,_hK,_hL,_hM,_hN,_hO,_hP,_hQ){if(_hJ<=1){return new F(function(){return _3(_hG,new T2(1,new T2(0,_hJ,_hH),_v));});}else{var _hR=E(_hP),_hS=E(_hN),_hT=E(_hL),_hU=E(_hM),_hV=E(_hQ),_hW=E(_hO),_hX=E(_hI),_hY=Math.sin(_hT*3.141592653589793/180),_hZ=E(_hK),_i0=1.0e-2*_hZ,_i1=(_hR*_hS*_hY*_hY*Math.cos(_hT*3.141592653589793/180)*Math.cos(_hU*3.141592653589793/180)+_hV*(_hW+_hS*_hY*_hY*_hY)*Math.sin(_hU*3.141592653589793/180))/_hX,_i2=1.0e-2*(-9.80665+_i1*_hZ*_hZ),_i3=1.0e-2*(-9.80665+_i1*_i0*_i0+0.5*_i2),_i4=_hZ+0.5*_i2,_i5=1.0e-2*(-9.80665+_i1*_i4*_i4),_i6=_hZ+0.5*_i5,_i7=1.0e-2*(-9.80665+_i1*_i6*_i6),_i8=_hZ+_i7;return new F(function(){return _gX(B(_3(_hG,new T2(1,new T2(0,_hJ,_hH),_v))),new T(function(){return E(_hH)+1.0e-2;}),_hX,_hJ+0.16666666666666666*(_i0+_i3+_i3+1.0e-2*(-9.80665+_i1*_i0*_i0+0.5*_i3)+1.0e-2*(-9.80665+_i1*_i0*_i0+0.5*_i3)+1.0e-2*(_hZ+_i7)),_hZ+0.16666666666666666*(_i2+_i5+_i5+_i7+_i7+1.0e-2*(-9.80665+_i1*_i8*_i8)),_hT,_hU,_hS,_hW,_hR,_hV);});}},_i9=0,_ia=function(_ib,_ic,_id,_ie,_if,_ig,_ih,_ii,_ij,_ik,_il){return new F(function(){return _hF(_v,_i9,_ib,_ik*Math.sin(_il*3.141592653589793/180),_ic,_id,_ie,_ig,_if,new T(function(){return E(_ih)*E(_ij)/2;}),new T(function(){return E(_ih)*E(_ii)/2;}));});},_im=function(_in,_io){while(1){var _ip=E(_in);if(!_ip._){return E(_io);}else{_in=_ip.b;_io=_ip.a;continue;}}},_iq=function(_ir,_is){while(1){var _it=E(_ir);if(!_it._){return E(_is);}else{_ir=_it.b;_is=_it.a;continue;}}},_iu=function(_iv,_iw){while(1){var _ix=E(_iv);if(!_ix._){return E(_iw);}else{_iv=_ix.b;_iw=_ix.a;continue;}}},_iy=new T(function(){return B(unCStr("last"));}),_iz=new T(function(){return B(_9P(_iy));}),_iA=0,_iB=new T(function(){return B(_dx("Server/Jump/Position.hs:(147,1)-(178,69)|function xNumerical"));}),_iC=function(_iD,_iE,_iF,_iG,_iH,_iI,_iJ,_iK,_iL,_iM,_iN,_iO){while(1){var _iP=B((function(_iQ,_iR,_iS,_iT,_iU,_iV,_iW,_iX,_iY,_iZ,_j0,_j1){var _j2=E(_iS);if(!_j2._){return new F(function(){return _3(_iQ,new T2(1,new T(function(){var _j3=E(_iU),_j4=E(_iZ),_j5=E(_iW),_j6=E(_iX),_j7=Math.sin(_j5*3.141592653589793/180),_j8=1.0e-2*(E(_j0)*_j4*_j7*_j7*Math.cos(_j5*3.141592653589793/180)*Math.sin(_j6*3.141592653589793/180)-E(_j1)*(E(_iY)+_j4*_j7*_j7*_j7)*Math.cos(_j6*3.141592653589793/180))/E(_iV),_j9=_j3+0.5*_j8*_j3*_j3,_ja=_j3+0.5*_j8*_j9*_j9;return E(_iT)+0.16666666666666666*(1.0e-2*_j3+1.0e-2*_j9+1.0e-2*_j9+1.0e-2*_ja+1.0e-2*_ja+1.0e-2*(_j3+_j8*_ja*_ja));}),_v));});}else{var _jb=new T(function(){var _jc=E(_iZ),_jd=E(_iW),_je=E(_iX),_jf=Math.sin(_jd*3.141592653589793/180);return E(_j0)*_jc*_jf*_jf*Math.cos(_jd*3.141592653589793/180)*Math.sin(_je*3.141592653589793/180)-E(_j1)*(E(_iY)+_jc*_jf*_jf*_jf)*Math.cos(_je*3.141592653589793/180);}),_jg=new T(function(){var _jh=E(_iU);return 1.0e-2*E(_jb)/E(_iV)*_jh*_jh;}),_ji=new T(function(){var _jj=E(_iU)+0.5*E(_jg);return 1.0e-2*E(_jb)/E(_iV)*_jj*_jj;}),_jk=new T(function(){var _jl=E(_iU)+0.5*E(_ji);return 1.0e-2*E(_jb)/E(_iV)*_jl*_jl;}),_jm=B(_3(_iQ,new T2(1,_iT,_v))),_jn=_iV,_jo=_iW,_jp=_iX,_jq=_iY,_jr=_iZ,_js=_j0,_jt=_j1;_iD=_jm;_iE=_j2.a;_iF=_j2.b;_iG=new T(function(){var _ju=E(_iU),_jv=E(_jg),_jw=E(_ji);return E(_iT)+0.16666666666666666*(1.0e-2*_ju+1.0e-2*(_ju+0.5*_jv)+1.0e-2*(_ju+0.5*_jv)+1.0e-2*(_ju+0.5*_jw)+1.0e-2*(_ju+0.5*_jw)+1.0e-2*(_ju+E(_jk)));});_iH=new T(function(){var _jx=E(_iU),_jy=E(_ji),_jz=E(_jk),_jA=_jx+_jz;return _jx+0.16666666666666666*(E(_jg)+_jy+_jy+_jz+_jz+1.0e-2*E(_jb)/E(_iV)*_jA*_jA);});_iI=_jn;_iJ=_jo;_iK=_jp;_iL=_jq;_iM=_jr;_iN=_js;_iO=_jt;return __continue;}})(_iD,_iE,_iF,_iG,_iH,_iI,_iJ,_iK,_iL,_iM,_iN,_iO));if(_iP!=__continue){return _iP;}}},_jB=function(_jC,_jD,_jE,_jF,_jG,_jH,_jI,_jJ,_jK,_jL,_jM){var _jN=E(_jD);if(!_jN._){return E(_iB);}else{var _jO=E(_jN.b);if(!_jO._){return new F(function(){return _3(_jC,new T2(1,new T(function(){var _jP=E(_jF),_jQ=E(_jK),_jR=E(_jH),_jS=E(_jI),_jT=Math.sin(_jR*3.141592653589793/180),_jU=1.0e-2*(E(_jL)*_jQ*_jT*_jT*Math.cos(_jR*3.141592653589793/180)*Math.sin(_jS*3.141592653589793/180)-E(_jM)*(E(_jJ)+_jQ*_jT*_jT*_jT)*Math.cos(_jS*3.141592653589793/180))/E(_jG),_jV=_jP+0.5*_jU*_jP*_jP,_jW=_jP+0.5*_jU*_jV*_jV;return E(_jE)+0.16666666666666666*(1.0e-2*_jP+1.0e-2*_jV+1.0e-2*_jV+1.0e-2*_jW+1.0e-2*_jW+1.0e-2*(_jP+_jU*_jW*_jW));}),_v));});}else{var _jX=new T(function(){var _jY=E(_jK),_jZ=E(_jH),_k0=E(_jI),_k1=Math.sin(_jZ*3.141592653589793/180);return E(_jL)*_jY*_k1*_k1*Math.cos(_jZ*3.141592653589793/180)*Math.sin(_k0*3.141592653589793/180)-E(_jM)*(E(_jJ)+_jY*_k1*_k1*_k1)*Math.cos(_k0*3.141592653589793/180);}),_k2=new T(function(){var _k3=E(_jF);return 1.0e-2*E(_jX)/E(_jG)*_k3*_k3;}),_k4=new T(function(){var _k5=E(_jF)+0.5*E(_k2);return 1.0e-2*E(_jX)/E(_jG)*_k5*_k5;}),_k6=new T(function(){var _k7=E(_jF)+0.5*E(_k4);return 1.0e-2*E(_jX)/E(_jG)*_k7*_k7;});return new F(function(){return _iC(B(_3(_jC,new T2(1,_jE,_v))),_jO.a,_jO.b,new T(function(){var _k8=E(_jF),_k9=E(_k2),_ka=E(_k4);return E(_jE)+0.16666666666666666*(1.0e-2*_k8+1.0e-2*(_k8+0.5*_k9)+1.0e-2*(_k8+0.5*_k9)+1.0e-2*(_k8+0.5*_ka)+1.0e-2*(_k8+0.5*_ka)+1.0e-2*(_k8+E(_k6)));}),new T(function(){var _kb=E(_jF),_kc=E(_k4),_kd=E(_k6),_ke=_kb+_kd;return _kb+0.16666666666666666*(E(_k2)+_kc+_kc+_kd+_kd+1.0e-2*E(_jX)/E(_jG)*_ke*_ke);}),_jG,_jH,_jI,_jJ,_jK,_jL,_jM);});}}},_kf=function(_kg,_kh,_ki,_kj,_kk,_kl,_km,_kn,_ko,_kp,_kq,_kr){var _ks=new T(function(){return B(_ia(_kg,_kh,_kj,_kk,_kl,_km,_kn,_ko,_kp,E(_kq),E(_kr)));}),_kt=new T(function(){return E(B(_gR(_ks)).b);}),_ku=new T(function(){return B(_iu(B(_jB(_v,_kt,_iA,_ki,_kg,_kj,_kk,_kl,_km,new T(function(){return E(_kn)*E(_kp)/2;}),new T(function(){return E(_kn)*E(_ko)/2;}))),_iz));});return new T3(0,_ku,new T(function(){return E(B(_iq(_ks,_iz)).a);}),new T(function(){return B(_im(_kt,_iz));}));},_kv=new T(function(){return B(unCStr("Negative exponent"));}),_kw=new T(function(){return B(err(_kv));}),_kx=function(_ky,_kz,_kA){while(1){if(!(_kz%2)){var _kB=_ky*_ky,_kC=quot(_kz,2);_ky=_kB;_kz=_kC;continue;}else{var _kD=E(_kz);if(_kD==1){return _ky*_kA;}else{var _kB=_ky*_ky,_kE=_ky*_kA;_ky=_kB;_kz=quot(_kD-1|0,2);_kA=_kE;continue;}}}},_kF=function(_kG,_kH){while(1){if(!(_kH%2)){var _kI=_kG*_kG,_kJ=quot(_kH,2);_kG=_kI;_kH=_kJ;continue;}else{var _kK=E(_kH);if(_kK==1){return E(_kG);}else{return new F(function(){return _kx(_kG*_kG,quot(_kK-1|0,2),_kG);});}}}},_kL=function(_kM,_kN){if(_kN<0){var _kO= -_kN;if(_kO>=0){var _kP=E(_kO);return (_kP==0)?1:1/B(_kF(E(_kM),_kP));}else{return E(_kw);}}else{if(_kN>=0){var _kQ=E(_kN);if(!_kQ){return 1;}else{return new F(function(){return _kF(E(_kM),_kQ);});}}else{return E(_kw);}}},_kR=function(_kS){var _kT=I_decodeDouble(_kS);return new T2(0,new T1(1,_kT.b),_kT.a);},_kU=function(_kV){var _kW=E(_kV);if(!_kW._){return _kW.a;}else{return new F(function(){return I_toNumber(_kW.a);});}},_kX=function(_kY){return new T1(0,_kY);},_kZ=function(_l0){var _l1=hs_intToInt64(2147483647),_l2=hs_leInt64(_l0,_l1);if(!_l2){return new T1(1,I_fromInt64(_l0));}else{var _l3=hs_intToInt64(-2147483648),_l4=hs_geInt64(_l0,_l3);if(!_l4){return new T1(1,I_fromInt64(_l0));}else{var _l5=hs_int64ToInt(_l0);return new F(function(){return _kX(_l5);});}}},_l6=function(_l7){var _l8=hs_intToInt64(_l7);return E(_l8);},_l9=function(_la){var _lb=E(_la);if(!_lb._){return new F(function(){return _l6(_lb.a);});}else{return new F(function(){return I_toInt64(_lb.a);});}},_lc=10,_ld=function(_le,_lf){while(1){var _lg=E(_le);if(!_lg._){_le=new T1(1,I_fromInt(_lg.a));continue;}else{return new T1(1,I_shiftLeft(_lg.a,_lf));}}},_lh=function(_li,_lj){if(_lj>=0){var _lk=function(_ll){var _lm=rintDouble(_li*_ll),_ln=B(_kR(_lm)),_lo=_ln.a,_lp=_ln.b;if(_lp>=0){return B(_kU(B(_ld(_lo,_lp))))/B(_kL(_lc,_lj));}else{var _lq=hs_uncheckedIShiftRA64(B(_l9(_lo)), -_lp);return B(_kU(B(_kZ(_lq))))/B(_kL(_lc,_lj));}},_lr=E(_lj);if(!_lr){return new F(function(){return _lk(1);});}else{return new F(function(){return _lk(B(_kF(10,_lr)));});}}else{return E(_kw);}},_ls=new T(function(){return B(unCStr("Prelude.read: no parse"));}),_lt=new T(function(){return B(err(_ls));}),_lu=new T(function(){return B(unCStr("Prelude.read: ambiguous parse"));}),_lv=new T(function(){return B(err(_lu));}),_lw=new T(function(){return B(_dx("Text/ParserCombinators/ReadP.hs:(128,3)-(151,52)|function <|>"));}),_lx=function(_ly,_lz){while(1){var _lA=B((function(_lB,_lC){var _lD=E(_lB);switch(_lD._){case 0:var _lE=E(_lC);if(!_lE._){return __Z;}else{_ly=B(A1(_lD.a,_lE.a));_lz=_lE.b;return __continue;}break;case 1:var _lF=B(A1(_lD.a,_lC)),_lG=_lC;_ly=_lF;_lz=_lG;return __continue;case 2:return __Z;case 3:return new T2(1,new T2(0,_lD.a,_lC),new T(function(){return B(_lx(_lD.b,_lC));}));default:return E(_lD.a);}})(_ly,_lz));if(_lA!=__continue){return _lA;}}},_lH=function(_lI,_lJ){var _lK=function(_lL){var _lM=E(_lJ);if(_lM._==3){return new T2(3,_lM.a,new T(function(){return B(_lH(_lI,_lM.b));}));}else{var _lN=E(_lI);if(_lN._==2){return E(_lM);}else{var _lO=E(_lM);if(_lO._==2){return E(_lN);}else{var _lP=function(_lQ){var _lR=E(_lO);if(_lR._==4){var _lS=function(_lT){return new T1(4,new T(function(){return B(_3(B(_lx(_lN,_lT)),_lR.a));}));};return new T1(1,_lS);}else{var _lU=E(_lN);if(_lU._==1){var _lV=_lU.a,_lW=E(_lR);if(!_lW._){return new T1(1,function(_lX){return new F(function(){return _lH(B(A1(_lV,_lX)),_lW);});});}else{var _lY=function(_lZ){return new F(function(){return _lH(B(A1(_lV,_lZ)),new T(function(){return B(A1(_lW.a,_lZ));}));});};return new T1(1,_lY);}}else{var _m0=E(_lR);if(!_m0._){return E(_lw);}else{var _m1=function(_m2){return new F(function(){return _lH(_lU,new T(function(){return B(A1(_m0.a,_m2));}));});};return new T1(1,_m1);}}}},_m3=E(_lN);switch(_m3._){case 1:var _m4=E(_lO);if(_m4._==4){var _m5=function(_m6){return new T1(4,new T(function(){return B(_3(B(_lx(B(A1(_m3.a,_m6)),_m6)),_m4.a));}));};return new T1(1,_m5);}else{return new F(function(){return _lP(_);});}break;case 4:var _m7=_m3.a,_m8=E(_lO);switch(_m8._){case 0:var _m9=function(_ma){var _mb=new T(function(){return B(_3(_m7,new T(function(){return B(_lx(_m8,_ma));},1)));});return new T1(4,_mb);};return new T1(1,_m9);case 1:var _mc=function(_md){var _me=new T(function(){return B(_3(_m7,new T(function(){return B(_lx(B(A1(_m8.a,_md)),_md));},1)));});return new T1(4,_me);};return new T1(1,_mc);default:return new T1(4,new T(function(){return B(_3(_m7,_m8.a));}));}break;default:return new F(function(){return _lP(_);});}}}}},_mf=E(_lI);switch(_mf._){case 0:var _mg=E(_lJ);if(!_mg._){var _mh=function(_mi){return new F(function(){return _lH(B(A1(_mf.a,_mi)),new T(function(){return B(A1(_mg.a,_mi));}));});};return new T1(0,_mh);}else{return new F(function(){return _lK(_);});}break;case 3:return new T2(3,_mf.a,new T(function(){return B(_lH(_mf.b,_lJ));}));default:return new F(function(){return _lK(_);});}},_mj=new T(function(){return B(unCStr("("));}),_mk=new T(function(){return B(unCStr(")"));}),_ml=function(_mm,_mn){return E(_mm)!=E(_mn);},_mo=function(_mp,_mq){return E(_mp)==E(_mq);},_mr=new T2(0,_mo,_ml),_ms=function(_mt,_mu){while(1){var _mv=E(_mt);if(!_mv._){return (E(_mu)._==0)?true:false;}else{var _mw=E(_mu);if(!_mw._){return false;}else{if(E(_mv.a)!=E(_mw.a)){return false;}else{_mt=_mv.b;_mu=_mw.b;continue;}}}}},_mx=function(_my,_mz){return (!B(_ms(_my,_mz)))?true:false;},_mA=new T2(0,_ms,_mx),_mB=function(_mC,_mD){var _mE=E(_mC);switch(_mE._){case 0:return new T1(0,function(_mF){return new F(function(){return _mB(B(A1(_mE.a,_mF)),_mD);});});case 1:return new T1(1,function(_mG){return new F(function(){return _mB(B(A1(_mE.a,_mG)),_mD);});});case 2:return new T0(2);case 3:return new F(function(){return _lH(B(A1(_mD,_mE.a)),new T(function(){return B(_mB(_mE.b,_mD));}));});break;default:var _mH=function(_mI){var _mJ=E(_mI);if(!_mJ._){return __Z;}else{var _mK=E(_mJ.a);return new F(function(){return _3(B(_lx(B(A1(_mD,_mK.a)),_mK.b)),new T(function(){return B(_mH(_mJ.b));},1));});}},_mL=B(_mH(_mE.a));return (_mL._==0)?new T0(2):new T1(4,_mL);}},_mM=new T0(2),_mN=function(_mO){return new T2(3,_mO,_mM);},_mP=function(_mQ,_mR){var _mS=E(_mQ);if(!_mS){return new F(function(){return A1(_mR,_3f);});}else{var _mT=new T(function(){return B(_mP(_mS-1|0,_mR));});return new T1(0,function(_mU){return E(_mT);});}},_mV=function(_mW,_mX,_mY){var _mZ=new T(function(){return B(A1(_mW,_mN));}),_n0=function(_n1,_n2,_n3,_n4){while(1){var _n5=B((function(_n6,_n7,_n8,_n9){var _na=E(_n6);switch(_na._){case 0:var _nb=E(_n7);if(!_nb._){return new F(function(){return A1(_mX,_n9);});}else{var _nc=_n8+1|0,_nd=_n9;_n1=B(A1(_na.a,_nb.a));_n2=_nb.b;_n3=_nc;_n4=_nd;return __continue;}break;case 1:var _ne=B(A1(_na.a,_n7)),_nf=_n7,_nc=_n8,_nd=_n9;_n1=_ne;_n2=_nf;_n3=_nc;_n4=_nd;return __continue;case 2:return new F(function(){return A1(_mX,_n9);});break;case 3:var _ng=new T(function(){return B(_mB(_na,_n9));});return new F(function(){return _mP(_n8,function(_nh){return E(_ng);});});break;default:return new F(function(){return _mB(_na,_n9);});}})(_n1,_n2,_n3,_n4));if(_n5!=__continue){return _n5;}}};return function(_ni){return new F(function(){return _n0(_mZ,_ni,0,_mY);});};},_nj=function(_nk){return new F(function(){return A1(_nk,_v);});},_nl=function(_nm,_nn){var _no=function(_np){var _nq=E(_np);if(!_nq._){return E(_nj);}else{var _nr=_nq.a;if(!B(A1(_nm,_nr))){return E(_nj);}else{var _ns=new T(function(){return B(_no(_nq.b));}),_nt=function(_nu){var _nv=new T(function(){return B(A1(_ns,function(_nw){return new F(function(){return A1(_nu,new T2(1,_nr,_nw));});}));});return new T1(0,function(_nx){return E(_nv);});};return E(_nt);}}};return function(_ny){return new F(function(){return A2(_no,_ny,_nn);});};},_nz=new T0(6),_nA=new T(function(){return B(unCStr("valDig: Bad base"));}),_nB=new T(function(){return B(err(_nA));}),_nC=function(_nD,_nE){var _nF=function(_nG,_nH){var _nI=E(_nG);if(!_nI._){var _nJ=new T(function(){return B(A1(_nH,_v));});return function(_nK){return new F(function(){return A1(_nK,_nJ);});};}else{var _nL=E(_nI.a),_nM=function(_nN){var _nO=new T(function(){return B(_nF(_nI.b,function(_nP){return new F(function(){return A1(_nH,new T2(1,_nN,_nP));});}));}),_nQ=function(_nR){var _nS=new T(function(){return B(A1(_nO,_nR));});return new T1(0,function(_nT){return E(_nS);});};return E(_nQ);};switch(E(_nD)){case 8:if(48>_nL){var _nU=new T(function(){return B(A1(_nH,_v));});return function(_nV){return new F(function(){return A1(_nV,_nU);});};}else{if(_nL>55){var _nW=new T(function(){return B(A1(_nH,_v));});return function(_nX){return new F(function(){return A1(_nX,_nW);});};}else{return new F(function(){return _nM(_nL-48|0);});}}break;case 10:if(48>_nL){var _nY=new T(function(){return B(A1(_nH,_v));});return function(_nZ){return new F(function(){return A1(_nZ,_nY);});};}else{if(_nL>57){var _o0=new T(function(){return B(A1(_nH,_v));});return function(_o1){return new F(function(){return A1(_o1,_o0);});};}else{return new F(function(){return _nM(_nL-48|0);});}}break;case 16:if(48>_nL){if(97>_nL){if(65>_nL){var _o2=new T(function(){return B(A1(_nH,_v));});return function(_o3){return new F(function(){return A1(_o3,_o2);});};}else{if(_nL>70){var _o4=new T(function(){return B(A1(_nH,_v));});return function(_o5){return new F(function(){return A1(_o5,_o4);});};}else{return new F(function(){return _nM((_nL-65|0)+10|0);});}}}else{if(_nL>102){if(65>_nL){var _o6=new T(function(){return B(A1(_nH,_v));});return function(_o7){return new F(function(){return A1(_o7,_o6);});};}else{if(_nL>70){var _o8=new T(function(){return B(A1(_nH,_v));});return function(_o9){return new F(function(){return A1(_o9,_o8);});};}else{return new F(function(){return _nM((_nL-65|0)+10|0);});}}}else{return new F(function(){return _nM((_nL-97|0)+10|0);});}}}else{if(_nL>57){if(97>_nL){if(65>_nL){var _oa=new T(function(){return B(A1(_nH,_v));});return function(_ob){return new F(function(){return A1(_ob,_oa);});};}else{if(_nL>70){var _oc=new T(function(){return B(A1(_nH,_v));});return function(_od){return new F(function(){return A1(_od,_oc);});};}else{return new F(function(){return _nM((_nL-65|0)+10|0);});}}}else{if(_nL>102){if(65>_nL){var _oe=new T(function(){return B(A1(_nH,_v));});return function(_of){return new F(function(){return A1(_of,_oe);});};}else{if(_nL>70){var _og=new T(function(){return B(A1(_nH,_v));});return function(_oh){return new F(function(){return A1(_oh,_og);});};}else{return new F(function(){return _nM((_nL-65|0)+10|0);});}}}else{return new F(function(){return _nM((_nL-97|0)+10|0);});}}}else{return new F(function(){return _nM(_nL-48|0);});}}break;default:return E(_nB);}}},_oi=function(_oj){var _ok=E(_oj);if(!_ok._){return new T0(2);}else{return new F(function(){return A1(_nE,_ok);});}};return function(_ol){return new F(function(){return A3(_nF,_ol,_3j,_oi);});};},_om=16,_on=8,_oo=function(_op){var _oq=function(_or){return new F(function(){return A1(_op,new T1(5,new T2(0,_on,_or)));});},_os=function(_ot){return new F(function(){return A1(_op,new T1(5,new T2(0,_om,_ot)));});},_ou=function(_ov){switch(E(_ov)){case 79:return new T1(1,B(_nC(_on,_oq)));case 88:return new T1(1,B(_nC(_om,_os)));case 111:return new T1(1,B(_nC(_on,_oq)));case 120:return new T1(1,B(_nC(_om,_os)));default:return new T0(2);}};return function(_ow){return (E(_ow)==48)?E(new T1(0,_ou)):new T0(2);};},_ox=function(_oy){return new T1(0,B(_oo(_oy)));},_oz=function(_oA){return new F(function(){return A1(_oA,_2q);});},_oB=function(_oC){return new F(function(){return A1(_oC,_2q);});},_oD=10,_oE=new T1(0,1),_oF=new T1(0,2147483647),_oG=function(_oH,_oI){while(1){var _oJ=E(_oH);if(!_oJ._){var _oK=_oJ.a,_oL=E(_oI);if(!_oL._){var _oM=_oL.a,_oN=addC(_oK,_oM);if(!E(_oN.b)){return new T1(0,_oN.a);}else{_oH=new T1(1,I_fromInt(_oK));_oI=new T1(1,I_fromInt(_oM));continue;}}else{_oH=new T1(1,I_fromInt(_oK));_oI=_oL;continue;}}else{var _oO=E(_oI);if(!_oO._){_oH=_oJ;_oI=new T1(1,I_fromInt(_oO.a));continue;}else{return new T1(1,I_add(_oJ.a,_oO.a));}}}},_oP=new T(function(){return B(_oG(_oF,_oE));}),_oQ=function(_oR){var _oS=E(_oR);if(!_oS._){var _oT=E(_oS.a);return (_oT==(-2147483648))?E(_oP):new T1(0, -_oT);}else{return new T1(1,I_negate(_oS.a));}},_oU=new T1(0,10),_oV=function(_oW,_oX){while(1){var _oY=E(_oW);if(!_oY._){return E(_oX);}else{var _oZ=_oX+1|0;_oW=_oY.b;_oX=_oZ;continue;}}},_p0=function(_p1,_p2){var _p3=E(_p2);return (_p3._==0)?__Z:new T2(1,new T(function(){return B(A1(_p1,_p3.a));}),new T(function(){return B(_p0(_p1,_p3.b));}));},_p4=function(_p5){return new F(function(){return _kX(E(_p5));});},_p6=new T(function(){return B(unCStr("this should not happen"));}),_p7=new T(function(){return B(err(_p6));}),_p8=function(_p9,_pa){while(1){var _pb=E(_p9);if(!_pb._){var _pc=_pb.a,_pd=E(_pa);if(!_pd._){var _pe=_pd.a;if(!(imul(_pc,_pe)|0)){return new T1(0,imul(_pc,_pe)|0);}else{_p9=new T1(1,I_fromInt(_pc));_pa=new T1(1,I_fromInt(_pe));continue;}}else{_p9=new T1(1,I_fromInt(_pc));_pa=_pd;continue;}}else{var _pf=E(_pa);if(!_pf._){_p9=_pb;_pa=new T1(1,I_fromInt(_pf.a));continue;}else{return new T1(1,I_mul(_pb.a,_pf.a));}}}},_pg=function(_ph,_pi){var _pj=E(_pi);if(!_pj._){return __Z;}else{var _pk=E(_pj.b);return (_pk._==0)?E(_p7):new T2(1,B(_oG(B(_p8(_pj.a,_ph)),_pk.a)),new T(function(){return B(_pg(_ph,_pk.b));}));}},_pl=new T1(0,0),_pm=function(_pn,_po,_pp){while(1){var _pq=B((function(_pr,_ps,_pt){var _pu=E(_pt);if(!_pu._){return E(_pl);}else{if(!E(_pu.b)._){return E(_pu.a);}else{var _pv=E(_ps);if(_pv<=40){var _pw=function(_px,_py){while(1){var _pz=E(_py);if(!_pz._){return E(_px);}else{var _pA=B(_oG(B(_p8(_px,_pr)),_pz.a));_px=_pA;_py=_pz.b;continue;}}};return new F(function(){return _pw(_pl,_pu);});}else{var _pB=B(_p8(_pr,_pr));if(!(_pv%2)){var _pC=B(_pg(_pr,_pu));_pn=_pB;_po=quot(_pv+1|0,2);_pp=_pC;return __continue;}else{var _pC=B(_pg(_pr,new T2(1,_pl,_pu)));_pn=_pB;_po=quot(_pv+1|0,2);_pp=_pC;return __continue;}}}}})(_pn,_po,_pp));if(_pq!=__continue){return _pq;}}},_pD=function(_pE,_pF){return new F(function(){return _pm(_pE,new T(function(){return B(_oV(_pF,0));},1),B(_p0(_p4,_pF)));});},_pG=function(_pH){var _pI=new T(function(){var _pJ=new T(function(){var _pK=function(_pL){return new F(function(){return A1(_pH,new T1(1,new T(function(){return B(_pD(_oU,_pL));})));});};return new T1(1,B(_nC(_oD,_pK)));}),_pM=function(_pN){if(E(_pN)==43){var _pO=function(_pP){return new F(function(){return A1(_pH,new T1(1,new T(function(){return B(_pD(_oU,_pP));})));});};return new T1(1,B(_nC(_oD,_pO)));}else{return new T0(2);}},_pQ=function(_pR){if(E(_pR)==45){var _pS=function(_pT){return new F(function(){return A1(_pH,new T1(1,new T(function(){return B(_oQ(B(_pD(_oU,_pT))));})));});};return new T1(1,B(_nC(_oD,_pS)));}else{return new T0(2);}};return B(_lH(B(_lH(new T1(0,_pQ),new T1(0,_pM))),_pJ));});return new F(function(){return _lH(new T1(0,function(_pU){return (E(_pU)==101)?E(_pI):new T0(2);}),new T1(0,function(_pV){return (E(_pV)==69)?E(_pI):new T0(2);}));});},_pW=function(_pX){var _pY=function(_pZ){return new F(function(){return A1(_pX,new T1(1,_pZ));});};return function(_q0){return (E(_q0)==46)?new T1(1,B(_nC(_oD,_pY))):new T0(2);};},_q1=function(_q2){return new T1(0,B(_pW(_q2)));},_q3=function(_q4){var _q5=function(_q6){var _q7=function(_q8){return new T1(1,B(_mV(_pG,_oz,function(_q9){return new F(function(){return A1(_q4,new T1(5,new T3(1,_q6,_q8,_q9)));});})));};return new T1(1,B(_mV(_q1,_oB,_q7)));};return new F(function(){return _nC(_oD,_q5);});},_qa=function(_qb){return new T1(1,B(_q3(_qb)));},_qc=function(_qd){return E(E(_qd).a);},_qe=function(_qf,_qg,_qh){while(1){var _qi=E(_qh);if(!_qi._){return false;}else{if(!B(A3(_qc,_qf,_qg,_qi.a))){_qh=_qi.b;continue;}else{return true;}}}},_qj=new T(function(){return B(unCStr("!@#$%&*+./<=>?\\^|:-~"));}),_qk=function(_ql){return new F(function(){return _qe(_mr,_ql,_qj);});},_qm=false,_qn=true,_qo=function(_qp){var _qq=new T(function(){return B(A1(_qp,_on));}),_qr=new T(function(){return B(A1(_qp,_om));});return function(_qs){switch(E(_qs)){case 79:return E(_qq);case 88:return E(_qr);case 111:return E(_qq);case 120:return E(_qr);default:return new T0(2);}};},_qt=function(_qu){return new T1(0,B(_qo(_qu)));},_qv=function(_qw){return new F(function(){return A1(_qw,_oD);});},_qx=function(_qy){return new F(function(){return err(B(unAppCStr("Prelude.chr: bad argument: ",new T(function(){return B(_d(9,_qy,_v));}))));});},_qz=function(_qA){var _qB=E(_qA);if(!_qB._){return E(_qB.a);}else{return new F(function(){return I_toInt(_qB.a);});}},_qC=function(_qD,_qE){var _qF=E(_qD);if(!_qF._){var _qG=_qF.a,_qH=E(_qE);return (_qH._==0)?_qG<=_qH.a:I_compareInt(_qH.a,_qG)>=0;}else{var _qI=_qF.a,_qJ=E(_qE);return (_qJ._==0)?I_compareInt(_qI,_qJ.a)<=0:I_compare(_qI,_qJ.a)<=0;}},_qK=function(_qL){return new T0(2);},_qM=function(_qN){var _qO=E(_qN);if(!_qO._){return E(_qK);}else{var _qP=_qO.a,_qQ=E(_qO.b);if(!_qQ._){return E(_qP);}else{var _qR=new T(function(){return B(_qM(_qQ));}),_qS=function(_qT){return new F(function(){return _lH(B(A1(_qP,_qT)),new T(function(){return B(A1(_qR,_qT));}));});};return E(_qS);}}},_qU=function(_qV,_qW){var _qX=function(_qY,_qZ,_r0){var _r1=E(_qY);if(!_r1._){return new F(function(){return A1(_r0,_qV);});}else{var _r2=E(_qZ);if(!_r2._){return new T0(2);}else{if(E(_r1.a)!=E(_r2.a)){return new T0(2);}else{var _r3=new T(function(){return B(_qX(_r1.b,_r2.b,_r0));});return new T1(0,function(_r4){return E(_r3);});}}}};return function(_r5){return new F(function(){return _qX(_qV,_r5,_qW);});};},_r6=new T(function(){return B(unCStr("SO"));}),_r7=14,_r8=function(_r9){var _ra=new T(function(){return B(A1(_r9,_r7));});return new T1(1,B(_qU(_r6,function(_rb){return E(_ra);})));},_rc=new T(function(){return B(unCStr("SOH"));}),_rd=1,_re=function(_rf){var _rg=new T(function(){return B(A1(_rf,_rd));});return new T1(1,B(_qU(_rc,function(_rh){return E(_rg);})));},_ri=function(_rj){return new T1(1,B(_mV(_re,_r8,_rj)));},_rk=new T(function(){return B(unCStr("NUL"));}),_rl=0,_rm=function(_rn){var _ro=new T(function(){return B(A1(_rn,_rl));});return new T1(1,B(_qU(_rk,function(_rp){return E(_ro);})));},_rq=new T(function(){return B(unCStr("STX"));}),_rr=2,_rs=function(_rt){var _ru=new T(function(){return B(A1(_rt,_rr));});return new T1(1,B(_qU(_rq,function(_rv){return E(_ru);})));},_rw=new T(function(){return B(unCStr("ETX"));}),_rx=3,_ry=function(_rz){var _rA=new T(function(){return B(A1(_rz,_rx));});return new T1(1,B(_qU(_rw,function(_rB){return E(_rA);})));},_rC=new T(function(){return B(unCStr("EOT"));}),_rD=4,_rE=function(_rF){var _rG=new T(function(){return B(A1(_rF,_rD));});return new T1(1,B(_qU(_rC,function(_rH){return E(_rG);})));},_rI=new T(function(){return B(unCStr("ENQ"));}),_rJ=5,_rK=function(_rL){var _rM=new T(function(){return B(A1(_rL,_rJ));});return new T1(1,B(_qU(_rI,function(_rN){return E(_rM);})));},_rO=new T(function(){return B(unCStr("ACK"));}),_rP=6,_rQ=function(_rR){var _rS=new T(function(){return B(A1(_rR,_rP));});return new T1(1,B(_qU(_rO,function(_rT){return E(_rS);})));},_rU=new T(function(){return B(unCStr("BEL"));}),_rV=7,_rW=function(_rX){var _rY=new T(function(){return B(A1(_rX,_rV));});return new T1(1,B(_qU(_rU,function(_rZ){return E(_rY);})));},_s0=new T(function(){return B(unCStr("BS"));}),_s1=8,_s2=function(_s3){var _s4=new T(function(){return B(A1(_s3,_s1));});return new T1(1,B(_qU(_s0,function(_s5){return E(_s4);})));},_s6=new T(function(){return B(unCStr("HT"));}),_s7=9,_s8=function(_s9){var _sa=new T(function(){return B(A1(_s9,_s7));});return new T1(1,B(_qU(_s6,function(_sb){return E(_sa);})));},_sc=new T(function(){return B(unCStr("LF"));}),_sd=10,_se=function(_sf){var _sg=new T(function(){return B(A1(_sf,_sd));});return new T1(1,B(_qU(_sc,function(_sh){return E(_sg);})));},_si=new T(function(){return B(unCStr("VT"));}),_sj=11,_sk=function(_sl){var _sm=new T(function(){return B(A1(_sl,_sj));});return new T1(1,B(_qU(_si,function(_sn){return E(_sm);})));},_so=new T(function(){return B(unCStr("FF"));}),_sp=12,_sq=function(_sr){var _ss=new T(function(){return B(A1(_sr,_sp));});return new T1(1,B(_qU(_so,function(_st){return E(_ss);})));},_su=new T(function(){return B(unCStr("CR"));}),_sv=13,_sw=function(_sx){var _sy=new T(function(){return B(A1(_sx,_sv));});return new T1(1,B(_qU(_su,function(_sz){return E(_sy);})));},_sA=new T(function(){return B(unCStr("SI"));}),_sB=15,_sC=function(_sD){var _sE=new T(function(){return B(A1(_sD,_sB));});return new T1(1,B(_qU(_sA,function(_sF){return E(_sE);})));},_sG=new T(function(){return B(unCStr("DLE"));}),_sH=16,_sI=function(_sJ){var _sK=new T(function(){return B(A1(_sJ,_sH));});return new T1(1,B(_qU(_sG,function(_sL){return E(_sK);})));},_sM=new T(function(){return B(unCStr("DC1"));}),_sN=17,_sO=function(_sP){var _sQ=new T(function(){return B(A1(_sP,_sN));});return new T1(1,B(_qU(_sM,function(_sR){return E(_sQ);})));},_sS=new T(function(){return B(unCStr("DC2"));}),_sT=18,_sU=function(_sV){var _sW=new T(function(){return B(A1(_sV,_sT));});return new T1(1,B(_qU(_sS,function(_sX){return E(_sW);})));},_sY=new T(function(){return B(unCStr("DC3"));}),_sZ=19,_t0=function(_t1){var _t2=new T(function(){return B(A1(_t1,_sZ));});return new T1(1,B(_qU(_sY,function(_t3){return E(_t2);})));},_t4=new T(function(){return B(unCStr("DC4"));}),_t5=20,_t6=function(_t7){var _t8=new T(function(){return B(A1(_t7,_t5));});return new T1(1,B(_qU(_t4,function(_t9){return E(_t8);})));},_ta=new T(function(){return B(unCStr("NAK"));}),_tb=21,_tc=function(_td){var _te=new T(function(){return B(A1(_td,_tb));});return new T1(1,B(_qU(_ta,function(_tf){return E(_te);})));},_tg=new T(function(){return B(unCStr("SYN"));}),_th=22,_ti=function(_tj){var _tk=new T(function(){return B(A1(_tj,_th));});return new T1(1,B(_qU(_tg,function(_tl){return E(_tk);})));},_tm=new T(function(){return B(unCStr("ETB"));}),_tn=23,_to=function(_tp){var _tq=new T(function(){return B(A1(_tp,_tn));});return new T1(1,B(_qU(_tm,function(_tr){return E(_tq);})));},_ts=new T(function(){return B(unCStr("CAN"));}),_tt=24,_tu=function(_tv){var _tw=new T(function(){return B(A1(_tv,_tt));});return new T1(1,B(_qU(_ts,function(_tx){return E(_tw);})));},_ty=new T(function(){return B(unCStr("EM"));}),_tz=25,_tA=function(_tB){var _tC=new T(function(){return B(A1(_tB,_tz));});return new T1(1,B(_qU(_ty,function(_tD){return E(_tC);})));},_tE=new T(function(){return B(unCStr("SUB"));}),_tF=26,_tG=function(_tH){var _tI=new T(function(){return B(A1(_tH,_tF));});return new T1(1,B(_qU(_tE,function(_tJ){return E(_tI);})));},_tK=new T(function(){return B(unCStr("ESC"));}),_tL=27,_tM=function(_tN){var _tO=new T(function(){return B(A1(_tN,_tL));});return new T1(1,B(_qU(_tK,function(_tP){return E(_tO);})));},_tQ=new T(function(){return B(unCStr("FS"));}),_tR=28,_tS=function(_tT){var _tU=new T(function(){return B(A1(_tT,_tR));});return new T1(1,B(_qU(_tQ,function(_tV){return E(_tU);})));},_tW=new T(function(){return B(unCStr("GS"));}),_tX=29,_tY=function(_tZ){var _u0=new T(function(){return B(A1(_tZ,_tX));});return new T1(1,B(_qU(_tW,function(_u1){return E(_u0);})));},_u2=new T(function(){return B(unCStr("RS"));}),_u3=30,_u4=function(_u5){var _u6=new T(function(){return B(A1(_u5,_u3));});return new T1(1,B(_qU(_u2,function(_u7){return E(_u6);})));},_u8=new T(function(){return B(unCStr("US"));}),_u9=31,_ua=function(_ub){var _uc=new T(function(){return B(A1(_ub,_u9));});return new T1(1,B(_qU(_u8,function(_ud){return E(_uc);})));},_ue=new T(function(){return B(unCStr("SP"));}),_uf=32,_ug=function(_uh){var _ui=new T(function(){return B(A1(_uh,_uf));});return new T1(1,B(_qU(_ue,function(_uj){return E(_ui);})));},_uk=new T(function(){return B(unCStr("DEL"));}),_ul=127,_um=function(_un){var _uo=new T(function(){return B(A1(_un,_ul));});return new T1(1,B(_qU(_uk,function(_up){return E(_uo);})));},_uq=new T2(1,_um,_v),_ur=new T2(1,_ug,_uq),_us=new T2(1,_ua,_ur),_ut=new T2(1,_u4,_us),_uu=new T2(1,_tY,_ut),_uv=new T2(1,_tS,_uu),_uw=new T2(1,_tM,_uv),_ux=new T2(1,_tG,_uw),_uy=new T2(1,_tA,_ux),_uz=new T2(1,_tu,_uy),_uA=new T2(1,_to,_uz),_uB=new T2(1,_ti,_uA),_uC=new T2(1,_tc,_uB),_uD=new T2(1,_t6,_uC),_uE=new T2(1,_t0,_uD),_uF=new T2(1,_sU,_uE),_uG=new T2(1,_sO,_uF),_uH=new T2(1,_sI,_uG),_uI=new T2(1,_sC,_uH),_uJ=new T2(1,_sw,_uI),_uK=new T2(1,_sq,_uJ),_uL=new T2(1,_sk,_uK),_uM=new T2(1,_se,_uL),_uN=new T2(1,_s8,_uM),_uO=new T2(1,_s2,_uN),_uP=new T2(1,_rW,_uO),_uQ=new T2(1,_rQ,_uP),_uR=new T2(1,_rK,_uQ),_uS=new T2(1,_rE,_uR),_uT=new T2(1,_ry,_uS),_uU=new T2(1,_rs,_uT),_uV=new T2(1,_rm,_uU),_uW=new T2(1,_ri,_uV),_uX=new T(function(){return B(_qM(_uW));}),_uY=34,_uZ=new T1(0,1114111),_v0=92,_v1=39,_v2=function(_v3){var _v4=new T(function(){return B(A1(_v3,_rV));}),_v5=new T(function(){return B(A1(_v3,_s1));}),_v6=new T(function(){return B(A1(_v3,_s7));}),_v7=new T(function(){return B(A1(_v3,_sd));}),_v8=new T(function(){return B(A1(_v3,_sj));}),_v9=new T(function(){return B(A1(_v3,_sp));}),_va=new T(function(){return B(A1(_v3,_sv));}),_vb=new T(function(){return B(A1(_v3,_v0));}),_vc=new T(function(){return B(A1(_v3,_v1));}),_vd=new T(function(){return B(A1(_v3,_uY));}),_ve=new T(function(){var _vf=function(_vg){var _vh=new T(function(){return B(_kX(E(_vg)));}),_vi=function(_vj){var _vk=B(_pD(_vh,_vj));if(!B(_qC(_vk,_uZ))){return new T0(2);}else{return new F(function(){return A1(_v3,new T(function(){var _vl=B(_qz(_vk));if(_vl>>>0>1114111){return B(_qx(_vl));}else{return _vl;}}));});}};return new T1(1,B(_nC(_vg,_vi)));},_vm=new T(function(){var _vn=new T(function(){return B(A1(_v3,_u9));}),_vo=new T(function(){return B(A1(_v3,_u3));}),_vp=new T(function(){return B(A1(_v3,_tX));}),_vq=new T(function(){return B(A1(_v3,_tR));}),_vr=new T(function(){return B(A1(_v3,_tL));}),_vs=new T(function(){return B(A1(_v3,_tF));}),_vt=new T(function(){return B(A1(_v3,_tz));}),_vu=new T(function(){return B(A1(_v3,_tt));}),_vv=new T(function(){return B(A1(_v3,_tn));}),_vw=new T(function(){return B(A1(_v3,_th));}),_vx=new T(function(){return B(A1(_v3,_tb));}),_vy=new T(function(){return B(A1(_v3,_t5));}),_vz=new T(function(){return B(A1(_v3,_sZ));}),_vA=new T(function(){return B(A1(_v3,_sT));}),_vB=new T(function(){return B(A1(_v3,_sN));}),_vC=new T(function(){return B(A1(_v3,_sH));}),_vD=new T(function(){return B(A1(_v3,_sB));}),_vE=new T(function(){return B(A1(_v3,_r7));}),_vF=new T(function(){return B(A1(_v3,_rP));}),_vG=new T(function(){return B(A1(_v3,_rJ));}),_vH=new T(function(){return B(A1(_v3,_rD));}),_vI=new T(function(){return B(A1(_v3,_rx));}),_vJ=new T(function(){return B(A1(_v3,_rr));}),_vK=new T(function(){return B(A1(_v3,_rd));}),_vL=new T(function(){return B(A1(_v3,_rl));}),_vM=function(_vN){switch(E(_vN)){case 64:return E(_vL);case 65:return E(_vK);case 66:return E(_vJ);case 67:return E(_vI);case 68:return E(_vH);case 69:return E(_vG);case 70:return E(_vF);case 71:return E(_v4);case 72:return E(_v5);case 73:return E(_v6);case 74:return E(_v7);case 75:return E(_v8);case 76:return E(_v9);case 77:return E(_va);case 78:return E(_vE);case 79:return E(_vD);case 80:return E(_vC);case 81:return E(_vB);case 82:return E(_vA);case 83:return E(_vz);case 84:return E(_vy);case 85:return E(_vx);case 86:return E(_vw);case 87:return E(_vv);case 88:return E(_vu);case 89:return E(_vt);case 90:return E(_vs);case 91:return E(_vr);case 92:return E(_vq);case 93:return E(_vp);case 94:return E(_vo);case 95:return E(_vn);default:return new T0(2);}};return B(_lH(new T1(0,function(_vO){return (E(_vO)==94)?E(new T1(0,_vM)):new T0(2);}),new T(function(){return B(A1(_uX,_v3));})));});return B(_lH(new T1(1,B(_mV(_qt,_qv,_vf))),_vm));});return new F(function(){return _lH(new T1(0,function(_vP){switch(E(_vP)){case 34:return E(_vd);case 39:return E(_vc);case 92:return E(_vb);case 97:return E(_v4);case 98:return E(_v5);case 102:return E(_v9);case 110:return E(_v7);case 114:return E(_va);case 116:return E(_v6);case 118:return E(_v8);default:return new T0(2);}}),_ve);});},_vQ=function(_vR){return new F(function(){return A1(_vR,_3f);});},_vS=function(_vT){var _vU=E(_vT);if(!_vU._){return E(_vQ);}else{var _vV=E(_vU.a),_vW=_vV>>>0,_vX=new T(function(){return B(_vS(_vU.b));});if(_vW>887){var _vY=u_iswspace(_vV);if(!E(_vY)){return E(_vQ);}else{var _vZ=function(_w0){var _w1=new T(function(){return B(A1(_vX,_w0));});return new T1(0,function(_w2){return E(_w1);});};return E(_vZ);}}else{var _w3=E(_vW);if(_w3==32){var _w4=function(_w5){var _w6=new T(function(){return B(A1(_vX,_w5));});return new T1(0,function(_w7){return E(_w6);});};return E(_w4);}else{if(_w3-9>>>0>4){if(E(_w3)==160){var _w8=function(_w9){var _wa=new T(function(){return B(A1(_vX,_w9));});return new T1(0,function(_wb){return E(_wa);});};return E(_w8);}else{return E(_vQ);}}else{var _wc=function(_wd){var _we=new T(function(){return B(A1(_vX,_wd));});return new T1(0,function(_wf){return E(_we);});};return E(_wc);}}}}},_wg=function(_wh){var _wi=new T(function(){return B(_wg(_wh));}),_wj=function(_wk){return (E(_wk)==92)?E(_wi):new T0(2);},_wl=function(_wm){return E(new T1(0,_wj));},_wn=new T1(1,function(_wo){return new F(function(){return A2(_vS,_wo,_wl);});}),_wp=new T(function(){return B(_v2(function(_wq){return new F(function(){return A1(_wh,new T2(0,_wq,_qn));});}));}),_wr=function(_ws){var _wt=E(_ws);if(_wt==38){return E(_wi);}else{var _wu=_wt>>>0;if(_wu>887){var _wv=u_iswspace(_wt);return (E(_wv)==0)?new T0(2):E(_wn);}else{var _ww=E(_wu);return (_ww==32)?E(_wn):(_ww-9>>>0>4)?(E(_ww)==160)?E(_wn):new T0(2):E(_wn);}}};return new F(function(){return _lH(new T1(0,function(_wx){return (E(_wx)==92)?E(new T1(0,_wr)):new T0(2);}),new T1(0,function(_wy){var _wz=E(_wy);if(E(_wz)==92){return E(_wp);}else{return new F(function(){return A1(_wh,new T2(0,_wz,_qm));});}}));});},_wA=function(_wB,_wC){var _wD=new T(function(){return B(A1(_wC,new T1(1,new T(function(){return B(A1(_wB,_v));}))));}),_wE=function(_wF){var _wG=E(_wF),_wH=E(_wG.a);if(E(_wH)==34){if(!E(_wG.b)){return E(_wD);}else{return new F(function(){return _wA(function(_wI){return new F(function(){return A1(_wB,new T2(1,_wH,_wI));});},_wC);});}}else{return new F(function(){return _wA(function(_wJ){return new F(function(){return A1(_wB,new T2(1,_wH,_wJ));});},_wC);});}};return new F(function(){return _wg(_wE);});},_wK=new T(function(){return B(unCStr("_\'"));}),_wL=function(_wM){var _wN=u_iswalnum(_wM);if(!E(_wN)){return new F(function(){return _qe(_mr,_wM,_wK);});}else{return true;}},_wO=function(_wP){return new F(function(){return _wL(E(_wP));});},_wQ=new T(function(){return B(unCStr(",;()[]{}`"));}),_wR=new T(function(){return B(unCStr("=>"));}),_wS=new T2(1,_wR,_v),_wT=new T(function(){return B(unCStr("~"));}),_wU=new T2(1,_wT,_wS),_wV=new T(function(){return B(unCStr("@"));}),_wW=new T2(1,_wV,_wU),_wX=new T(function(){return B(unCStr("->"));}),_wY=new T2(1,_wX,_wW),_wZ=new T(function(){return B(unCStr("<-"));}),_x0=new T2(1,_wZ,_wY),_x1=new T(function(){return B(unCStr("|"));}),_x2=new T2(1,_x1,_x0),_x3=new T(function(){return B(unCStr("\\"));}),_x4=new T2(1,_x3,_x2),_x5=new T(function(){return B(unCStr("="));}),_x6=new T2(1,_x5,_x4),_x7=new T(function(){return B(unCStr("::"));}),_x8=new T2(1,_x7,_x6),_x9=new T(function(){return B(unCStr(".."));}),_xa=new T2(1,_x9,_x8),_xb=function(_xc){var _xd=new T(function(){return B(A1(_xc,_nz));}),_xe=new T(function(){var _xf=new T(function(){var _xg=function(_xh){var _xi=new T(function(){return B(A1(_xc,new T1(0,_xh)));});return new T1(0,function(_xj){return (E(_xj)==39)?E(_xi):new T0(2);});};return B(_v2(_xg));}),_xk=function(_xl){var _xm=E(_xl);switch(E(_xm)){case 39:return new T0(2);case 92:return E(_xf);default:var _xn=new T(function(){return B(A1(_xc,new T1(0,_xm)));});return new T1(0,function(_xo){return (E(_xo)==39)?E(_xn):new T0(2);});}},_xp=new T(function(){var _xq=new T(function(){return B(_wA(_3j,_xc));}),_xr=new T(function(){var _xs=new T(function(){var _xt=new T(function(){var _xu=function(_xv){var _xw=E(_xv),_xx=u_iswalpha(_xw);return (E(_xx)==0)?(E(_xw)==95)?new T1(1,B(_nl(_wO,function(_xy){return new F(function(){return A1(_xc,new T1(3,new T2(1,_xw,_xy)));});}))):new T0(2):new T1(1,B(_nl(_wO,function(_xz){return new F(function(){return A1(_xc,new T1(3,new T2(1,_xw,_xz)));});})));};return B(_lH(new T1(0,_xu),new T(function(){return new T1(1,B(_mV(_ox,_qa,_xc)));})));}),_xA=function(_xB){return (!B(_qe(_mr,_xB,_qj)))?new T0(2):new T1(1,B(_nl(_qk,function(_xC){var _xD=new T2(1,_xB,_xC);if(!B(_qe(_mA,_xD,_xa))){return new F(function(){return A1(_xc,new T1(4,_xD));});}else{return new F(function(){return A1(_xc,new T1(2,_xD));});}})));};return B(_lH(new T1(0,_xA),_xt));});return B(_lH(new T1(0,function(_xE){if(!B(_qe(_mr,_xE,_wQ))){return new T0(2);}else{return new F(function(){return A1(_xc,new T1(2,new T2(1,_xE,_v)));});}}),_xs));});return B(_lH(new T1(0,function(_xF){return (E(_xF)==34)?E(_xq):new T0(2);}),_xr));});return B(_lH(new T1(0,function(_xG){return (E(_xG)==39)?E(new T1(0,_xk)):new T0(2);}),_xp));});return new F(function(){return _lH(new T1(1,function(_xH){return (E(_xH)._==0)?E(_xd):new T0(2);}),_xe);});},_xI=0,_xJ=function(_xK,_xL){var _xM=new T(function(){var _xN=new T(function(){var _xO=function(_xP){var _xQ=new T(function(){var _xR=new T(function(){return B(A1(_xL,_xP));});return B(_xb(function(_xS){var _xT=E(_xS);return (_xT._==2)?(!B(_cz(_xT.a,_mk)))?new T0(2):E(_xR):new T0(2);}));}),_xU=function(_xV){return E(_xQ);};return new T1(1,function(_xW){return new F(function(){return A2(_vS,_xW,_xU);});});};return B(A2(_xK,_xI,_xO));});return B(_xb(function(_xX){var _xY=E(_xX);return (_xY._==2)?(!B(_cz(_xY.a,_mj)))?new T0(2):E(_xN):new T0(2);}));}),_xZ=function(_y0){return E(_xM);};return function(_y1){return new F(function(){return A2(_vS,_y1,_xZ);});};},_y2=function(_y3,_y4){var _y5=function(_y6){var _y7=new T(function(){return B(A1(_y3,_y6));}),_y8=function(_y9){return new F(function(){return _lH(B(A1(_y7,_y9)),new T(function(){return new T1(1,B(_xJ(_y5,_y9)));}));});};return E(_y8);},_ya=new T(function(){return B(A1(_y3,_y4));}),_yb=function(_yc){return new F(function(){return _lH(B(A1(_ya,_yc)),new T(function(){return new T1(1,B(_xJ(_y5,_yc)));}));});};return E(_yb);},_yd=function(_ye,_yf){var _yg=function(_yh,_yi){var _yj=function(_yk){return new F(function(){return A1(_yi,new T(function(){return  -E(_yk);}));});},_yl=new T(function(){return B(_xb(function(_ym){return new F(function(){return A3(_ye,_ym,_yh,_yj);});}));}),_yn=function(_yo){return E(_yl);},_yp=function(_yq){return new F(function(){return A2(_vS,_yq,_yn);});},_yr=new T(function(){return B(_xb(function(_ys){var _yt=E(_ys);if(_yt._==4){var _yu=E(_yt.a);if(!_yu._){return new F(function(){return A3(_ye,_yt,_yh,_yi);});}else{if(E(_yu.a)==45){if(!E(_yu.b)._){return E(new T1(1,_yp));}else{return new F(function(){return A3(_ye,_yt,_yh,_yi);});}}else{return new F(function(){return A3(_ye,_yt,_yh,_yi);});}}}else{return new F(function(){return A3(_ye,_yt,_yh,_yi);});}}));}),_yv=function(_yw){return E(_yr);};return new T1(1,function(_yx){return new F(function(){return A2(_vS,_yx,_yv);});});};return new F(function(){return _y2(_yg,_yf);});},_yy=new T(function(){return 1/0;}),_yz=function(_yA,_yB){return new F(function(){return A1(_yB,_yy);});},_yC=new T(function(){return 0/0;}),_yD=function(_yE,_yF){return new F(function(){return A1(_yF,_yC);});},_yG=new T(function(){return B(unCStr("NaN"));}),_yH=new T(function(){return B(unCStr("Infinity"));}),_yI=1024,_yJ=-1021,_yK=new T(function(){return B(unCStr("base"));}),_yL=new T(function(){return B(unCStr("GHC.Exception"));}),_yM=new T(function(){return B(unCStr("ArithException"));}),_yN=new T5(0,new Long(4194982440,719304104,true),new Long(3110813675,1843557400,true),_yK,_yL,_yM),_yO=new T5(0,new Long(4194982440,719304104,true),new Long(3110813675,1843557400,true),_yN,_v,_v),_yP=function(_yQ){return E(_yO);},_yR=function(_yS){var _yT=E(_yS);return new F(function(){return _X(B(_V(_yT.a)),_yP,_yT.b);});},_yU=new T(function(){return B(unCStr("Ratio has zero denominator"));}),_yV=new T(function(){return B(unCStr("denormal"));}),_yW=new T(function(){return B(unCStr("divide by zero"));}),_yX=new T(function(){return B(unCStr("loss of precision"));}),_yY=new T(function(){return B(unCStr("arithmetic underflow"));}),_yZ=new T(function(){return B(unCStr("arithmetic overflow"));}),_z0=function(_z1,_z2){switch(E(_z1)){case 0:return new F(function(){return _3(_yZ,_z2);});break;case 1:return new F(function(){return _3(_yY,_z2);});break;case 2:return new F(function(){return _3(_yX,_z2);});break;case 3:return new F(function(){return _3(_yW,_z2);});break;case 4:return new F(function(){return _3(_yV,_z2);});break;default:return new F(function(){return _3(_yU,_z2);});}},_z3=function(_z4){return new F(function(){return _z0(_z4,_v);});},_z5=function(_z6,_z7,_z8){return new F(function(){return _z0(_z7,_z8);});},_z9=function(_za,_zb){return new F(function(){return _28(_z0,_za,_zb);});},_zc=new T3(0,_z5,_z3,_z9),_zd=new T(function(){return new T5(0,_yP,_zc,_ze,_yR,_z3);}),_ze=function(_da){return new T2(0,_zd,_da);},_zf=3,_zg=new T(function(){return B(_ze(_zf));}),_zh=new T(function(){return die(_zg);}),_zi=function(_zj,_zk){var _zl=E(_zj);if(!_zl._){var _zm=_zl.a,_zn=E(_zk);return (_zn._==0)?_zm==_zn.a:(I_compareInt(_zn.a,_zm)==0)?true:false;}else{var _zo=_zl.a,_zp=E(_zk);return (_zp._==0)?(I_compareInt(_zo,_zp.a)==0)?true:false:(I_compare(_zo,_zp.a)==0)?true:false;}},_zq=new T1(0,0),_zr=function(_zs,_zt){while(1){var _zu=E(_zs);if(!_zu._){var _zv=E(_zu.a);if(_zv==(-2147483648)){_zs=new T1(1,I_fromInt(-2147483648));continue;}else{var _zw=E(_zt);if(!_zw._){return new T1(0,_zv%_zw.a);}else{_zs=new T1(1,I_fromInt(_zv));_zt=_zw;continue;}}}else{var _zx=_zu.a,_zy=E(_zt);return (_zy._==0)?new T1(1,I_rem(_zx,I_fromInt(_zy.a))):new T1(1,I_rem(_zx,_zy.a));}}},_zz=function(_zA,_zB){if(!B(_zi(_zB,_zq))){return new F(function(){return _zr(_zA,_zB);});}else{return E(_zh);}},_zC=function(_zD,_zE){while(1){if(!B(_zi(_zE,_zq))){var _zF=_zE,_zG=B(_zz(_zD,_zE));_zD=_zF;_zE=_zG;continue;}else{return E(_zD);}}},_zH=function(_zI){var _zJ=E(_zI);if(!_zJ._){var _zK=E(_zJ.a);return (_zK==(-2147483648))?E(_oP):(_zK<0)?new T1(0, -_zK):E(_zJ);}else{var _zL=_zJ.a;return (I_compareInt(_zL,0)>=0)?E(_zJ):new T1(1,I_negate(_zL));}},_zM=function(_zN,_zO){while(1){var _zP=E(_zN);if(!_zP._){var _zQ=E(_zP.a);if(_zQ==(-2147483648)){_zN=new T1(1,I_fromInt(-2147483648));continue;}else{var _zR=E(_zO);if(!_zR._){return new T1(0,quot(_zQ,_zR.a));}else{_zN=new T1(1,I_fromInt(_zQ));_zO=_zR;continue;}}}else{var _zS=_zP.a,_zT=E(_zO);return (_zT._==0)?new T1(0,I_toInt(I_quot(_zS,I_fromInt(_zT.a)))):new T1(1,I_quot(_zS,_zT.a));}}},_zU=5,_zV=new T(function(){return B(_ze(_zU));}),_zW=new T(function(){return die(_zV);}),_zX=function(_zY,_zZ){if(!B(_zi(_zZ,_zq))){var _A0=B(_zC(B(_zH(_zY)),B(_zH(_zZ))));return (!B(_zi(_A0,_zq)))?new T2(0,B(_zM(_zY,_A0)),B(_zM(_zZ,_A0))):E(_zh);}else{return E(_zW);}},_A1=new T1(0,1),_A2=new T(function(){return B(unCStr("Negative exponent"));}),_A3=new T(function(){return B(err(_A2));}),_A4=new T1(0,2),_A5=new T(function(){return B(_zi(_A4,_zq));}),_A6=function(_A7,_A8){while(1){var _A9=E(_A7);if(!_A9._){var _Aa=_A9.a,_Ab=E(_A8);if(!_Ab._){var _Ac=_Ab.a,_Ad=subC(_Aa,_Ac);if(!E(_Ad.b)){return new T1(0,_Ad.a);}else{_A7=new T1(1,I_fromInt(_Aa));_A8=new T1(1,I_fromInt(_Ac));continue;}}else{_A7=new T1(1,I_fromInt(_Aa));_A8=_Ab;continue;}}else{var _Ae=E(_A8);if(!_Ae._){_A7=_A9;_A8=new T1(1,I_fromInt(_Ae.a));continue;}else{return new T1(1,I_sub(_A9.a,_Ae.a));}}}},_Af=function(_Ag,_Ah,_Ai){while(1){if(!E(_A5)){if(!B(_zi(B(_zr(_Ah,_A4)),_zq))){if(!B(_zi(_Ah,_A1))){var _Aj=B(_p8(_Ag,_Ag)),_Ak=B(_zM(B(_A6(_Ah,_A1)),_A4)),_Al=B(_p8(_Ag,_Ai));_Ag=_Aj;_Ah=_Ak;_Ai=_Al;continue;}else{return new F(function(){return _p8(_Ag,_Ai);});}}else{var _Aj=B(_p8(_Ag,_Ag)),_Ak=B(_zM(_Ah,_A4));_Ag=_Aj;_Ah=_Ak;continue;}}else{return E(_zh);}}},_Am=function(_An,_Ao){while(1){if(!E(_A5)){if(!B(_zi(B(_zr(_Ao,_A4)),_zq))){if(!B(_zi(_Ao,_A1))){return new F(function(){return _Af(B(_p8(_An,_An)),B(_zM(B(_A6(_Ao,_A1)),_A4)),_An);});}else{return E(_An);}}else{var _Ap=B(_p8(_An,_An)),_Aq=B(_zM(_Ao,_A4));_An=_Ap;_Ao=_Aq;continue;}}else{return E(_zh);}}},_Ar=function(_As,_At){var _Au=E(_As);if(!_Au._){var _Av=_Au.a,_Aw=E(_At);return (_Aw._==0)?_Av<_Aw.a:I_compareInt(_Aw.a,_Av)>0;}else{var _Ax=_Au.a,_Ay=E(_At);return (_Ay._==0)?I_compareInt(_Ax,_Ay.a)<0:I_compare(_Ax,_Ay.a)<0;}},_Az=function(_AA,_AB){if(!B(_Ar(_AB,_zq))){if(!B(_zi(_AB,_zq))){return new F(function(){return _Am(_AA,_AB);});}else{return E(_A1);}}else{return E(_A3);}},_AC=new T1(0,1),_AD=new T1(0,0),_AE=new T1(0,-1),_AF=function(_AG){var _AH=E(_AG);if(!_AH._){var _AI=_AH.a;return (_AI>=0)?(E(_AI)==0)?E(_AD):E(_oE):E(_AE);}else{var _AJ=I_compareInt(_AH.a,0);return (_AJ<=0)?(E(_AJ)==0)?E(_AD):E(_AE):E(_oE);}},_AK=function(_AL,_AM,_AN){while(1){var _AO=E(_AN);if(!_AO._){if(!B(_Ar(_AL,_pl))){return new T2(0,B(_p8(_AM,B(_Az(_oU,_AL)))),_A1);}else{var _AP=B(_Az(_oU,B(_oQ(_AL))));return new F(function(){return _zX(B(_p8(_AM,B(_AF(_AP)))),B(_zH(_AP)));});}}else{var _AQ=B(_A6(_AL,_AC)),_AR=B(_oG(B(_p8(_AM,_oU)),B(_kX(E(_AO.a)))));_AL=_AQ;_AM=_AR;_AN=_AO.b;continue;}}},_AS=function(_AT,_AU){var _AV=E(_AT);if(!_AV._){var _AW=_AV.a,_AX=E(_AU);return (_AX._==0)?_AW>=_AX.a:I_compareInt(_AX.a,_AW)<=0;}else{var _AY=_AV.a,_AZ=E(_AU);return (_AZ._==0)?I_compareInt(_AY,_AZ.a)>=0:I_compare(_AY,_AZ.a)>=0;}},_B0=function(_B1){var _B2=E(_B1);if(!_B2._){var _B3=_B2.b;return new F(function(){return _zX(B(_p8(B(_pm(new T(function(){return B(_kX(E(_B2.a)));}),new T(function(){return B(_oV(_B3,0));},1),B(_p0(_p4,_B3)))),_AC)),_AC);});}else{var _B4=_B2.a,_B5=_B2.c,_B6=E(_B2.b);if(!_B6._){var _B7=E(_B5);if(!_B7._){return new F(function(){return _zX(B(_p8(B(_pD(_oU,_B4)),_AC)),_AC);});}else{var _B8=_B7.a;if(!B(_AS(_B8,_pl))){var _B9=B(_Az(_oU,B(_oQ(_B8))));return new F(function(){return _zX(B(_p8(B(_pD(_oU,_B4)),B(_AF(_B9)))),B(_zH(_B9)));});}else{return new F(function(){return _zX(B(_p8(B(_p8(B(_pD(_oU,_B4)),B(_Az(_oU,_B8)))),_AC)),_AC);});}}}else{var _Ba=_B6.a,_Bb=E(_B5);if(!_Bb._){return new F(function(){return _AK(_pl,B(_pD(_oU,_B4)),_Ba);});}else{return new F(function(){return _AK(_Bb.a,B(_pD(_oU,_B4)),_Ba);});}}}},_Bc=function(_Bd,_Be){while(1){var _Bf=E(_Be);if(!_Bf._){return __Z;}else{if(!B(A1(_Bd,_Bf.a))){return E(_Bf);}else{_Be=_Bf.b;continue;}}}},_Bg=function(_Bh,_Bi){var _Bj=E(_Bh);if(!_Bj._){var _Bk=_Bj.a,_Bl=E(_Bi);return (_Bl._==0)?_Bk>_Bl.a:I_compareInt(_Bl.a,_Bk)<0;}else{var _Bm=_Bj.a,_Bn=E(_Bi);return (_Bn._==0)?I_compareInt(_Bm,_Bn.a)>0:I_compare(_Bm,_Bn.a)>0;}},_Bo=0,_Bp=function(_Bq,_Br){return E(_Bq)==E(_Br);},_Bs=function(_Bt){return new F(function(){return _Bp(_Bo,_Bt);});},_Bu=new T2(0,E(_pl),E(_A1)),_Bv=new T1(1,_Bu),_Bw=new T1(0,-2147483648),_Bx=new T1(0,2147483647),_By=function(_Bz,_BA,_BB){var _BC=E(_BB);if(!_BC._){return new T1(1,new T(function(){var _BD=B(_B0(_BC));return new T2(0,E(_BD.a),E(_BD.b));}));}else{var _BE=E(_BC.c);if(!_BE._){return new T1(1,new T(function(){var _BF=B(_B0(_BC));return new T2(0,E(_BF.a),E(_BF.b));}));}else{var _BG=_BE.a;if(!B(_Bg(_BG,_Bx))){if(!B(_Ar(_BG,_Bw))){var _BH=function(_BI){var _BJ=_BI+B(_qz(_BG))|0;return (_BJ<=(E(_BA)+3|0))?(_BJ>=(E(_Bz)-3|0))?new T1(1,new T(function(){var _BK=B(_B0(_BC));return new T2(0,E(_BK.a),E(_BK.b));})):E(_Bv):__Z;},_BL=B(_Bc(_Bs,_BC.a));if(!_BL._){var _BM=E(_BC.b);if(!_BM._){return E(_Bv);}else{var _BN=B(_db(_Bs,_BM.a));if(!E(_BN.b)._){return E(_Bv);}else{return new F(function(){return _BH( -B(_oV(_BN.a,0)));});}}}else{return new F(function(){return _BH(B(_oV(_BL,0)));});}}else{return __Z;}}else{return __Z;}}}},_BO=function(_BP,_BQ){return new T0(2);},_BR=new T1(0,1),_BS=function(_BT,_BU){var _BV=E(_BT);if(!_BV._){var _BW=_BV.a,_BX=E(_BU);if(!_BX._){var _BY=_BX.a;return (_BW!=_BY)?(_BW>_BY)?2:0:1;}else{var _BZ=I_compareInt(_BX.a,_BW);return (_BZ<=0)?(_BZ>=0)?1:2:0;}}else{var _C0=_BV.a,_C1=E(_BU);if(!_C1._){var _C2=I_compareInt(_C0,_C1.a);return (_C2>=0)?(_C2<=0)?1:2:0;}else{var _C3=I_compare(_C0,_C1.a);return (_C3>=0)?(_C3<=0)?1:2:0;}}},_C4=function(_C5,_C6){var _C7=E(_C5);return (_C7._==0)?_C7.a*Math.pow(2,_C6):I_toNumber(_C7.a)*Math.pow(2,_C6);},_C8=function(_C9,_Ca){while(1){var _Cb=E(_C9);if(!_Cb._){var _Cc=E(_Cb.a);if(_Cc==(-2147483648)){_C9=new T1(1,I_fromInt(-2147483648));continue;}else{var _Cd=E(_Ca);if(!_Cd._){var _Ce=_Cd.a;return new T2(0,new T1(0,quot(_Cc,_Ce)),new T1(0,_Cc%_Ce));}else{_C9=new T1(1,I_fromInt(_Cc));_Ca=_Cd;continue;}}}else{var _Cf=E(_Ca);if(!_Cf._){_C9=_Cb;_Ca=new T1(1,I_fromInt(_Cf.a));continue;}else{var _Cg=I_quotRem(_Cb.a,_Cf.a);return new T2(0,new T1(1,_Cg.a),new T1(1,_Cg.b));}}}},_Ch=new T1(0,0),_Ci=function(_Cj,_Ck,_Cl){if(!B(_zi(_Cl,_Ch))){var _Cm=B(_C8(_Ck,_Cl)),_Cn=_Cm.a;switch(B(_BS(B(_ld(_Cm.b,1)),_Cl))){case 0:return new F(function(){return _C4(_Cn,_Cj);});break;case 1:if(!(B(_qz(_Cn))&1)){return new F(function(){return _C4(_Cn,_Cj);});}else{return new F(function(){return _C4(B(_oG(_Cn,_BR)),_Cj);});}break;default:return new F(function(){return _C4(B(_oG(_Cn,_BR)),_Cj);});}}else{return E(_zh);}},_Co=function(_Cp){var _Cq=function(_Cr,_Cs){while(1){if(!B(_Ar(_Cr,_Cp))){if(!B(_Bg(_Cr,_Cp))){if(!B(_zi(_Cr,_Cp))){return new F(function(){return _dx("GHC/Integer/Type.lhs:(553,5)-(555,32)|function l2");});}else{return E(_Cs);}}else{return _Cs-1|0;}}else{var _Ct=B(_ld(_Cr,1)),_Cu=_Cs+1|0;_Cr=_Ct;_Cs=_Cu;continue;}}};return new F(function(){return _Cq(_oE,0);});},_Cv=function(_Cw){var _Cx=E(_Cw);if(!_Cx._){var _Cy=_Cx.a>>>0;if(!_Cy){return -1;}else{var _Cz=function(_CA,_CB){while(1){if(_CA>=_Cy){if(_CA<=_Cy){if(_CA!=_Cy){return new F(function(){return _dx("GHC/Integer/Type.lhs:(609,5)-(611,40)|function l2");});}else{return E(_CB);}}else{return _CB-1|0;}}else{var _CC=imul(_CA,2)>>>0,_CD=_CB+1|0;_CA=_CC;_CB=_CD;continue;}}};return new F(function(){return _Cz(1,0);});}}else{return new F(function(){return _Co(_Cx);});}},_CE=function(_CF){var _CG=E(_CF);if(!_CG._){var _CH=_CG.a>>>0;if(!_CH){return new T2(0,-1,0);}else{var _CI=function(_CJ,_CK){while(1){if(_CJ>=_CH){if(_CJ<=_CH){if(_CJ!=_CH){return new F(function(){return _dx("GHC/Integer/Type.lhs:(609,5)-(611,40)|function l2");});}else{return E(_CK);}}else{return _CK-1|0;}}else{var _CL=imul(_CJ,2)>>>0,_CM=_CK+1|0;_CJ=_CL;_CK=_CM;continue;}}};return new T2(0,B(_CI(1,0)),(_CH&_CH-1>>>0)>>>0&4294967295);}}else{var _CN=_CG.a;return new T2(0,B(_Cv(_CG)),I_compareInt(I_and(_CN,I_sub(_CN,I_fromInt(1))),0));}},_CO=function(_CP,_CQ){while(1){var _CR=E(_CP);if(!_CR._){var _CS=_CR.a,_CT=E(_CQ);if(!_CT._){return new T1(0,(_CS>>>0&_CT.a>>>0)>>>0&4294967295);}else{_CP=new T1(1,I_fromInt(_CS));_CQ=_CT;continue;}}else{var _CU=E(_CQ);if(!_CU._){_CP=_CR;_CQ=new T1(1,I_fromInt(_CU.a));continue;}else{return new T1(1,I_and(_CR.a,_CU.a));}}}},_CV=new T1(0,2),_CW=function(_CX,_CY){var _CZ=E(_CX);if(!_CZ._){var _D0=(_CZ.a>>>0&(2<<_CY>>>0)-1>>>0)>>>0,_D1=1<<_CY>>>0;return (_D1<=_D0)?(_D1>=_D0)?1:2:0;}else{var _D2=B(_CO(_CZ,B(_A6(B(_ld(_CV,_CY)),_oE)))),_D3=B(_ld(_oE,_CY));return (!B(_Bg(_D3,_D2)))?(!B(_Ar(_D3,_D2)))?1:2:0;}},_D4=function(_D5,_D6){while(1){var _D7=E(_D5);if(!_D7._){_D5=new T1(1,I_fromInt(_D7.a));continue;}else{return new T1(1,I_shiftRight(_D7.a,_D6));}}},_D8=function(_D9,_Da,_Db,_Dc){var _Dd=B(_CE(_Dc)),_De=_Dd.a;if(!E(_Dd.b)){var _Df=B(_Cv(_Db));if(_Df<((_De+_D9|0)-1|0)){var _Dg=_De+(_D9-_Da|0)|0;if(_Dg>0){if(_Dg>_Df){if(_Dg<=(_Df+1|0)){if(!E(B(_CE(_Db)).b)){return 0;}else{return new F(function(){return _C4(_BR,_D9-_Da|0);});}}else{return 0;}}else{var _Dh=B(_D4(_Db,_Dg));switch(B(_CW(_Db,_Dg-1|0))){case 0:return new F(function(){return _C4(_Dh,_D9-_Da|0);});break;case 1:if(!(B(_qz(_Dh))&1)){return new F(function(){return _C4(_Dh,_D9-_Da|0);});}else{return new F(function(){return _C4(B(_oG(_Dh,_BR)),_D9-_Da|0);});}break;default:return new F(function(){return _C4(B(_oG(_Dh,_BR)),_D9-_Da|0);});}}}else{return new F(function(){return _C4(_Db,(_D9-_Da|0)-_Dg|0);});}}else{if(_Df>=_Da){var _Di=B(_D4(_Db,(_Df+1|0)-_Da|0));switch(B(_CW(_Db,_Df-_Da|0))){case 0:return new F(function(){return _C4(_Di,((_Df-_De|0)+1|0)-_Da|0);});break;case 2:return new F(function(){return _C4(B(_oG(_Di,_BR)),((_Df-_De|0)+1|0)-_Da|0);});break;default:if(!(B(_qz(_Di))&1)){return new F(function(){return _C4(_Di,((_Df-_De|0)+1|0)-_Da|0);});}else{return new F(function(){return _C4(B(_oG(_Di,_BR)),((_Df-_De|0)+1|0)-_Da|0);});}}}else{return new F(function(){return _C4(_Db, -_De);});}}}else{var _Dj=B(_Cv(_Db))-_De|0,_Dk=function(_Dl){var _Dm=function(_Dn,_Do){if(!B(_qC(B(_ld(_Do,_Da)),_Dn))){return new F(function(){return _Ci(_Dl-_Da|0,_Dn,_Do);});}else{return new F(function(){return _Ci((_Dl-_Da|0)+1|0,_Dn,B(_ld(_Do,1)));});}};if(_Dl>=_Da){if(_Dl!=_Da){return new F(function(){return _Dm(_Db,new T(function(){return B(_ld(_Dc,_Dl-_Da|0));}));});}else{return new F(function(){return _Dm(_Db,_Dc);});}}else{return new F(function(){return _Dm(new T(function(){return B(_ld(_Db,_Da-_Dl|0));}),_Dc);});}};if(_D9>_Dj){return new F(function(){return _Dk(_D9);});}else{return new F(function(){return _Dk(_Dj);});}}},_Dp=new T(function(){return 0/0;}),_Dq=new T(function(){return -1/0;}),_Dr=new T(function(){return 1/0;}),_Ds=0,_Dt=function(_Du,_Dv){if(!B(_zi(_Dv,_Ch))){if(!B(_zi(_Du,_Ch))){if(!B(_Ar(_Du,_Ch))){return new F(function(){return _D8(-1021,53,_Du,_Dv);});}else{return  -B(_D8(-1021,53,B(_oQ(_Du)),_Dv));}}else{return E(_Ds);}}else{return (!B(_zi(_Du,_Ch)))?(!B(_Ar(_Du,_Ch)))?E(_Dr):E(_Dq):E(_Dp);}},_Dw=function(_Dx){var _Dy=E(_Dx);switch(_Dy._){case 3:var _Dz=_Dy.a;return (!B(_cz(_Dz,_yH)))?(!B(_cz(_Dz,_yG)))?E(_BO):E(_yD):E(_yz);case 5:var _DA=B(_By(_yJ,_yI,_Dy.a));if(!_DA._){return E(_yz);}else{var _DB=new T(function(){var _DC=E(_DA.a);return B(_Dt(_DC.a,_DC.b));});return function(_DD,_DE){return new F(function(){return A1(_DE,_DB);});};}break;default:return E(_BO);}},_DF=function(_DG){var _DH=function(_DI){return E(new T2(3,_DG,_mM));};return new T1(1,function(_DJ){return new F(function(){return A2(_vS,_DJ,_DH);});});},_DK=new T(function(){return B(A3(_yd,_Dw,_xI,_DF));}),_DL=function(_DM){while(1){var _DN=B((function(_DO){var _DP=E(_DO);if(!_DP._){return __Z;}else{var _DQ=_DP.b,_DR=E(_DP.a);if(!E(_DR.b)._){return new T2(1,_DR.a,new T(function(){return B(_DL(_DQ));}));}else{_DM=_DQ;return __continue;}}})(_DM));if(_DN!=__continue){return _DN;}}},_DS=function(_DT,_DU,_DV,_DW,_DX,_DY,_DZ,_E0,_E1,_E2,_E3,_E4,_){var _E5=new T(function(){var _E6=B(_DL(B(_lx(_DK,_E3))));if(!_E6._){return E(_lt);}else{if(!E(_E6.b)._){return E(_E6.a);}else{return E(_lv);}}}),_E7=new T(function(){var _E8=B(_DL(B(_lx(_DK,_E4))));if(!_E8._){return E(_lt);}else{if(!E(_E8.b)._){return E(_E8.a);}else{return E(_lv);}}}),_E9=new T(function(){var _Ea=B(_kf(new T(function(){var _Eb=B(_DL(B(_lx(_DK,_DT))));if(!_Eb._){return E(_lt);}else{if(!E(_Eb.b)._){return E(_Eb.a);}else{return E(_lv);}}}),new T(function(){var _Ec=B(_DL(B(_lx(_DK,_DU))));if(!_Ec._){return E(_lt);}else{if(!E(_Ec.b)._){return E(_Ec.a);}else{return E(_lv);}}}),new T(function(){var _Ed=B(_DL(B(_lx(_DK,_DV))));if(!_Ed._){return E(_lt);}else{if(!E(_Ed.b)._){return E(_Ed.a);}else{return E(_lv);}}}),new T(function(){var _Ee=B(_DL(B(_lx(_DK,_DW))));if(!_Ee._){return E(_lt);}else{if(!E(_Ee.b)._){return E(_Ee.a);}else{return E(_lv);}}}),new T(function(){var _Ef=B(_DL(B(_lx(_DK,_DX))));if(!_Ef._){return E(_lt);}else{if(!E(_Ef.b)._){return E(_Ef.a);}else{return E(_lv);}}}),new T(function(){var _Eg=B(_DL(B(_lx(_DK,_DY))));if(!_Eg._){return E(_lt);}else{if(!E(_Eg.b)._){return E(_Eg.a);}else{return E(_lv);}}}),new T(function(){var _Eh=B(_DL(B(_lx(_DK,_DZ))));if(!_Eh._){return E(_lt);}else{if(!E(_Eh.b)._){return E(_Eh.a);}else{return E(_lv);}}}),new T(function(){var _Ei=B(_DL(B(_lx(_DK,_E0))));if(!_Ei._){return E(_lt);}else{if(!E(_Ei.b)._){return E(_Ei.a);}else{return E(_lv);}}}),new T(function(){var _Ej=B(_DL(B(_lx(_DK,_E1))));if(!_Ej._){return E(_lt);}else{if(!E(_Ej.b)._){return E(_Ej.a);}else{return E(_lv);}}}),new T(function(){var _Ek=B(_DL(B(_lx(_DK,_E2))));if(!_Ek._){return E(_lt);}else{if(!E(_Ek.b)._){return E(_Ek.a);}else{return E(_lv);}}}),_E5,_E7));return new T3(0,_Ea.a,_Ea.b,_Ea.c);});return new T2(0,new T(function(){var _El=E(E(_E9).a),_Em=E(_E5)*Math.sin(E(_E7)*3.141592653589793/180);return B(_lh(Math.sqrt(_Em*_Em+_El*_El),2));}),new T(function(){return B(_lh(E(E(_E9).c),2));}));},_En=new T(function(){return eval("(function(e){return e.getContext(\'2d\');})");}),_Eo=new T(function(){return eval("(function(e){return !!e.getContext;})");}),_Ep=new T(function(){return eval("(function(e){e.width = e.width;})");}),_Eq=new T(function(){return B(unCStr("height"));}),_Er=new T(function(){return B(unCStr("width"));}),_Es=new T(function(){return B(unCStr("canvas"));}),_Et=function(_Eu,_Ev,_){var _Ew=__app1(E(_4m),toJSStr(E(_Es))),_Ex=jsShow(E(_Eu)),_Ey=E(_5o),_Ez=__app3(_Ey,_Ew,toJSStr(E(_Er)),toJSStr(fromJSStr(_Ex))),_EA=jsShow(E(_Ev)),_EB=__app3(_Ey,_Ew,toJSStr(E(_Eq)),toJSStr(fromJSStr(_EA)));return _Ew;},_EC=new T(function(){return new T2(0,E(new T1(1,"backgroundColor")),toJSStr(E(_9j)));}),_ED=new T2(1,_EC,_v),_EE=new T(function(){return new T2(0,E(new T1(1,"margin")),"0px auto 0 auto");}),_EF=new T2(1,_EE,_ED),_EG=new T(function(){return new T2(0,E(new T1(1,"border")),"1px solid black");}),_EH=new T2(1,_EG,_EF),_EI=new T(function(){return new T2(0,E(new T1(1,"display")),"block");}),_EJ=new T2(1,_EI,_EH),_EK=new T(function(){return B(_dx("Client/Panels/GraphPanel.hs:(43,1)-(48,23)|function drawGraph"));}),_EL=new T(function(){return B(unCStr("Pattern match failure in do expression at Client/Panels/GraphPanel.hs:64:3-13"));}),_EM=new T6(0,_2q,_2r,_v,_EL,_2q,_2q),_EN=new T(function(){return B(_2o(_EM));}),_EO=new T(function(){return eval("(function(ctx, x, y, radius, fromAngle, toAngle){ctx.arc(x, y, radius, fromAngle, toAngle);})");}),_EP=0,_EQ=6.283185307179586,_ER=new T(function(){return eval("(function(ctx,x,y){ctx.moveTo(x,y);})");}),_ES=function(_ET,_EU,_EV,_EW,_){var _EX=__app3(E(_ER),_EW,_ET+_EV,_EU),_EY=__apply(E(_EO),new T2(1,_EQ,new T2(1,_EP,new T2(1,_EV,new T2(1,_EU,new T2(1,_ET,new T2(1,_EW,_v)))))));return new F(function(){return _3g(_);});},_EZ=new T(function(){return eval("(function(ctx){ctx.beginPath();})");}),_F0=new T(function(){return eval("(function(ctx){ctx.fill();})");}),_F1=function(_F2,_F3,_){var _F4=__app1(E(_EZ),_F3),_F5=B(A2(_F2,_F3,_)),_F6=__app1(E(_F0),_F3);return new F(function(){return _3g(_);});},_F7=",",_F8="rgba(",_F9=new T(function(){return toJSStr(_v);}),_Fa="rgb(",_Fb=")",_Fc=new T2(1,_Fb,_v),_Fd=function(_Fe){var _Ff=E(_Fe);if(!_Ff._){var _Fg=jsCat(new T2(1,_Fa,new T2(1,new T(function(){return String(_Ff.a);}),new T2(1,_F7,new T2(1,new T(function(){return String(_Ff.b);}),new T2(1,_F7,new T2(1,new T(function(){return String(_Ff.c);}),_Fc)))))),E(_F9));return E(_Fg);}else{var _Fh=jsCat(new T2(1,_F8,new T2(1,new T(function(){return String(_Ff.a);}),new T2(1,_F7,new T2(1,new T(function(){return String(_Ff.b);}),new T2(1,_F7,new T2(1,new T(function(){return String(_Ff.c);}),new T2(1,_F7,new T2(1,new T(function(){return String(_Ff.d);}),_Fc)))))))),E(_F9));return E(_Fh);}},_Fi="strokeStyle",_Fj="fillStyle",_Fk=function(_Fl,_Fm){var _Fn=new T(function(){return B(_Fd(_Fl));});return function(_Fo,_){var _Fp=E(_Fo),_Fq=E(_Fj),_Fr=E(_4Y),_Fs=__app2(_Fr,_Fp,_Fq),_Ft=E(_Fi),_Fu=__app2(_Fr,_Fp,_Ft),_Fv=E(_Fn),_Fw=E(_5o),_Fx=__app3(_Fw,_Fp,_Fq,_Fv),_Fy=__app3(_Fw,_Fp,_Ft,_Fv),_Fz=B(A2(_Fm,_Fp,_)),_FA=String(_Fs),_FB=__app3(_Fw,_Fp,_Fq,_FA),_FC=String(_Fu),_FD=__app3(_Fw,_Fp,_Ft,_FC);return new F(function(){return _3g(_);});};},_FE=new T3(0,255,255,255),_FF=function(_FG,_FH){var _FI=new T(function(){var _FJ=E(_FG);return new T2(0,new T(function(){return 6*E(_FJ.a);}),new T(function(){return 6*E(_FJ.b);}));}),_FK=new T(function(){return E(_FH)-E(E(_FI).b);}),_FL=function(_FM,_){return new F(function(){return _ES(E(E(_FI).a),E(_FK),2,E(_FM),_);});};return new F(function(){return _Fk(_FE,function(_FN,_){return new F(function(){return _F1(_FL,E(_FN),_);});});});},_FO=function(_FP,_FQ,_FR){var _FS=E(_FQ);if(!_FS._){return new F(function(){return _FF(_FP,_FR);});}else{var _FT=new T(function(){return B(_FF(_FP,_FR));}),_FU=new T(function(){return B(_FO(_FS.a,_FS.b,_FR));});return function(_FV,_){var _FW=B(A2(_FT,_FV,_));return new F(function(){return A2(_FU,_FV,_);});};}},_FX=800,_FY=500,_FZ=1000,_G0=function(_G1,_G2,_){var _G3=new T(function(){var _G4=E(_G1);if(!_G4._){return E(_9S);}else{if(E(E(_G4.a).b)>=83){return E(_FX);}else{return E(_FY);}}}),_G5=B(A(_7p,[_3l,_43,function(_){return new F(function(){return _Et(_FZ,_G3,_);});},_EJ,_])),_G6=E(_G5),_G7=__app1(E(_Eo),_G6);if(!_G7){return new F(function(){return die(_EN);});}else{var _G8=__app1(E(_En),_G6),_G9=__app1(E(_Ep),_G6),_Ga=new T(function(){var _Gb=E(_G1);if(!_Gb._){return E(_9S);}else{if(E(E(_Gb.a).b)>=83){return E(_FX);}else{return E(_FY);}}}),_Gc=E(_G1);if(!_Gc._){return E(_EK);}else{var _Gd=_Gc.a,_Ge=E(_Gc.b);if(!_Ge._){var _Gf=B(A(_FF,[_Gd,_Ga,_G8,_])),_Gg=E(_G2),_Gh=__app2(E(_4a),_G6,_Gg);return _Gg;}else{var _Gi=B(A(_FF,[_Gd,_Ga,_G8,_])),_Gj=B(A(_FO,[_Ge.a,_Ge.b,_Ga,_G8,_])),_Gk=E(_G2),_Gl=__app2(E(_4a),_G6,_Gk);return _Gk;}}}},_Gm=function(_Gn){var _Go=E(_Gn);if(!_Go._){return new T2(0,_v,_v);}else{var _Gp=E(_Go.a),_Gq=new T(function(){var _Gr=B(_Gm(_Go.b));return new T2(0,_Gr.a,_Gr.b);});return new T2(0,new T2(1,_Gp.a,new T(function(){return E(E(_Gq).a);})),new T2(1,_Gp.b,new T(function(){return E(E(_Gq).b);})));}},_Gs=function(_Gt){var _Gu=E(_Gt);if(!_Gu._){return new T2(0,_v,_v);}else{var _Gv=E(_Gu.a),_Gw=new T(function(){var _Gx=B(_Gs(_Gu.b));return new T2(0,_Gx.a,_Gx.b);});return new T2(0,new T2(1,_Gv.a,new T(function(){return E(E(_Gw).a);})),new T2(1,_Gv.b,new T(function(){return E(E(_Gw).b);})));}},_Gy=function(_Gz,_GA){var _GB=E(_Gz);if(!_GB._){return __Z;}else{var _GC=E(_GA);return (_GC._==0)?__Z:new T2(1,new T2(0,_GB.a,_GC.a),new T(function(){return B(_Gy(_GB.b,_GC.b));}));}},_GD=function(_GE,_GF,_GG,_GH,_GI,_GJ,_GK,_GL,_GM,_GN,_GO,_GP){var _GQ=B(_ia(_GE,_GF,_GH,_GI,_GJ,_GK,_GL,_GM,_GN,_GO,_GP));return new F(function(){return _Gy(B(_jB(_v,B(_Gm(_GQ)).b,_iA,_GG,_GE,_GH,_GI,_GJ,_GK,new T(function(){return E(_GL)*E(_GN)/2;}),new T(function(){return E(_GL)*E(_GM)/2;}))),new T(function(){return E(B(_Gs(_GQ)).a);},1));});},_GR=function(_GS,_GT,_GU,_GV,_GW,_GX,_GY,_GZ,_H0,_H1,_H2,_H3,_){return new T(function(){var _H4=B(_DL(B(_lx(_DK,_H2))));if(!_H4._){return E(_lt);}else{if(!E(_H4.b)._){var _H5=B(_DL(B(_lx(_DK,_H3))));if(!_H5._){return E(_lt);}else{if(!E(_H5.b)._){return B(_GD(new T(function(){var _H6=B(_DL(B(_lx(_DK,_GS))));if(!_H6._){return E(_lt);}else{if(!E(_H6.b)._){return E(_H6.a);}else{return E(_lv);}}}),new T(function(){var _H7=B(_DL(B(_lx(_DK,_GT))));if(!_H7._){return E(_lt);}else{if(!E(_H7.b)._){return E(_H7.a);}else{return E(_lv);}}}),new T(function(){var _H8=B(_DL(B(_lx(_DK,_GU))));if(!_H8._){return E(_lt);}else{if(!E(_H8.b)._){return E(_H8.a);}else{return E(_lv);}}}),new T(function(){var _H9=B(_DL(B(_lx(_DK,_GV))));if(!_H9._){return E(_lt);}else{if(!E(_H9.b)._){return E(_H9.a);}else{return E(_lv);}}}),new T(function(){var _Ha=B(_DL(B(_lx(_DK,_GW))));if(!_Ha._){return E(_lt);}else{if(!E(_Ha.b)._){return E(_Ha.a);}else{return E(_lv);}}}),new T(function(){var _Hb=B(_DL(B(_lx(_DK,_GX))));if(!_Hb._){return E(_lt);}else{if(!E(_Hb.b)._){return E(_Hb.a);}else{return E(_lv);}}}),new T(function(){var _Hc=B(_DL(B(_lx(_DK,_GY))));if(!_Hc._){return E(_lt);}else{if(!E(_Hc.b)._){return E(_Hc.a);}else{return E(_lv);}}}),new T(function(){var _Hd=B(_DL(B(_lx(_DK,_GZ))));if(!_Hd._){return E(_lt);}else{if(!E(_Hd.b)._){return E(_Hd.a);}else{return E(_lv);}}}),new T(function(){var _He=B(_DL(B(_lx(_DK,_H0))));if(!_He._){return E(_lt);}else{if(!E(_He.b)._){return E(_He.a);}else{return E(_lv);}}}),new T(function(){var _Hf=B(_DL(B(_lx(_DK,_H1))));if(!_Hf._){return E(_lt);}else{if(!E(_Hf.b)._){return E(_Hf.a);}else{return E(_lv);}}}),E(_H4.a),E(_H5.a)));}else{return E(_lv);}}}else{return E(_lv);}}});},_Hg=new T(function(){return eval("(function(e) {e.focus();})");}),_Hh=new T(function(){return B(unCStr("foldr1"));}),_Hi=new T(function(){return B(_9P(_Hh));}),_Hj=function(_Hk,_Hl){var _Hm=E(_Hl);if(!_Hm._){return E(_Hi);}else{var _Hn=_Hm.a,_Ho=E(_Hm.b);if(!_Ho._){return E(_Hn);}else{return new F(function(){return A2(_Hk,_Hn,new T(function(){return B(_Hj(_Hk,_Ho));}));});}}},_Hp=new T2(1,_9n,_v),_Hq=new T2(1,_9V,_Hp),_Hr=new T2(1,_4p,_Hq),_Hs=new T2(1,_9g,_Hr),_Ht=new T2(1,_a6,_v),_Hu=new T2(1,_4p,_Ht),_Hv=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"440px");}),_Hw=new T2(1,_Hv,_Hu),_Hx=new T2(1,_9g,_Hw),_Hy="(function (x) { console.log(x);})",_Hz=new T(function(){return eval(E(_Hy));}),_HA=function(_HB){var _HC=jsShow(E(_HB));return new F(function(){return fromJSStr(_HC);});},_HD=new T2(1,_b,_v),_HE=new T(function(){return B(unCStr("biomechanics"));}),_HF=function(_HG,_){var _HH=__app1(E(_Hz),toJSStr(E(_HE)));return new F(function(){return _3g(_);});},_HI=new T(function(){return B(unCStr("calculateBtn"));}),_HJ=new T(function(){return B(unCStr("static/jump.jpeg"));}),_HK=new T(function(){return B(unCStr("Podaj parametry aerodynamiczne"));}),_HL=new T(function(){return B(unCStr("Podaj parametry biomechaniczne"));}),_HM=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"200px");}),_HN=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_ak)))),toJSStr(E(_4t)));}),_HO=new T2(1,_HN,_v),_HP=new T2(1,_4s,_HO),_HQ=new T2(1,_4r,_HP),_HR=new T(function(){return new T2(0,E(new T1(1,"font-size")),"22px");}),_HS=new T2(1,_HR,_HQ),_HT=new T2(1,_4x,_HS),_HU=new T2(1,_4p,_HT),_HV=new T2(1,_HM,_HU),_HW=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"340px");}),_HX=new T2(1,_HW,_HV),_HY=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"40px");}),_HZ=new T2(1,_HY,_eP),_I0=new T(function(){return B(unCStr("Wybierz skoczni\u0119"));}),_I1=new T(function(){return B(unCStr("static/hill.png"));}),_I2=new T(function(){return B(unCStr("Podaj rozmiar skoczni"));}),_I3=new T(function(){return new T2(0,E(new T1(1,"margin-top")),"15px");}),_I4=new T2(1,_I3,_v),_I5=function(_I6,_I7,_I8,_){var _I9=B(A(_7p,[_3l,_43,_4n,_I8,_])),_Ia=__app1(E(_8i),toJSStr(E(_I7))),_Ib=B(_eU(_I6,_I4,_)),_Ic=B(_gE(B(_gO(new T2(1,_Ia,new T2(1,_Ib,_v)))),_)),_Id=B(_4b(_I9,_Ic,_));return _I9;},_Ie=new T(function(){return B(unCStr("static/jumper.png"));}),_If=new T(function(){return B(unCStr("static/pencil.png"));}),_Ig=new T(function(){return B(unCStr("static/snowflake.png"));}),_Ih=function(_){var _Ii=B(A(_7p,[_3l,_43,_8h,_ab,_])),_Ij=B(_7m(_)),_Ik=B(_7A(_)),_Il=B(_I5(_I1,_I0,_HX,_)),_Im=E(_Ik),_In=E(_4a),_Io=__app2(_In,E(_Il),_Im),_Ip=B(_7A(_)),_Iq=B(_I5(_If,_I2,_HX,_)),_Ir=E(_Ip),_Is=__app2(_In,E(_Iq),_Ir),_It=B(_7A(_)),_Iu=B(_I5(_Ie,_HK,_HX,_)),_Iv=E(_It),_Iw=__app2(_In,E(_Iu),_Iv),_Ix=B(_7A(_)),_Iy=B(_I5(_Ig,_HL,_HX,_)),_Iz=E(_Ix),_IA=__app2(_In,E(_Iy),_Iz),_IB=B(A(_7p,[_3l,_43,_7A,_HZ,_])),_IC=B(A(_7p,[_3l,_43,_7A,_HZ,_])),_ID=B(A(_7p,[_3l,_43,_7A,_HZ,_])),_IE=B(_4b(_Ij,new T2(1,_Im,new T2(1,_IB,new T2(1,_Ir,new T2(1,_IC,new T2(1,_Iv,new T2(1,_ID,new T2(1,_Iz,_v))))))),_)),_IF=E(_Ii),_IG=__app2(_In,E(_Ij),_IF);return new T5(0,_IF,_Im,_Ir,_Iv,_Iz);},_IH=new T(function(){return new T2(0,E(new T1(1,"z-index")),"-1");}),_II=new T2(1,_IH,_v),_IJ=new T2(1,_4p,_II),_IK=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"900px");}),_IL=new T2(1,_IK,_IJ),_IM=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"100%");}),_IN=new T2(1,_IM,_IL),_IO=new T(function(){return new T2(0,E(new T1(1,"top")),"0px");}),_IP=new T2(1,_IO,_IN),_IQ=new T(function(){return new T2(0,E(new T1(1,"position")),"absolute");}),_IR=new T2(1,_IQ,_IP),_IS=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"260px");}),_IT=new T(function(){return new T2(0,E(new T1(1,"margin-top")),"300px");}),_IU=new T(function(){return new T2(0,E(new T1(1,"font-size")),"72px");}),_IV=new T2(1,_IU,_9U),_IW=new T2(1,_9n,_IV),_IX=new T2(1,_4x,_IW),_IY=new T(function(){return new T2(0,E(new T1(1,"padding-top")),"300px");}),_IZ=new T2(1,_IY,_IX),_J0=new T2(1,_IT,_IZ),_J1=new T2(1,_4p,_J0),_J2=new T2(1,_IS,_J1),_J3=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7j)))),"1500p");}),_J4=new T2(1,_J3,_J2),_J5=new T2(1,_9n,_9U),_J6=new T2(1,_9V,_J5),_J7=new T2(1,_4p,_J6),_J8=new T(function(){return new T2(0,E(new T1(1,toJSStr(E(_7g)))),"150px");}),_J9=new T2(1,_J8,_J7),_Ja=new T2(1,_9g,_J9),_Jb=new T(function(){return B(unCStr(" Odleg\u0142o\u015b\u0107: "));}),_Jc=new T(function(){return B(unCStr(" m"));}),_Jd=function(_Je,_Jf,_){return new F(function(){return _8j(_Je,B(_3(_Jb,new T(function(){var _Jg=jsShow(E(_Jf));return B(_3(fromJSStr(_Jg),_Jc));},1))),_);});},_Jh=new T(function(){return B(unCStr(" s"));}),_Ji=new T(function(){return B(unCStr(" Czas: "));}),_Jj=function(_Jk,_Jl,_){return new F(function(){return _8j(_Jk,B(_3(_Ji,new T(function(){var _Jm=jsShow(E(_Jl));return B(_3(fromJSStr(_Jm),_Jh));},1))),_);});},_Jn=new T(function(){return B(unCStr("Orze\u0142ek"));}),_Jo=function(_){var _Jp=B(A(_7p,[_3l,_43,_9F,_J4,_])),_Jq=B(_eU(_HJ,_IR,_)),_Jr=B(_8j(_Jp,_Jn,_)),_Js=E(_9G),_Jt=E(_4a),_Ju=__app2(_Jt,E(_Jq),_Js),_Jv=B(A(_7p,[_3l,_43,_9F,_ab,_])),_Jw=B(_Ih(_)),_Jx=E(_Jw),_Jy=E(_Jv),_Jz=__app2(_Jt,E(_Jx.a),_Jy),_JA=B(A(_7p,[_3l,_43,_9F,_Hx,_])),_JB=B(_fo(_)),_JC=E(_JB),_JD=_JC.b,_JE=_JC.c,_JF=E(_JA),_JG=__app2(_Jt,E(_JC.a),_JF),_JH=B(A(_7p,[_3l,_43,_9F,_9Z,_])),_JI=B(_aI(_)),_JJ=E(_JI),_JK=_JJ.b,_JL=_JJ.c,_JM=_JJ.d,_JN=E(_JH),_JO=__app2(_Jt,E(_JJ.a),_JN),_JP=B(A(_7p,[_3l,_43,_9F,_9Z,_])),_JQ=B(_bl(_)),_JR=E(_JQ),_JS=_JR.b,_JT=_JR.c,_JU=_JR.d,_JV=_JR.e,_JW=_JR.f,_JX=_JR.g,_JY=_JR.h,_JZ=E(_JP),_K0=__app2(_Jt,E(_JR.a),_JZ),_K1=B(A(_7p,[_3l,_43,_9F,_a3,_])),_K2=B(_8o(_JD,_JE,_JK,_JL,_JM,_JS,_JT,_JU,_JV,_JW,_JX,_JY,_)),_K3=E(_K2),_K4=E(_K1),_K5=__app2(_Jt,E(_K3.a),_K4),_K6=B(A(_7p,[_3l,_43,_9F,_Ja,_])),_K7=B(A(_7p,[_3l,_43,_9F,_Hs,_])),_K8=_K7,_K9=B(_9w(_)),_Ka=E(_K9),_Kb=E(_K6),_Kc=__app2(_Jt,E(_Ka.a),_Kb),_Kd=function(_Ke,_){var _Kf=B(A(_5b,[_3l,_43,_JS,_5l,_])),_Kg=E(_Hz),_Kh=__app1(_Kg,toJSStr(E(_HI))),_Ki=B(A(_5b,[_3l,_43,_JU,_5l,_])),_Kj=B(A(_5b,[_3l,_43,_JT,_5l,_])),_Kk=B(A(_5b,[_3l,_43,_JV,_5l,_])),_Kl=B(A(_5b,[_3l,_43,_JW,_5l,_])),_Km=B(A(_5b,[_3l,_43,_JY,_5l,_])),_Kn=B(A(_5b,[_3l,_43,_JX,_5l,_])),_Ko=B(A(_5b,[_3l,_43,_JK,_5l,_])),_Kp=B(A(_5b,[_3l,_43,_JL,_5l,_])),_Kq=B(A(_5b,[_3l,_43,_JM,_5l,_])),_Kr=B(A(_5b,[_3l,_43,_JD,_5l,_])),_Ks=B(A(_5b,[_3l,_43,_JE,_5l,_])),_Kt=B(_DS(_Kf,_Ki,_Kj,_Kk,_Kl,_Km,_Kn,_Ko,_Kp,_Kq,_Kr,_Ks,_)),_Ku=E(_Kt),_Kv=E(_Ku.a),_Kw=jsShow(_Kv),_Kx=__app1(_Kg,toJSStr(fromJSStr(_Kw))),_Ky=B(_GR(_Kf,_Ki,_Kj,_Kk,_Kl,_Km,_Kn,_Ko,_Kp,_Kq,_Kr,_Ks,_)),_Kz=E(_Ky);if(!_Kz._){return E(_9S);}else{var _KA=E(_Kz.a),_KB=new T(function(){var _KC=new T(function(){return B(_HA(_KA.b));}),_KD=new T(function(){return B(_HA(_KA.a));});return B(A3(_Hj,_45,new T2(1,function(_KE){return new F(function(){return _3(_KD,_KE);});},new T2(1,function(_KE){return new F(function(){return _3(_KC,_KE);});},_v)),_HD));}),_KF=__app1(_Kg,toJSStr(new T2(1,_c,_KB))),_KG=E(_K8),_KH=E(_ac),_KI=__app1(_KH,_KG),_KJ=B(_G0(_Kz,_KG,_)),_KK=E(_Ka.b),_KL=__app1(_KH,_KK),_KM=E(_Ka.c),_KN=__app1(_KH,_KM),_KO=B(_Jd(_KK,_Kv,_));return new F(function(){return _Jj(_KM,_Ku.b,_);});}},_KP=B(A(_7P,[_44,_3l,_3e,_K3.b,_49,_Kd,_])),_KQ=B(A(_7P,[_44,_3l,_3e,_Jx.b,_49,function(_KR,_){var _KS=__app1(E(_Hg),_JF);return new F(function(){return _3g(_);});},_])),_KT=B(A(_7P,[_44,_3l,_3e,_Jx.c,_49,function(_KU,_){var _KV=__app1(E(_Hg),_JF);return new F(function(){return _3g(_);});},_])),_KW=B(A(_7P,[_44,_3l,_3e,_Jx.d,_49,function(_KX,_){var _KY=__app1(E(_Hg),_JN);return new F(function(){return _3g(_);});},_])),_KZ=B(A(_7P,[_44,_3l,_3e,_Jx.e,_49,_HF,_])),_L0=B(_gE(B(_gO(new T2(1,_Jp,new T2(1,_Jy,new T2(1,_JF,new T2(1,_JN,new T2(1,_JZ,new T2(1,_K4,new T2(1,_Kb,new T2(1,_K8,_v)))))))))),_)),_L1=B(_4b(_Js,_L0,_));return new F(function(){return _9H(_);});},_L2=function(_){return new F(function(){return _Jo(_);});};
var hasteMain = function() {B(A(_L2, [0]));};window.onload = hasteMain;