module Client.Panels.ResultPanel where

import Haste.DOM
import Client.Styles.Common
import Client.Styles.Bright
import Client.Tags.Table
import Client.Utils.LayoutUtils

createResultPanel :: Double -> Double -> IO (Elem, Elem, Elem)
createResultPanel distance time = do
  resultTable <- table
  --mass
  distanceRow <- row
  timeRow <- row

  distanceResult <- resultColumn
  --writeOnElem distanceResult (distanceText ++ "m")
  --
  timeResult <- resultColumn
  --writeOnElem timeResult (timeText ++ "s")

  appendChildren distanceRow [distanceResult]
  appendChildren timeRow [timeResult]
  appendChildren resultTable [distanceRow, timeRow]

  return (resultTable, distanceResult, timeResult)

setDistance :: Elem -> Double -> IO ()
setDistance panel distance =
    writeOnElem panel (distanceText ++ show distance ++ " m")

setTime :: Elem -> Double -> IO ()
setTime panel time =
    writeOnElem panel (timeText ++ show time ++ " s")
