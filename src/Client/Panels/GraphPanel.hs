module Client.Panels.GraphPanel where

import Haste.DOM
import Haste.Graphics.Canvas
import Client.Styles.Bright
import Client.Tags.Canvas
import Client.Utils.LayoutUtils
import Server.Simulation.Results
import Server.Math.AngleCalc

width, heightSmall, heightBig, radius :: Double

width = 1000 -- width of canvas
heightSmall = 500 -- height of canvas
heightBig = 800
radius = 2 -- radius of point

-- mock
-- flightPath :: [Point]
-- flightPath =
--   [ (a,b) | a <- [1..30] , b <- [a+1] ]

-- textColor = "#EFE8E0"
scaleVector :: Vector
scaleVector = (6, 6)

white :: Picture () -> Picture ()
white = color (RGB 255 255 255)

flightPoint :: Point -> Double -> Picture ()
flightPoint point height  =
  white $ fill $ circle (mirrorImageY (scalePoint point) height) radius

mirrorImageY :: Point -> Double -> Point
mirrorImageY point height =
    (fst point, height - snd point)

scalePoint :: Point -> Point
scalePoint (x,y) =
      ((fst scaleVector) * x, (snd scaleVector) * y)

drawGraph :: [Point] -> Double -> Picture ()
drawGraph [last] height =
  flightPoint last height

drawGraph (head:tail) height = do
  flightPoint head height
  drawGraph tail height

createGraphPanel:: IO Elem
createGraphPanel = do
  graphPanel <- newStyledDiv graphPanelStyle

  return graphPanel

fitHeight :: Point -> Double
fitHeight point
  | snd point < 83 = heightSmall
  | otherwise = heightBig

drawAndRender :: [Point] -> Elem -> IO Elem
drawAndRender path panel = do
  canvasElem <- styledCanvas width (fitHeight $ head path) canvasStyle
  Just canvas <- fromElem canvasElem
  render canvas $ drawGraph path (fitHeight $ head path)

  appendChild panel canvasElem

  return panel
