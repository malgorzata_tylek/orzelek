module Client.Panels.ButtonsPanel where

import Haste.DOM
import Haste.Events
import Control.Monad.IO.Class
import Control.Monad
import Data.Maybe
import Client.Styles.Common
import Client.Styles.Bright
import Client.Tags.Table
import Client.Tags.Button
import Client.Utils.LayoutUtils
import Client.Panels.HillPanel
import Server.Simulation.Defaults

data Buttons = Buttons {
  buttonTbl :: Elem,
  calculateBtn :: Elem
}

createButtonsPanel :: (Elem, Elem) -> (Elem, Elem, Elem) -> (Elem, Elem, Elem, Elem, Elem, Elem, Elem) -> IO Buttons
createButtonsPanel (hsInput, slopeInput) (airDensityInput, dragInput, liftInput)
  (massInput, velocityXInput, velocityYInput, jumperAngleInput, movementAngleInput, longitudinalAreaInput, frontalAreaInput) = do
  buttonsTable <- table
  buttonsRow <- styledRow buttonsRowStyle
  calculateColumn <- styledColumn buttonColumnStyle
  calculateButton <- button calculateText buttonStyle
  appendChild calculateColumn calculateButton

  clearColumn <- styledColumn buttonColumnStyle
  clearButton <- button clearText buttonStyle
  appendChild clearColumn clearButton

  fillColumn <- styledColumn buttonColumnStyle
  fillButton <- button fillText buttonStyle
  appendChild fillColumn fillButton

  -- hsInput <- elemById "hs"
  onEvent clearButton Click $ \_ -> do
     set hsInput [ prop "value" =: "" ]
     set slopeInput [ prop "value" =: "" ]
     set airDensityInput [ prop "value" =: "" ]
     set dragInput [ prop "value" =: "" ]
     set liftInput [ prop "value" =: "" ]
     set massInput [ prop "value" =: "" ]
     set velocityXInput [ prop "value" =: "" ]
     set velocityYInput [ prop "value" =: "" ]
     set jumperAngleInput [ prop "value" =: "" ]
     set movementAngleInput [ prop "value" =: "" ]
     set longitudinalAreaInput [ prop "value" =: "" ]
     set frontalAreaInput [ prop "value" =: "" ]

  onEvent fillButton Click $ \_ -> do
    fillAll hsInput slopeInput airDensityInput dragInput liftInput massInput velocityXInput velocityYInput jumperAngleInput movementAngleInput longitudinalAreaInput frontalAreaInput

  onEvent calculateButton Click $ \_ -> do
    fillAll hsInput slopeInput airDensityInput dragInput liftInput massInput velocityXInput velocityYInput jumperAngleInput movementAngleInput longitudinalAreaInput frontalAreaInput

  appendChildren buttonsRow [calculateColumn, clearColumn, fillColumn]
  appendChild buttonsTable buttonsRow

  return Buttons {
    buttonTbl = buttonsTable,
    calculateBtn = calculateButton
  }

fillAll :: Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> Elem -> IO ()
fillAll hsInput slopeInput airDensityInput dragInput liftInput massInput velocityXInput velocityYInput jumperAngleInput movementAngleInput longitudinalAreaInput frontalAreaInput = do
  fillValue hsInput $ show hillSize
  fillValue slopeInput $ show slopeAngle
  fillValue airDensityInput $ show airDensity
  fillValue dragInput $ show dragCoefficient
  fillValue liftInput $ show liftCoefficient
  fillValue massInput $ show mass
  fillValue velocityXInput $ show velocity_x
  fillValue velocityYInput $ show velocity_y
  fillValue jumperAngleInput $ show jumperAngle
  fillValue movementAngleInput $ show movementAngle
  fillValue longitudinalAreaInput $ show longitudinalArea
  fillValue frontalAreaInput $ show frontalArea


fillValue :: Elem -> String -> IO ()
fillValue inputElem defaultValue = do
  currentValue <- getInputValue inputElem
  txt <- getProperText currentValue defaultValue
  set inputElem [ prop "value" =: txt ]

getInputValue :: Elem -> IO String
getInputValue input =
  getProp input getPropId

getPropId :: PropID
getPropId = "value"

getProperText :: String -> String -> IO AttrValue

getProperText "" defaultValue =
  return defaultValue

getProperText currentValue defaultValue =
  return currentValue
