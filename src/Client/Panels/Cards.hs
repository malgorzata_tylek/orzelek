module Client.Panels.Cards where

import Haste.DOM
import Control.Monad
import Client.Utils.LayoutUtils
import Client.Styles.Common
import Client.Styles.Bright
import Client.Tags.Table
import Client.Tags.Button
--import Network.CGI

data Cards = Cards {
  cardsTable :: Elem,
  hillChoiceCrd :: Elem,
  hillSizeCrd :: Elem,
  aerodynamicsCrd :: Elem,
  biomechanicsCrd :: Elem
}

--todo: refactor
makeCards :: IO Cards
makeCards = do
  cardsAreaTable <- styledTable cardAreaStyle
  cardsAreaRow <- row
  --
  hillChoiceCard <- column
  hillChoiceButton <- imgTextButton hillImgSrc hillChoiceText cardStyle
  appendChild hillChoiceCard hillChoiceButton

  hillSizeCard <- column
  hillSizeButton <- imgTextButton pencilImgSrc hillSizeText cardStyle
  appendChild hillSizeCard hillSizeButton

  aerodynamicsCard <- column
  aerodynamicsButton <- imgTextButton jumperImgSrc aerodynamicsText cardStyle
  appendChild aerodynamicsCard aerodynamicsButton

  biomechanicsCard <- column
  biomechanicsButton <- imgTextButton snowflakeImgSrc biomechanicsText cardStyle
  appendChild biomechanicsCard biomechanicsButton
  --
  emptyCard0 <- styledColumn emptyCardStyle
  emptyCard1 <- styledColumn emptyCardStyle
  emptyCard2 <- styledColumn emptyCardStyle
  appendChildren cardsAreaRow [hillChoiceCard, emptyCard0, hillSizeCard, emptyCard1, aerodynamicsCard, emptyCard2, biomechanicsCard]
  appendChild cardsAreaTable cardsAreaRow

  return Cards {
    cardsTable = cardsAreaTable,
    hillChoiceCrd = hillChoiceCard,
    hillSizeCrd = hillSizeCard,
    aerodynamicsCrd = aerodynamicsCard,
    biomechanicsCrd = biomechanicsCard
  }

-- refactor - usunac 'do'
makeStyledCardWithText :: [Attribute] -> String -> IO Elem
makeStyledCardWithText attributes text = do
  card <- styledColumn attributes
  writeOnElem card text
  return card

--makeStyledCardWithText0 :: [Attribute] -> String -> IO Elem
-- makeStyledCardWithText0 attributes text =
--   return $ writeOnElem (liftIO $ styledColumn attributes) text
