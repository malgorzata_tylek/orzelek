module Client.Panels.AerodynamicsPanel where

import Haste.DOM
import Client.Styles.Common
import Client.Styles.Bright
import Client.Tags.Table
import Client.Utils.LayoutUtils

data AerodynamicsInput = AerodynamicsInput {
  aeroTbl :: Elem,
  airInput :: Elem,
  dragInput :: Elem,
  liftInput :: Elem
}

--todo: wydzielic do funkcji
createAerodynamicsPanel :: IO AerodynamicsInput
createAerodynamicsPanel = do
  aeroTable <- table
  --
  airDensityRow <- styledRow paramRowStyle
  airDensityTitle <- parameterColumn
  writeOnElem airDensityTitle airDensityText
  airDensityInputColumn <- parameterColumn
  airDensityInput <- newElem "input" `with` paramInputStyle

  appendChild airDensityInputColumn airDensityInput
  appendChildren airDensityRow [airDensityTitle, airDensityInputColumn]
  --
  dragRow <- styledRow paramRowStyle
  dragTitle <- parameterColumn
  writeOnElem dragTitle dragCoefficientText
  dragInputColumn <- parameterColumn
  dragInput <- newElem "input" `with` paramInputStyle

  appendChild dragInputColumn dragInput
  appendChildren dragRow [dragTitle, dragInputColumn]
  --
  liftRow <- styledRow paramRowStyle
  liftTitle <- parameterColumn
  writeOnElem liftTitle liftCoefficientText
  liftInputColumn <- parameterColumn
  liftInput <- newElem "input" `with` paramInputStyle

  appendChild liftInputColumn liftInput
  appendChildren liftRow [liftTitle, liftInputColumn]

  --
  appendChildren aeroTable [airDensityRow, dragRow, liftRow]

  return AerodynamicsInput {
    aeroTbl = aeroTable,
    airInput = airDensityInput,
    dragInput = dragInput,
    liftInput = liftInput
  }
