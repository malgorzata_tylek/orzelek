module Client.Panels.BiomechanicsPanel where

import Haste.DOM
import Client.Styles.Common
import Client.Styles.Bright
import Client.Tags.Table
import Client.Utils.LayoutUtils

data BiomechanicsInput = BiomechanicsInput {
  bioTbl :: Elem,
  massInput :: Elem,
  velXInput :: Elem,
  velYInput :: Elem,
  jmpAngleInput :: Elem,
  mvtAngleInput :: Elem,
  longAreaInput :: Elem,
  frontAreaInput :: Elem
}

createBiomechanicsPanel :: IO BiomechanicsInput
createBiomechanicsPanel = do
  bioTable <- table
  --mass
  massRow <- styledRow paramRowStyle
  massTitle <- parameterColumn
  writeOnElem massTitle massText
  massInputColumn <- parameterColumn
  massInput <- newElem "input" `with` paramInputStyle

  appendChild massInputColumn massInput
  appendChildren massRow [massTitle, massInputColumn]

  --velocityX
  velocityXRow <- styledRow paramRowStyle
  velocityXTitle <- parameterColumn
  writeOnElem velocityXTitle velocityXText
  velocityXInputColumn <- parameterColumn
  velocityXInput <- newElem "input" `with` paramInputStyle

  appendChild velocityXInputColumn velocityXInput
  appendChildren velocityXRow [velocityXTitle, velocityXInputColumn]
  --velocityY
  velocityYRow <- styledRow paramRowStyle
  velocityYTitle <- parameterColumn
  writeOnElem velocityYTitle velocityYText
  velocityYInputColumn <- parameterColumn
  velocityYInput <- newElem "input" `with` paramInputStyle

  appendChild velocityYInputColumn velocityYInput
  appendChildren velocityYRow [velocityYTitle, velocityYInputColumn]
  --jumperAngle
  jumperAngleRow <- styledRow paramRowStyle
  jumperAngleTitle <- parameterColumn
  writeOnElem jumperAngleTitle jumperAngleText
  jumperAngleInputColumn <- parameterColumn
  jumperAngleInput <- newElem "input" `with` paramInputStyle

  appendChild jumperAngleInputColumn jumperAngleInput
  appendChildren jumperAngleRow [jumperAngleTitle, jumperAngleInputColumn]

  --movementAngle
  movementAngleRow <- styledRow paramRowStyle
  movementAngleTitle <- parameterColumn
  writeOnElem movementAngleTitle movementAngleText
  movementAngleInputColumn <- parameterColumn
  movementAngleInput <- newElem "input" `with` paramInputStyle

  appendChild movementAngleInputColumn movementAngleInput
  appendChildren movementAngleRow [movementAngleTitle, movementAngleInputColumn]

  --longitudinalArea
  longitudinalAreaRow <- styledRow paramRowStyle
  longitudinalAreaTitle <- parameterColumn
  writeOnElem longitudinalAreaTitle longitudinalAreaText
  longitudinalAreaInputColumn <- parameterColumn
  longitudinalAreaInput <- newElem "input" `with` paramInputStyle

  appendChild longitudinalAreaInputColumn longitudinalAreaInput
  appendChildren longitudinalAreaRow [longitudinalAreaTitle, longitudinalAreaInputColumn]

  --frontalArea
  frontalAreaRow <- styledRow paramRowStyle
  frontalAreaTitle <- parameterColumn
  writeOnElem frontalAreaTitle frontalAreaText
  frontalAreaInputColumn <- parameterColumn
  frontalAreaInput <- newElem "input" `with` paramInputStyle

  appendChild frontalAreaInputColumn frontalAreaInput
  appendChildren frontalAreaRow [frontalAreaTitle, frontalAreaInputColumn]

  appendChildren bioTable [massRow, velocityXRow, velocityYRow, jumperAngleRow, movementAngleRow, longitudinalAreaRow, frontalAreaRow]

  return BiomechanicsInput {
    bioTbl = bioTable,
    massInput = massInput,
    velXInput = velocityXInput,
    velYInput = velocityYInput,
    jmpAngleInput = jumperAngleInput,
    mvtAngleInput = movementAngleInput,
    longAreaInput = longitudinalAreaInput,
    frontAreaInput = frontalAreaInput
  }
