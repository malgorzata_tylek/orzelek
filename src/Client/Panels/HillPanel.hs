module Client.Panels.HillPanel where

import Haste.DOM
import Haste
import Haste.App
import Haste.Events
import Control.Monad
import Client.Styles.Common
import Client.Styles.Bright
import Client.Tags.Table
import Client.Tags.Button
import Client.Tags.Input
import Client.Utils.LayoutUtils
import Client.Parameters.Hills
import Client.Events.HillClickedEvents

data HillsInput = HillsInput {
  hillTbl :: Elem,
  hsInput :: Elem,
  slopeInput :: Elem
}

createHillPanel :: IO HillsInput
createHillPanel = do
  hillTable <- table
  hillsAndSizeRow <- styledRow hillRowStyle
  hillsRow <- styledRow hillRowStyle
  -- hillsAndSizeRow
  -- refactor
  zakopaneHill <- styledColumn hillColumnStyle
  zakopaneButton <- imgButton zakopaneImgSrc hillButtonStyle hillPhotoStyle
  appendChild zakopaneHill zakopaneButton

  engelbergHill <- styledColumn hillColumnStyle
  engelbergButton <- imgButton engelbergImgSrc hillButtonStyle hillPhotoStyle
  appendChild engelbergHill engelbergButton

  ramsauHill <- styledColumn hillColumnStyle
  ramsauButton <- imgButton ramsauImgSrc hillButtonStyle hillPhotoStyle
  appendChild ramsauHill ramsauButton

  kuusamoHill <- styledColumn hillColumnStyle
  kuusamoButton <- imgButton kuusamoImgSrc hillButtonStyle hillPhotoStyle
  appendChild kuusamoHill kuusamoButton

  hillSizeTitle <- hillParameterColumn
  writeOnElem hillSizeTitle hsText
  hillSizeInputColumn <- hillParameterColumn
  hillSizeInput <- input paramInputStyle
  appendChild hillSizeInputColumn hillSizeInput

  appendChildren hillsAndSizeRow [zakopaneHill, engelbergHill, ramsauHill, kuusamoHill, hillSizeTitle, hillSizeInputColumn]
  -- hillsRow
  innsbruckHill <- styledColumn hillColumnStyle
  innsbruckButton <- imgButton innsbruckImgSrc hillButtonStyle hillPhotoStyle
  appendChild innsbruckHill innsbruckButton

  bischofshofenHill <- styledColumn hillColumnStyle
  bischofshofenButton <- imgButton bischofshofenImgSrc hillButtonStyle hillPhotoStyle
  appendChild bischofshofenHill bischofshofenButton

  osloHill <- styledColumn hillColumnStyle
  osloButton <- imgButton osloImgSrc hillButtonStyle hillPhotoStyle
  appendChild osloHill osloButton

  planicaHill <- styledColumn hillColumnStyle
  planicaButton <- imgButton planicaImgSrc hillButtonStyle hillPhotoStyle
  appendChild planicaHill planicaButton

  slopeAngleTitle <- hillParameterColumn
  writeOnElem slopeAngleTitle slopeAngleText
  slopeAngleInputColumn <- hillParameterColumn
  slopeAngleInput <- input paramInputStyle
  appendChild slopeAngleInputColumn slopeAngleInput

  appendChildren hillsRow [innsbruckHill, bischofshofenHill, osloHill, planicaHill, slopeAngleTitle, slopeAngleInputColumn]
  -- table
  appendChildren hillTable [hillsAndSizeRow, hillsRow]

-- click events
  onEvent zakopaneButton Click $ \_ ->
    hillClicked "zakopane" hillSizeInput slopeAngleInput

  onEvent engelbergButton Click $ \_ ->
    hillClicked "engelberg" hillSizeInput slopeAngleInput

  onEvent ramsauButton Click $ \_ ->
    hillClicked "ramsau" hillSizeInput slopeAngleInput

  onEvent kuusamoButton Click $ \_ ->
    hillClicked "kuusamo" hillSizeInput slopeAngleInput

  onEvent innsbruckButton Click $ \_ ->
    hillClicked "innsbruck" hillSizeInput slopeAngleInput

  onEvent bischofshofenButton Click $ \_ ->
    hillClicked "bischofshofen" hillSizeInput slopeAngleInput

  onEvent osloButton Click $ \_ ->
    hillClicked "oslo" hillSizeInput slopeAngleInput

  onEvent planicaButton Click $ \_ ->
    hillClicked "planica" hillSizeInput slopeAngleInput

  return HillsInput {
    hillTbl = hillTable,
    hsInput = hillSizeInput,
    slopeInput = slopeAngleInput
  }





-- setPrompt elem promptText =
--   onEvent elem MouseOver $ \_ -> do
--     appendChild $ elem caption promptText
--     return ()
