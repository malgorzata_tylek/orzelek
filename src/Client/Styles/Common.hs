{-# LANGUAGE OverloadedStrings #-}
module Client.Styles.Common where

-- The color palette.
type Color = String

fstColor, sndColor, bgColor, textColor, cardColor, photoColor :: Color

-- bordowy
fstColor = "#8E121A"
-- granatowy
sndColor = "#143057"
-- beżowy
bgColor = "#A8AEC1"
-- biały
textColor = "#EFE8E0"
--szary
cardColor = "#555641"
-- blekitny
photoColor = "#90B8C8"

---------------------------------------------------------------------------

-- texts

titleText, hillChoiceText, hillSizeText, aerodynamicsText, biomechanicsText :: String
hsText, slopeAngleText, airDensityText, dragCoefficientText, liftCoefficientText :: String
jumperAngleText, movementAngleText, velocityXText, velocityYText, longitudinalAreaText, frontalAreaText, massText :: String
calculateText, clearText, distanceText, timeText  :: String

titleText = "Orzełek"
hillChoiceText = "Wybierz skocznię"
hillSizeText = "Podaj rozmiar skoczni"
aerodynamicsText = "Podaj parametry aerodynamiczne"
biomechanicsText = "Podaj parametry biomechaniczne"

hsText = "HS [m]"
slopeAngleText = "Kąt nachylenia stoku [stopnie]"
airDensityText = "Gęstość powietrza [kg/m3]"
dragCoefficientText = "Współczynnik oporu"
liftCoefficientText = "Współczynnik siły nośnej"

jumperAngleText = "Kąt skierowania skoczka [stopnie]"
movementAngleText = "Kąt natarcia [stopnie]"
velocityXText = "Prędkość na progu w poziomie [m/s]"
velocityYText = "Prędkość na progu w pionie [m/s]"
longitudinalAreaText = "Powierzchnia przekroju podłużnego skoczka [m2]"
frontalAreaText = "Powierzchnia przekroju poprzecznego skoczka [m2]"
massText = "Masa skoczka [kg]"

calculateText = "Oblicz"
clearText = "Wyczyść"
fillText = "Uzupełnij wartości"

distanceText = " Odległość: "
timeText = " Czas: "

----------------------------------------------

--src

mainPhotoImgSrc, hillImgSrc, jumperImgSrc, snowflakeImgSrc :: String

hillImgSrc = "static/hill.png"
jumperImgSrc = "static/jumper.png"
snowflakeImgSrc = "static/snowflake.png"
pencilImgSrc = "static/pencil.png"
mainPhotoImgSrc = "static/jump.jpeg"

--hills

bischofshofenImgSrc = "static/bischofshofen.jpg"
innsbruckImgSrc = "static/innsbruck.jpg"
ramsauImgSrc = "static/ramsau.jpg"
zakopaneImgSrc = "static/zakopane.jpg"
engelbergImgSrc = "static/engelberg.jpg"
osloImgSrc = "static/oslo.jpg"
kuusamoImgSrc = "static/kuusamo.jpg"
planicaImgSrc = "static/planica.jpg"

wallpaperUrl = "static/wallpapers/wallpaper0.jpg"
