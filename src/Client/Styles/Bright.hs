module Client.Styles.Bright where

import Haste.DOM
import Control.Monad.IO.Class
import Client.Styles.Common
import Client.Tags.PropIDs

--dwa sposoby na przypisanie stylu:
--I
assignBgStyle :: IO ()
assignBgStyle = do
  setStyle documentBody "background-image" "url(\"static/wallpapers/wallpaper3.jpg\")"
  setStyle documentBody "margin" "auto"
  setStyle documentBody width "100%"

--II
photoAreaStyle :: [Attribute]
photoAreaStyle =
  [
    style width =: "1500p",
    style height =: "260px",
    style "margin" =: "auto",
    style "margin-top" =: "300px",
    style "padding-top" =: "300px",
    style "color" =: textColor,
    style "text-align" =: "center",
    style "font-size" =: "72px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif"
  ]

photo :: [Attribute]
photo =
  [
    style "position" =: "absolute",
    style "top" =: "0px",
    style width =: "100%",
    style height =: "900px",
    style "margin" =: "auto",
    style "z-index" =: "-1"
  ]

cardAreaStyle :: [Attribute]
cardAreaStyle =
  [
    style width =: "1500px",
    style height =: "160px",
    style "margin" =: "auto",
    style "margin-top" =: "80px",
    style "margin-bottom" =: "100px"
  ]

cardStyle :: [Attribute]
cardStyle =
  [
    style width =: "340px",
    style height =: "200px",
    style "margin" =: "auto",
    style "color" =: textColor,
    style "font-size" =: "22px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif",
    style background =: fstColor
  ]

emptyCardStyle :: [Attribute]
emptyCardStyle =
  [
    style width =: "40px",
    style height =: "180px"
    --style background =: bgColor
  ]

hillPanelStyle :: [Attribute]
hillPanelStyle =
  [
    style width =: "1500px",
    style height =: "440px",
    style "margin" =: "auto",
    style "margin-top" =: "80px"
  ]

hillRowStyle :: [Attribute]
hillRowStyle =
  [
    style width =: "1500px",
    style height =: "180px"
    --style background =: photoColor
  ]

emptyRowStyle :: [Attribute]
emptyRowStyle =
  [
    style width =: "1500px",
    style height =: "10px",
    style background =: bgColor
  ]

hillColumnStyle :: [Attribute]
hillColumnStyle =
  [
    style width =: "230px",
    style height =: "160px"
    --style background =: "yellow"
  ]

hillPhotoStyle :: [Attribute]
hillPhotoStyle =
  [
    style width =: "200px",
    style height =: "160px",
    style margin =: "auto"
    --style background =: "yellow"
  ]

hillButtonStyle :: [Attribute]
hillButtonStyle =
  [
    style width =: "200px",
    style height =: "160px",
    style margin =: "auto",
    style "padding" =: "0px"
      --style background =: "yellow"
  ]

frameStyle :: [Attribute]
frameStyle =
  [
    style width =: "5px",
    style background =: fstColor
  ]

hillParameterColumnStyle :: [Attribute]
hillParameterColumnStyle =
  [
    style width =: "280px",
    style height =: "160px",
    style "padding-right" =: "20px",
    style "color" =: sndColor,
    style "text-align" =: "center",
    style "font-size" =: "28px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif"
  ]

parameterColumnStyle :: [Attribute]
parameterColumnStyle =
  [
    style width =: "430px",
    style height =: "100px",
    style "padding-left" =: "20px",
    style "padding-right" =: "20px",
    style "margin" =: "auto",
    style "color" =: sndColor,
    style "text-align" =: "left",
    style "font-size" =: "28px"
  ]

columnStyle :: [Attribute]
columnStyle =
  [
    style width =: "1500px",
    style height =: "100px",
    style "padding-left" =: "20px",
    style "padding-right" =: "20px",
    style "margin" =: "auto",
    style "color" =: sndColor,
    style "text-align" =: "center",
    style "font-size" =: "28px"
  ]

paramInputStyle :: [Attribute]
paramInputStyle =
  [
    style "float" =: "right",
    style width =: "200px",
    style height =: "50px",
    style "padding-right" =: "10px",
    style "font-size" =: "24px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif",
    style "color" =: textColor,
    style background =: sndColor
  ]

paramRowStyle :: [Attribute]
paramRowStyle =
  [
    style width =: "520px",
    style height =: "100px",
    style "margin" =: "auto"
    --style background =: "red"
  ]

aeroPanelStyle :: [Attribute]
aeroPanelStyle =
  [
    style width =: "1000px",
    style "margin" =: "auto",
    style "margin-bottom" =: "80px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif"
  ]

bioPanelStyle :: [Attribute]
bioPanelStyle =
  [
    style width =: "1000px",
    style "margin" =: "auto",
    style "margin-bottom" =: "80px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif"
  ]

buttonsPanelStyle :: [Attribute]
buttonsPanelStyle =
  [
    style width =: "1050px",
    style height =: "100px",
    style "margin" =: "auto",
    style "margin-bottom" =: "80px"
    --style background =: "blue"
  ]

buttonsRowStyle :: [Attribute]
buttonsRowStyle =
  [
    style width =: "1050px",
    style height =: "100px",
    style "margin" =: "auto"
  ]

buttonColumnStyle :: [Attribute]
buttonColumnStyle =
  [
    style width =: "350px",
    style height =: "100px",
    style "margin" =: "auto"
  ]

buttonStyle :: [Attribute]
buttonStyle =
  [
    style "width" =: "350px",
    style "height" =: "100px",
    style "margin" =: "auto",
    style "font-size" =: "24px",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif",
    style "color" =: textColor,
    style "background" =: fstColor
  ]

resultsPanelStyle :: [Attribute]
resultsPanelStyle =
  [
    style width =: "1500px",
    style height =: "150px",
    style "margin" =: "auto",
    style "margin-bottom" =: "80px",
    style "text-align" =: "center",
    style "font-weight" =: "lighter",
    style "font-family" =: "Palatino Linotyp, sans-serif"
  ]

canvasStyle :: [Attribute]
canvasStyle =
  [
    style "display" =: "block",
    style "border" =: "1px solid black",
    style "margin" =: "0px auto 0 auto",
    style "backgroundColor" =: sndColor
  ]

graphPanelStyle :: [Attribute]
graphPanelStyle =
  [
    style width =: "1500px",
    style "margin" =: "auto",
    style "margin-bottom" =: "80px",
    style "text-align" =: "center"
  ]
