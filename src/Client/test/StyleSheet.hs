module StyleSheet where

import Clay
import Control.Monad
import Data.Monoid
import Prelude hiding (div, span)
import System.Environment

import qualified Clay.Media        as Media
import qualified Data.Text.Lazy.IO as Text

--import Codeblock
import Common
--import Header

--main = putCss theStylesheet

theStylesheet :: Css
theStylesheet =
    do
      background blue
      width  (px 50)
      height (px 60)
