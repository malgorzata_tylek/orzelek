module Client.Events.HillClickedEvents where

import Haste.DOM
import Haste.Events
import Client.Parameters.Hills


hillClicked :: String -> Elem -> Elem -> IO ()

hillClicked "zakopane" inputHS inputAngle = do
  set inputHS [ prop "value" =: show zakopaneHS ]
  set inputAngle [ prop "value" =: show zakopaneSlopeAngle ]

hillClicked "engelberg" inputHS inputAngle = do
  set inputHS [ prop "value" =: show engelbergHS ]
  set inputAngle [ prop "value" =: show engelbergSlopeAngle ]

hillClicked "bischofshofen" inputHS inputAngle = do
  set inputHS [ prop "value" =: show bischofshofenHS ]
  set inputAngle [ prop "value" =: show bischofshofenSlopeAngle ]

hillClicked "innsbruck" inputHS inputAngle = do
  set inputHS [ prop "value" =: show innsbruckHS ]
  set inputAngle [ prop "value" =: show innsbruckSlopeAngle ]

hillClicked "ramsau" inputHS inputAngle = do
  set inputHS [ prop "value" =: show ramsauHS ]
  set inputAngle [ prop "value" =: show ramsauSlopeAngle ]

hillClicked "oslo" inputHS inputAngle = do
  set inputHS [ prop "value" =: show osloHS ]
  set inputAngle [ prop "value" =: show osloSlopeAngle ]

hillClicked "kuusamo" inputHS inputAngle = do
  set inputHS [ prop "value" =: show kuusamoHS ]
  set inputAngle [ prop "value" =: show kuusamoSlopeAngle ]

hillClicked "planica" inputHS inputAngle = do
  set inputHS [ prop "value" =: show planicaHS ]
  set inputAngle [ prop "value" =: show planicaSlopeAngle ]
