module Client.Tags.Canvas where

import Haste.DOM
import Haste.Graphics.Canvas

canvas :: Double -> Double -> IO Elem
canvas width height = do
 canvas <- newElem "canvas"
 setProp canvas "width" (show width)
 setProp canvas "height" (show height)
 return canvas

styledCanvas :: Double -> Double -> [Attribute] -> IO Elem
styledCanvas width height attributes =
  canvas width height `with` attributes
