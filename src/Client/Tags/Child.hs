module Client.Tags.Child where

import Haste.DOM

child :: Elem -> Elem -> IO ()
child parent childElem =
  appendChild parent childElem
