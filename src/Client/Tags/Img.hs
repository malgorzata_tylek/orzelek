module Client.Tags.Img where

import Haste.DOM

img :: String -> IO Elem
img imgSrc = do
  imgElem <- newElem "img"
  setProp imgElem "src" imgSrc
  return imgElem

styledImg :: String -> [Attribute] -> IO Elem
styledImg imgSrc attributes = do
  imgElem <- newElem "img" `with` attributes
  setProp imgElem "src" imgSrc
  return imgElem
