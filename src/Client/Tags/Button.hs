module Client.Tags.Button where

import Haste.DOM
import Client.Utils.LayoutUtils
import Client.Tags.Img

button :: String -> [Attribute] -> IO Elem
button label attributes = do
  buttonElem <- newElem "button" `with` attributes
  writeOnElem buttonElem label
  return buttonElem

imgTextButton :: String -> String -> [Attribute] -> IO Elem
imgTextButton imgSrc label attributes = do
  buttonElem <- newElem "button" `with` attributes
  labelElem <- newTextElem label
  imgElem <- styledImg imgSrc [style "margin-top" =: "15px"]
  --setProp imgElem "src" imgSrc
  displayAsRows buttonElem [labelElem, imgElem]
  return buttonElem

imgButton :: String -> [Attribute] -> [Attribute] -> IO Elem
imgButton imgSrc buttonAttributes imgAttributes = do
    buttonElem <- newElem "button" `with` buttonAttributes
    imgElem <- styledImg imgSrc imgAttributes
    appendChild buttonElem imgElem
    return buttonElem
