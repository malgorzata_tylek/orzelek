module Client.Tags.Table where

import Haste.DOM
import Client.Styles.Bright

column :: IO Elem
column =
  newElem "td"

styledColumn :: [Attribute] -> IO Elem
styledColumn attributes =
  newElem "td" `with` attributes

parameterColumn :: IO Elem
parameterColumn =
  styledColumn parameterColumnStyle

resultColumn :: IO Elem
resultColumn =
  styledColumn columnStyle

hillParameterColumn :: IO Elem
hillParameterColumn =
  styledColumn hillParameterColumnStyle

row :: IO Elem
row =
  newElem "tr"

styledRow :: [Attribute] -> IO Elem
styledRow attributes =
  newElem "tr" `with` attributes

table :: IO Elem
table =
    newElem "table"

styledTable :: [Attribute] -> IO Elem
styledTable attributes =
  newElem "table" `with` attributes
