module Client.Tags.Input where

import Haste.DOM

input :: [Attribute] -> IO Elem
input attributes =
  newElem "input" `with` attributes
