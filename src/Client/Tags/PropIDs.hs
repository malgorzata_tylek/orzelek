module Client.Tags.PropIDs where

import Haste.DOM

background :: PropID
background = "background"

width :: PropID
width = "width"

height :: PropID
height = "height"

color :: PropID
color = "color"

margin :: PropID
margin = "margin"
