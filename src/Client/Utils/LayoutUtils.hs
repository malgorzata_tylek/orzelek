module Client.Utils.LayoutUtils where

import Haste.DOM
import Control.Monad
import Control.Monad.IO.Class

type TextElem = Elem

-- displays horizontally
appendChildren :: Elem -> [Elem] -> IO ()
appendChildren parent children = sequence_ [appendChild parent child | child <- children]

displayAsColumns :: Elem -> [Elem] -> IO ()
displayAsColumns = appendChildren

createDiv :: IO Elem
createDiv =
  newElem "div"

addOnDiv :: Elem -> IO Elem
addOnDiv elem = do
  newDiv <- createDiv
  appendChild newDiv elem
  return newDiv

displayAsRows :: Elem -> [Elem] -> IO ()
displayAsRows parent children = do
  rows <- sequence [addOnDiv row | row <- children]
  appendChildren parent rows

  -- sequence - collect results
  -- sequence_ - ignore results

writeOn :: Elem -> TextElem -> IO ()
writeOn elem text =
  appendChild elem text

writeOnElem :: Elem -> String -> IO ()
writeOnElem elem text = do
   textElem <- newTextElem text
   appendChild elem textElem

newStyledDiv :: [Attribute] -> IO Elem
newStyledDiv attributes =
  newElem "div" `with` attributes
