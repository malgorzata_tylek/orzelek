module Client.Parameters.Hills where

-- 1. Bischofshofen
bischofshofenName :: String
bischofshofenName = "Paul Ausserleitner"

bischofshofenHS :: Double
bischofshofenHS = 140

bischofshofenSlopeAngle :: Double
bischofshofenSlopeAngle = 35

-- 2. Innsbruck
innsbruckName :: String
innsbruckName = "Bergisel"

innsbruckHS :: Double
innsbruckHS = 130

innsbruckSlopeAngle :: Double
innsbruckSlopeAngle = 34.5

-- 3. Ramsau
ramsauName :: String
ramsauName = "W90-Schanze"

ramsauHS :: Double
ramsauHS = 98

ramsauSlopeAngle :: Double
ramsauSlopeAngle = 36

-- 4. Zakopane
zakopaneName :: String
zakopaneName = "Wielka Krokiew"

zakopaneHS :: Double
zakopaneHS = 134

zakopaneSlopeAngle :: Double
zakopaneSlopeAngle = 35.5

-- 5. Engelberg
engelbergName :: String
engelbergName = "Gross-Titlis-Schanze"

engelbergHS :: Double
engelbergHS = 142

engelbergSlopeAngle :: Double
engelbergSlopeAngle = 34

-- 6. Oslo
osloName :: String
osloName = "Holmenkollbakken"

osloHS :: Double
osloHS = 134

osloSlopeAngle :: Double
osloSlopeAngle = 33.2

-- 7. Kuusamo
kuusamoName :: String
kuusamoName = "Rukatunturi"

kuusamoHS :: Double
kuusamoHS = 142

kuusamoSlopeAngle :: Double
kuusamoSlopeAngle = 36.9

-- 8. Planica
planicaName :: String
planicaName = "Letalnica bratov Gorišek"

planicaHS :: Double
planicaHS = 225

planicaSlopeAngle :: Double
planicaSlopeAngle = 33
