module Main where

import Haste.App.Standalone

main = runStandaloneApp $ do
  runServer<- remote (liftIO $ putStrLn "Hello Server!")
  runClient $ onServer runServer
