module Paths_haste_standalone (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/malgo/.cabal/bin"
libdir     = "/home/malgo/.cabal/lib/x86_64-linux-ghc-7.10.3/haste-standalone-0.1.0.0-LyCDfvvpOKcDAJasHjMrFr"
datadir    = "/home/malgo/.cabal/share/x86_64-linux-ghc-7.10.3/haste-standalone-0.1.0.0"
libexecdir = "/home/malgo/.cabal/libexec"
sysconfdir = "/home/malgo/.cabal/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "haste_standalone_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "haste_standalone_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "haste_standalone_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "haste_standalone_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "haste_standalone_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
