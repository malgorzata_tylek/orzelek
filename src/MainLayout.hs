module Main where

import Haste.DOM
import Haste
import Haste.Foreign
import Haste.Events
import Haste.Prim
import Control.Monad.IO.Class
import Client.Utils.LayoutUtils
import Client.Styles.Common
import Client.Styles.Bright
import Client.Panels.Cards
import Client.Panels.HillPanel
import Client.Panels.AerodynamicsPanel
import Client.Panels.BiomechanicsPanel
import Client.Panels.ButtonsPanel
import Client.Panels.ResultPanel
import Client.Panels.GraphPanel
import Client.Tags.Img
import Client.Events.HillClickedEvents
import Server.Simulation.Algorithm
import Server.Simulation.Results

main = do
  photoArea <- newStyledDiv photoAreaStyle
  photo <- styledImg mainPhotoImgSrc photo
  writeOnElem photoArea titleText
  appendChild documentBody photo
  --cards
  cardsArea <- newStyledDiv cardAreaStyle
  Cards cardsAreaTable hillChoiceCard hillSizeCard aerodynamicsCard biomechanicsCard <- makeCards
  appendChild cardsArea cardsAreaTable
  -- hillChoice
  hillArea <- newStyledDiv hillPanelStyle
  HillsInput hillAreaTable hsInput slopeInput <- createHillPanel
  appendChild hillArea hillAreaTable
  -- aerodynamics parameters
  aeroArea <- newStyledDiv aeroPanelStyle
  AerodynamicsInput aeroAreaTable airDensityInput dragInput liftInput <- createAerodynamicsPanel
  appendChild aeroArea aeroAreaTable
  -- biomechanics parameters
  bioArea <- newStyledDiv bioPanelStyle
  BiomechanicsInput bioAreaTable massInput velocityXInput velocityYInput jumperAngleInput movementAngleInput longitudinalAreaInput
    frontalAreaInput <- createBiomechanicsPanel
  appendChild bioArea bioAreaTable
  -- buttons
  buttonsArea <- newStyledDiv buttonsPanelStyle
  Buttons buttonsTable calculateButton <- createButtonsPanel (hsInput, slopeInput) (airDensityInput, dragInput, liftInput)
    (massInput, velocityXInput, velocityYInput, jumperAngleInput, movementAngleInput, longitudinalAreaInput, frontalAreaInput)
  appendChild buttonsArea buttonsTable
  -- results
  distanceArea <- newStyledDiv resultsPanelStyle
  -- distancePanel <- createResultPanel 127 6.34
  -- appendChild distanceArea distancePanel
  -- graph
  graphPanel <- createGraphPanel
  (distancePanel, distanceResult, timeResult) <- createResultPanel 0 0
  appendChild distanceArea distancePanel

  onEvent calculateButton Click $ \_ -> do
    mass <- getInputValue massInput
    jsLog (toJSStr "calculateBtn")
    --testFunc mass
    vY0 <- getInputValue velocityYInput
    vX0 <- getInputValue velocityXInput
    jumperAngle <- getInputValue jumperAngleInput
    movementAngle <- getInputValue movementAngleInput
    frontArea <- getInputValue frontalAreaInput
    longArea <- getInputValue longitudinalAreaInput
    airDensity <- getInputValue airDensityInput
    dragCoefficient <- getInputValue dragInput
    liftCoefficient <- getInputValue liftInput
    hillSize <- getInputValue hsInput
    slopeAngle <- getInputValue slopeInput

    (distance, time) <- distanceAndTimeIO mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
    jsLog (toJSStr (show distance))
    --distancePanel <- createResultPanel distance time
    path <- flightPathIO mass vY0 vX0 jumperAngle movementAngle frontArea longArea airDensity dragCoefficient liftCoefficient hillSize slopeAngle
    jsLog (toJSStr (show $ head path))
    clearChildren graphPanel
    drawAndRender path graphPanel
    --drawAndRender Server.Simulation.Results.flightPath graphPanel
    clearChildren distanceResult
    clearChildren timeResult
    setDistance distanceResult distance
    setTime timeResult time

-- nie przesuwa do paneli - todo fix !!!
  onEvent hillChoiceCard Click $ \_ -> do
    focus hillArea

  onEvent hillSizeCard Click $ \_ -> do
    focus hillArea

  onEvent aerodynamicsCard Click $ \_ -> do
    focus aeroArea

  onEvent biomechanicsCard Click $ \_ -> do
    jsLog (toJSStr "biomechanics")

  -- body
  displayAsRows documentBody [photoArea, cardsArea, hillArea, aeroArea, bioArea, buttonsArea, distanceArea, graphPanel]
  assignBgStyle

-- getNum :: Elem -> IO Double
-- getNum input = read $ getProp input "value"

jsFunc :: JSString
jsFunc = toJSStr "(function (x) { console.log(x);})"

jsLog :: JSString -> IO ()
jsLog = ffi jsFunc
